<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/old', 'HomepageController@index'); //old template
Route::get('/', 'HomepageController@index2'); //new template
Route::get('/about-us', 'HomepageController@about');
Route::get('/contactus', 'HomepageController@contactus');
Route::get('/tracking', 'HomepageController@tracking');
Route::get('/getTracking', 'HomepageController@getTracking');
Route::get('/categories/{id}/{category_name}', 'CategoriesController@index');
// Route::get('/order-received', 'CallbackController@received');
Route::match(['get','post'], '/order-received', 'CallbackController@received');

Route::get('/hguthmtbb830hkxznwaceezddyt7md.html', 'HomepageController@fb_dns');

Route::get('/reset_password', "HomepageController@password");
Route::get('/lost_password', "HomepageController@lost_password");

Route::prefix('change')->group(function () {
    Route::get('/language', 'ChangeController@language');
    Route::get('/currency', 'ChangeController@currency');
});

Route::get('/product/{id}/{slug}', 'ProductController@detail');
Route::get('/wholesale/{id}', 'ProductController@wholesale');
//Route::get('/search/{slug}', 'ProductController@list');
Route::get('/search', 'ProductController@list');
Route::get('/more', 'ProductController@more');

Route::prefix('member')->group(function () {
    Route::get('/', 'MemberController@index');
    Route::get('/logout', 'MemberController@logout');
    Route::get('/login', 'MemberController@login_and_register');
    Route::post('/login', "MemberController@login");
    Route::post('/login_social', "MemberController@login_social");
    Route::post('/recheck_login_social', "MemberController@recheck_social_login");
    Route::post('/register', "MemberController@register");
    Route::get('/order', "MemberController@order");
    Route::get('/order/view/{id}', "MemberController@order_detail");
    Route::get('/address', "MemberController@address");
    Route::get('/address/edit/{id}', "MemberController@address_edit");
    Route::get('/detail', "MemberController@detail");
    Route::post('/detail_submit', "MemberController@detail_submit");
    Route::get('/password', "MemberController@password");
    Route::post('/password_submit', "MemberController@password_submit");

});

Route::prefix('cart')->group(function () {
    Route::get('/', 'CartController@cart');
    Route::get('/checkout', 'CartController@checkout');
});

Route::get('/wishlist', 'WishlistController@wishlist');

