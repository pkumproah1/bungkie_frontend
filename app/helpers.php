<?php
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
if (!function_exists('pages_path')) {
    function pages_path($path = null) {
        if (empty($path)) return asset();
        return asset('public/assets/pages/') . '/' . $path;
    }
}

if(!function_exists("myarray")){
    function myarray($products, $field, $value)
    {
        foreach($products as $key => $product)
        {
            if ( $product[$field] === $value )
                return $key;
        }
        return false;
    }
}

if (!function_exists('http_request')) {
    function http_request($params = array(), $token = "1|ruaIEjWw1HGB8AADnOPRwiWznFjszCIalm8YLpBh") {

        if (\Session::has('logged')) {
            $token = \Session::get('token');
        }
        
        $api = config('constants.web_api');
        $client = new Client(['base_uri' => $api]);
        $headers = [
            'Authorization' => 'Bearer ' . $token,   
            'Accept' => 'application/json',
            'Lang' => !empty(session()->get('lang')) ? session()->get('lang') : \App::getLocale(),
            'Currency' => !empty(session()->get('currency')) ? session()->get('currency') : 'thb'
        ];
        
        try {
            $response = array();
            if (strtolower($params['method']) == "get") {
                $response = $client->request('GET', $params['url'], [
                    'headers' => $headers
                ]);
            } else {
                $headers['Content-Type'] = "application/json";
                $response = $client->request($params['method'], $params['url'], [
                    'headers' => $headers,
                    'body' => json_encode($params['data'])
                ]);
            }
            $response = json_decode($response->getBody()->getContents(), TRUE);
            return $response;
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            $error = json_decode($e->getResponse()->getBody()->getContents(), TRUE);
            return array('status' => false, 'message' => $error['message']);
        }
        
        
        
        
    }
}

if (!function_exists('get_rating_stars')) {
    function get_rating_stars($star) {
        $empty = (5-$star);
        for ($i = 1; $i <= $star; $i++) {
            echo '<img class="icon" src="'. asset('public/assets/images/icon/star-full.png').'"/>';
        }
        for ($x = 1; $x <= $empty; $x++) {
            echo '<img class="icon" src="'. asset('public/assets/images/icon/star-empty.png').'"/>';
        }
    }
}

if (!function_exists('_2c2p_payment_code')) {
    function _2c2p_payment_code($code) {
        $text = "Payment Failed";
        switch ($code) {
            case '000':
                $text = "Payment Successful";
                break;
            case '001':
                $text = "Payment Pending";
                break;
            case '002':
                $text = "Payment Rejected";
                break;
            case '003':
                $text = "Payment was canceled by user";
                break;
            case '999':
                $text = "Payment Failed";
                break;
            
            default:
                $text = "Payment Failed";
                break;
        }

        return $text;
    }
}

if (!function_exists('_2c2p_payment_channel')) {
    function _2c2p_payment_channel($code) {
        $text = "IPP transaction";
        if ($code) {
            switch ($code) {
                case '001':
                    $text = "Credit and debit cards";
                    break;
                case '002':
                    $text = "Cash payment channel";
                    break;
                case '003':
                    $text = "Direct debit";
                    break;
                case '004':
                    $text = "Others";
                    break;
                case '005':
                    $text = "IPP transaction";
                    break;
                
                default:
                    $text = "IPP transaction";
                    break;
            }
        }
        return $text;
    }
}

if (!function_exists('_2c2p_payment_currency')) {
    function _2c2p_payment_currency($code) {
        $text = "Thai Baht";
        if ($code) {
            if ($code == "840") {
                $text = "US Dollar";
            } else {
                $text = "Thai Baht";
            }
        }
        return $text;
    }
}