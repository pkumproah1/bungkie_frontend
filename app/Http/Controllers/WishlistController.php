<?php 
namespace App\Http\Controllers;
use Illuminate\Http\Request;
class WishlistController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        if(!\Session::has('logged')) return redirect('/member');
    }
    
    public function wishlist(Request $request)
    {
        return $this->view('wishlist.wishlist', $this->data);
    }

    
}
