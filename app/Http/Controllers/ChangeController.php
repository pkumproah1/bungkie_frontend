<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

class ChangeController extends Controller
{    
    /**
     * language
     *
     * @param  mixed $request
     * @return void
     */
    public function language(Request $request)
    {
        if (!in_array($request->language, ['th','en'])) return response()->json([
            'status' => false
        ]);
        session()->put('lang', $request->language);
        session()->save();
        return response()->json([
            'status' => true
        ]);
    }
    
    /**
     * currency
     *
     * @param  mixed $request
     * @return void
     */
    public function currency(Request $request)
    {
        if (!in_array($request->currency, ['thb','usd'])) return response()->json([
            'status' => false
        ]);
        session()->put('currency', $request->currency);
        session()->save();
        return response()->json([
            'status' => true
        ]);
    }
}
