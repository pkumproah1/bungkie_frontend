<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
class HomepageController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function index()
    {
        $api_result = http_request(['method' => "GET", "url" => "product/listhome?type=category&typeId=1,3,2"]);
        $this->data['product'] = $api_result;
        $this->data['currency'] = (session()->get('currency') === 'usd') ? 'USD' : 'THB';
        return $this->view('homepage.index');
    }

    public function custom_cat_sort($category_all){
        $after_sort = array();
        $pattern = array(
            3,4,6,1,5,2,8
        );
        $max = count($pattern);
        foreach($category_all as $key => $cat){
            
            $find = array_search($cat['id'], $pattern);
            if($find === false){
                $after_sort[$max] = $cat;
                $max++;
            }else{
                $after_sort[$find] = $cat;
            }
        }
        ksort($after_sort);
        return $after_sort;
    }

    public function index2()
    {
        // $api_result = http_request(['method' => "GET", "url" => "product/listhome?type=category&typeId=1,3,2,6,4,23,40,153,108,152,111"]);
        $api_result = http_request(['method' => "GET", "url" => "product/listhome?type=category&typeId=1,3,2,6,4,23,40,153,108,152,111,5,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,109,110,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175,176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,191,192,193,194,195,196,197,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,223,224,225,226,227,228,229,230,231,232,233,234,235,236,237,238,239,240,241,242,243,244,245,246,247,248,249,250,251,252,253,254,255,256,257,258,259,260,261,262,263,264,265,266,267,268,269,270,271,272,273,274,275,276,277,278,279,280,281,282,283,284,285,286,287,288,289,290,291,292,293,294,295,296,297,298,299,300,301,302,303,304,305,306,307,308,309,310,311,312,313,314,315,316,317,318,319,320,321,322,323,324,325,326,327,328,329,330,331,332,333,334,335,336,337,338,339,340,341,342,343,344,345,346,347,348,349,350,351,352,353,354,355,356,357,358,359,360,361,362,363,364,365,366,367,368,369,370,371,372,373,374,375,376,377,378,379,380,381,382,383,384,385,386,387,388,389,390,391,392,393,394,395,396,397,398,399,400"]);
        $this->data['category_all'] = $this->custom_cat_sort($this->data['category_all']);
        if ($api_result['status'] === false) return redirect('/member/logout');
        $this->data['product'] = $api_result['data'];
        $this->data['currency'] = (session()->get('currency') === 'usd') ? 'USD' : 'THB';
        return $this->view('homepage.index2');
    }

    public function about()
    {
        return $this->view('homepage.about');
    }

    public function contactus()
    {
        return $this->view('homepage.contactus');
    }

    public function fb_dns()
    {
        return $this->view('homepage.fb_dns');
    }

    public function tracking()
    {
        return $this->view('homepage.tracking');
    }

    public function getTracking(Request $request)
    {
        $response = http_request([
            'method' => "get",
            // 'url' => 'purchase/reference?referenceId='.$request->referenceId,
            'url' => 'product/tracking?reference_id='.$request->referenceId
        ]);
        // dd($response);
        return $response;
    }

    public function password(Request $request)
    {
        return $this->view('homepage.password');
    }

    public function lost_password()
    {
        return $this->view('homepage.lost');
    }
}
