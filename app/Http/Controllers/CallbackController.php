<?php

namespace App\Http\Controllers;
use Carbon\Carbon;
use Illuminate\Http\Request;
class CallbackController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function received(Request $request)
    {
        // if ($request->all()) {
        //     if (isset($request->payment_status)) {
        //         $backend_url = config('constants.web_api') . 'callback/2c2p';
        //         $ch = curl_init();
        //         curl_setopt($ch, CURLOPT_URL, $backend_url);
        //         curl_setopt($ch, CURLOPT_POST, 1);
        //         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        //         curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
        //         curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        //             'Content-Type: application/x-www-form-urlencoded',
        //             'Accept: application/json'
        //         ));
        //         curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($request->all()));
        //         $data = curl_exec($ch);
        //         $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        //         $new_data = json_decode($data, TRUE);
        //         curl_close($ch);
        //     }
        // }
        $purchase = http_request(['method' => 'GET', 'url' => 'purchase/reference?referenceId=' . $request->reference]);
        if ($purchase['status'] === false) {
            $this->data['purchase'] = null;
        } else {
            $this->data['purchase'] = array(
                'status' => $purchase['status'],
                'payment_status' => $purchase['data']['payment_status'],
                'payment_channel' => $purchase['data']['payment_channel'],
                'payment_currency' => $purchase['data']['currency'],
                'payment_order' => $purchase['data']['reference_id'],
                'payment_amount' => $purchase['data']['amount'],
                'payment_desc' => $purchase['data']['channel_response_desc'],
                'transaction_datetime' => Carbon::parse($purchase['data']['transaction_datetime'])->locale('en')->isoFormat("LL"),
                'products' => $purchase['data']['products']
            );
        }
        return $this->view('cart.order-received');
    }

}
