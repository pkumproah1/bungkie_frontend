<?php

namespace App\Http\Controllers;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public $data = array();
    public function __construct()
    {
        // * check language
        self::get_language();
        self::get_category();
        self::default_currency();
    }    
    
    /**
     * view
     *
     * @param  mixed $view
     * @param  mixed $parameter
     * @return void
     */
    public function view($view = null)
    {
        if (empty($view)) return abort(404);
        return view('website.' . $view, $this->data);
    }
    
    /**
     * get_category
     *
     * @return void
     */
    public function get_category()
    {
        $category = http_request(['method' => "GET", "url" => "category", "Lang" => \App::setLocale(\Session::get('lang'))]);
        if ($category['status'] === false) return redirect('/member/logout');
        if (!isset($category['data']) && empty($category['data'])) return redirect('/member/logout');
        $this->data['category_all'] = $category['data'];
    }

    /**
     * get_language
     *
     * @return void
     */
    private function get_language()
    {
        if (\Session::has('lang')) {
            \App::setLocale(\Session::get('lang'));
        }else{
            \Session::put('lang', 'en');
        }
    }
    /**
     * get_language
     *
     * @return void
     */
    private function default_currency()
    {
        if (!\Session::has('currency')) {
            
            session()->put('currency', 'thb');
            session()->save();
        }
    }
}
