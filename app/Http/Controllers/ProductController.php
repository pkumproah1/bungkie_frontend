<?php

namespace App\Http\Controllers;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function detail($id = null)
    {
        $product = http_request(['method' => "GET", "url" => "product/findby?product_id=" . $id]);
        
        if ($product['status'] === false) return redirect('/');
        if (!isset($product['data']) && empty($product['data'])) return redirect('/');
        $this->data['currency'] = (session()->get('currency') === 'usd') ? 'USD' : 'THB';
        $this->data['fbq_currency'] = strtoupper(session()->get('currency'));
        $this->data['product'] = $product['data'];

        $this->data['product_id'] = $id;
        $this->data['category_id'] = $product['data']['category']['category_id'];
        $this->data['category_url'] = str_replace('categories/', '', $product['data']['category']['url']);
        
        //related
        $related = http_request(['method' => "GET", "url" => "product/listhome?type=category&typeId=". $product['data']['category']['category_id']]);
        if($related['status'] != false) $this->data['products'] = $related['data']['category'][0]['data'];

        return $this->view('product.detail', $this->data);
    }

    public function list(Request $request)
    {
        $this->data['search'] = $request->s;
        return $this->view('product.list', $this->data);
    }
    
    public function more(Request $request)
    {
        return $this->view('product.more', $this->data);
    }

    public function wholesale(Request $request)
    {
        $slug = $request->id;
        $product = http_request(['method' => "GET", "url" => "product/findby?product_id=" . $slug]);
        if(isset($product['data'])) $this->data['product'] = $product['data']; else return redirect('/');
        $this->data['currency'] = (session()->get('currency') === 'usd') ? 'USD' : 'THB';        
        $this->data['search'] = $slug;
        $province = http_request(['method' => "GET", "url" => "location/province"]);
        if (!isset($province['data']) && empty($province['data'])) {
            $this->data['province'] = [];
        } else {
            $this->data['province'] = $province['data'];
        }
        return $this->view('product.wholesale', $this->data);
    }
}
