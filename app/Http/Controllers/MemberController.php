<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

class MemberController extends Controller
{
    public $maccount, $morder, $maddress, $mdetail, $mpassword;
    public function __construct()
    {
        parent::__construct();
        if (\Session::has('logged')){
            if(session()->get('account_type') == 'local'){
                $this->data['password_change'] = true;
            }else{
                $this->data['password_change'] = false;
            }
        }else{
            return redirect('member/login');
        }
        
    }
    
    public function index(Request $request)
    {
        if (!\Session::has('logged')) return redirect('member/login');
        $this->maccount = "is-active";
        
        return $this->view('member.index')
        ->with('profile', session()->get('profile'))
        ->with('maccount', $this->maccount)
        ->with('morder', $this->morder)
        ->with('maddress', $this->maddress)
        ->with('mdetail', $this->mdetail)
        ->with('mpassword', $this->mpassword);
    }

    public function order(Request $request)
    {
        if (!\Session::has('logged')) return redirect('member/login');
        $this->morder = "is-active";
        $api_result = http_request(['method' => "GET", "url" => "purchase/userid"]);
        // dd($api_result);
        if ($api_result['status'] === false) $this->data['order'] = array();
        else $this->data['order'] = $api_result['data'];
        // 
        return $this->view('member.order')
        ->with('profile', session()->get('profile'))
        ->with('maccount', $this->maccount)
        ->with('morder', $this->morder)
        ->with('maddress', $this->maddress)
        ->with('mdetail', $this->mdetail)
        ->with('mpassword', $this->mpassword);
    }

    public function order_detail(Request $request)
    {
        if (!\Session::has('logged')) return redirect('member/login');
        $this->morder = "is-active";

        $api_result = http_request(['method' => "GET", "url" => "purchase/userid"]);
        if ($api_result['status'] === false) return redirect('/member/logout');
        $this->data['order'] = $api_result['data'];
        // dd($api_result);

        return $this->view('member.order_view')
        ->with('profile', session()->get('profile'))
        ->with('order_id', $request->id)
        ->with('maccount', $this->maccount)
        ->with('morder', $this->morder)
        ->with('maddress', $this->maddress)
        ->with('mdetail', $this->mdetail)
        ->with('mpassword', $this->mpassword);
    }

    public function password(Request $request)
    {
        if (!\Session::has('logged')) return redirect('member/login');
        $this->mpassword = "is-active";

        
        
        return $this->view('member.password')
        ->with('profile', session()->get('profile'))
        ->with('maccount', $this->maccount)
        ->with('morder', $this->morder)
        ->with('maddress', $this->maddress)
        ->with('mdetail', $this->mdetail)
        ->with('mpassword', $this->mpassword);
    }

    public function password_submit(Request $request)
    {
        if (!\Session::has('logged')) return redirect('member/login');
        $this->mpassword = "is-active";

        $response = http_request([
            'method' => "post",
            'url' => 'member/signin',
            'data' => array(
                'type' => 'local',
                'email' => $request->email,
                'password' => $request->password,
                'remember' => 'on'
            ),
        ]);
        $profile = session()->get('profile');

        //dd($response);
        
        return $response;
    }

    public function detail(Request $request)
    {
        if (!\Session::has('logged')) return redirect('member/login');
        $this->mdetail = "is-active";

        $profile = http_request(['method' => "GET", "url" => "member/"]);

        return $this->view('member.detail')
        ->with('profile', $profile['data']['profile'])
        ->with('profile_type', $profile['data']['type'])
        ->with('maccount', $this->maccount)
        ->with('morder', $this->morder)
        ->with('maddress', $this->maddress)
        ->with('mdetail', $this->mdetail)
        ->with('mpassword', $this->mpassword);
    }

    public function address(Request $request)
    {
        if (!\Session::has('logged')) return redirect('member/login');
        $this->maddress = "is-active";

        
        
        $response = http_request([
            'method' => "get",
            'url' => 'member/address',
            'data' => array(),
        ]);
        // dd($response);
        if($response['status'] === false) $response['data'] = array(); 

        $province = http_request(['method' => "GET", "url" => "location/province"]);
        if (!isset($province['data']) && empty($province['data'])) {
            $this->data['province'] = [];
        } else {
            $this->data['province'] = $province['data'];
        }
        

        return $this->view('member.address')
        ->with('profile', session()->get('profile'))
        ->with('maccount', $this->maccount)
        ->with('morder', $this->morder)
        ->with('maddress', $this->maddress)
        ->with('mdetail', $this->mdetail)
        ->with('address', $response['data'])
        ->with('mpassword', $this->mpassword);
    }

    public function login_and_register(Request $request)
    {
        if (\Session::has('logged')) return redirect('member');
        // dd(session()->all());
        return $this->view('member.login');
    }


    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]);
        
        $response = http_request([
            'method' => "post",
            'url' => 'member/signin',
            'data' => array(
                'type' => 'local',
                'email' => $request->email,
                'password' => $request->password,
                'remember' => 'on'
            ),
        ]);

        


        if($response['status'] === false) return response()->json([
            'status' => false,
            'message' => $response['message']
        ], 400);

        \Session::put('profile', $response['data']['profile']);
        \Session::put('logged', true);
        \Session::put('account_type', $response['data']['type']);
        \Session::put('token', $response['data']['token']);
        \Session::save();
        
        

        if(session()->get('logintogo') != ''){
            $logintogo = session()->get('logintogo');
            \Session::put('logintogo', '');
        }else{
            $logintogo = url('/member/');
        }   

        return response()->json([
            'status' => true,
            'redirect' => $logintogo
        ], 200);
    }
    
    public function recheck_social_login(Request $request)
    {
        $response = http_request([
            'method' => "post",
            'url' => 'member/signin',
            'data' => array(
                'type' => $request->type,
                'email' => $request->email,
                'name' => $request->name,
                'phone' => $request->phone,
                'id' => $request->app_id,
                'avatar' => $request->avatar
            )
        ]);
        if($response['status'] === false) return response()->json([
            'status' => false,
            'message' => $response['message']
        ], 400);

        if (empty($response['data']['profile']['email']) || empty($response['data']['profile']['phone'])) {
            // login_social
            return response()->json([
                'status' => true,
                'data' => array(
                    'email' => $response['data']['profile']['email'],
                    'phone' => $response['data']['profile']['phone']
                )
            ], 200);
        } else {

            \Session::put('profile', $response['data']['profile']);
            \Session::put('logged', true);
            \Session::put('account_type', $response['data']['type']);
            \Session::put('avatar', $response['data']['profile']['avatar']);
            \Session::put('token', $response['data']['token']);
            \Session::save();

            $logintogo = url('/member/');

            if(session()->get('logintogo') != ""){
                $logintogo = session()->get('logintogo');
                \Session::put('logintogo', '');
            } 

            return response()->json([
                'status' => true,
                'data' => array(
                    'email' => $response['data']['profile']['email'],
                    'phone' => $response['data']['profile']['phone']
                ),
                // 'redirect' => url('/member/')
                'redirect' => $logintogo
            ], 200);
        }
        

    }
    /**
     * login_social
     *
     * @param  mixed $request
     * @return void
     */
    public function login_social(Request $request)
    {
        $request->validate([
            'app_id' => 'required|numeric',
            'type' => 'required',
            'name' => 'required',
        ]);
    
        $response = http_request([
            'method' => "post",
            'url' => 'member/signin',
            'data' => array(
                'type' => $request->type,
                'email' => $request->email,
                'name' => $request->name,
                'phone' => $request->phone,
                'id' => $request->app_id,
                'avatar' => $request->avatar
            )
        ]);
        
        if($response['status'] === false) return response()->json([
            'status' => false,
            'message' => $response['message']
        ], 400);

        

        \Session::put('profile', $response['data']['profile']);
        \Session::put('logged', true);
        \Session::put('account_type', $response['data']['type']);
        \Session::put('avatar', $response['data']['profile']['avatar']);
        \Session::put('token', $response['data']['token']);
        \Session::save();

        $logintogo = url('/member/');

        if(session()->get('logintogo') != ""){
            $logintogo = session()->get('logintogo');
            \Session::put('logintogo', '');
        } 
        

        return response()->json([
            'status' => true,
            'redirect' => $logintogo,
            'token' => $response['data']['token']
        ], 200);
    }

    public function register(Request $request)
    {
        $request->validate([
            'fname' => 'required',
            'lname' => 'required',
            'phone' => 'required|numeric',
            'email' => 'required|email',
            'password' => 'required'
        ]);
        
        $response = http_request([
            'method' => "post",
            'url' => 'member/signup',
            'data' => array(
                'firstName' => $request->fname,
                'lastName' => $request->lname,
                'phone' => $request->phone,
                'email' => $request->email,
                'password' => $request->password
            ),
        ]);

        if($response['status'] === false) return response()->json([
            'status' => false,
            'message' => $response['message']
        ], 400);

        \Session::put('profile', $response['data']['profile']);
        \Session::put('logged', true);
        \Session::put('account_type', $response['data']['type']);
        \Session::put('token', $response['data']['token']);
        \Session::save();

        $logintogo = url('/member/');

        if(session()->get('logintogo') != ""){
            $logintogo = session()->get('logintogo');
            \Session::put('logintogo', '');
        } 

        return response()->json([
            'status' => true,
            'redirect' => $logintogo
        ], 200);
    }

    public function test()
    {
        return $this->view('member.www');
    }

    public function logout(Request $request)
    {
        \Session::forget('logged');
        return redirect('/');
    }

}
