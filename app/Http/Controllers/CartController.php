<?php 
namespace App\Http\Controllers;
use Illuminate\Http\Request;
class CartController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        if(!\Session::has('logged')){
            \Session::put('logintogo', url()->current());
            return redirect('/member');
        } 
    }
    public function cart(Request $request)
    {
        if(!\Session::has('logged')){
            \Session::put('logintogo', url()->current());
            return redirect('/member');
        }
        return $this->view('cart.cart', $this->data);
    }

    public function checkout(Request $request)
    {
        $province = http_request(['method' => "GET", "url" => "location/province"]);
        $this->data['fbq_currency'] = strtoupper(session()->get('currency'));
        if (!isset($province['data']) && empty($province['data'])) {
            $this->data['province'] = [];
        } else {
            $this->data['province'] = $province['data'];
        }
        return $this->view('cart.checkout', $this->data);
    }
}
