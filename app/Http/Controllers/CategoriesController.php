<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    public function index(Request $request)
    {
        $category_name = $request->category_name;
        $cat = http_request(['method' => "GET", "url" => "category"]);
        $product = http_request(['method' => "GET", "url" => "product/listitem?limit=60&offset=0&type=category&typeId=".$request->id]);
        $this->data['currency'] = (session()->get('currency') === 'usd') ? 'USD' : 'THB';
        if(!$product['status']) $product['data'] = array();

        return $this->view('category.index')
        ->with('category', $cat['data'])
        ->with('product', $product['data'])
        ->with('category_search', $category_name)
        ->with('category_id', $request->id);
    }
}
