<?php 

namespace App\Libraries;

class THClass {
	// if($_SESSION['currency'] == 'th'){
	// 	$currency = "THB ";
	// }elseif($_SESSION['currency'] == 'en'){
	// 	$currency = "USD ";
	// }
	
	public $month_full = array(
		1 => "มกราคม",
		2 => "กุมภาพันธ์",
		3 => "มีนาคม",
		4 => "เมษายน",
		5 => "พฤษภาคม",
		6 => "มิถุนายน",
		7 => "กรกฏาคม",
		8 => "สิงหาคม",
		9 => "กันยายน",
		10 => "ตุลาคม",
		11 => "พฤศจิกายน",
		12 => "ธันวาคม"
	);
	
	public $month_short = array(
		1 => "ม.ค.",
		2 => "ก.พ.",
		3 => "มี.ค.",
		4 => "ม.ย.",
		5 => "พ.ค.",
		6 => "มิ.ย.",
		7 => "ก.ค.",
		8 => "ส.ค.",
		9 => "ก.ย.",
		10 => "ต.ค.",
		11 => "พ.ย.",
		12 => "ธ.ค."
	);
	
	public $day_full = array(
		1 => "วันจันทร์",
		2 => "วันอังคาร",
		3 => "วันพุธ",
		4 => "วันพฤหัสบดี",
		5 => "วันศุกร์",
		6 => "วันเสาร์",
		7 => "วันอาทิตย์"
	);
	
	public $day_short = array(
		1 => "จ.",
		2 => "อ.",
		3 => "พ.",
		4 => "พฤ.",
		5 => "ศ.",
		6 => "ส.",
		7 => "อา."
	);
	
	public $currency = "THB";
	public $nocomment = "ยังไม่มีความคิดเห็น";
	public $related_product = "สินค้าที่เกี่ยวข้อง";
	public $description = "รายละะอียด";
	public $review = "รีวิว";
	public $out_of = "จาก";
	public $rated = "เรท";
	public $your_review = "การรีวิวของคุณ";
	public $your_rating = "เรทติ้งของคุณ";
	public $add_a_review = "เพิ่มรีวิว";
	public $name = "ชื่อ";
	public $email = "อีเมล์";
	public $recently_product = "สินค้าที่เพิ่งรับชม";
	public $sign_up_to_newsletter = "ลงทะเบียนเพื่อรับอีเมล์";
	public $sign_up = "ลงทะเบียน";
	public $enter_your_email = "กรุณาใส่อีเมล์";
	public $add_to_wishlist = "เพิ่มในรายการโปรด";
	public $add_to_cart = "เพิ่มใส่ตะกร้า";
	public $add_to_compare = "เพิ่มเพื่อเปรียบเทียบ";
	public $shop = "ร้านค้า";
	public $home = "หน้าหลัก";
	public $read_more = "อ่านเพิ่มเติม";
	public $latest_product = "สินค้าล่าสุด";
	public $browse_category = "เข้าชมหมวดหมู่ต่างๆ";
	public $filter = "ตัวกรอง";
	public $filter_by_price = "คัดกรองจากราคา";
	public $show_all_category = "แสดงหมวดหมู่ทั้งหมด";
	public $search_for_product = "ค้นหาสินค้า";
	public $search = "ค้นหา";
	public $search_result_for = "ผลลัพธ์การค้นหาสำหรับ";
	public $signup_alert = "ลงทะเบียนแล้ว";
	public $signup_complete = "ขอบคุณสำหรับการลงทะเบียน";
	public $subtotal = "รวมทั้งหมด";
	public $view_cart = "ดูตะกร้าสินค้า";
	public $checkout = "เช็คเอ้าท์";
	public $your_cart = "ตะกร้าสินค้าของคุณ";
	public $proceed_to_checkout = "ไปที่เช็คเอาท์";
	public $shipping = "กำลังจัดส่ง";
	public $flat_rate = "อัตราคงที่";
	public $total = "ทั้งหมด";
	public $cart_total = "จำนวนสินค้าในตะกร้าทั้งหมด";
	public $back_to_shopping = "กลับไปช้อปปิ้งต่อ";
	public $tproduct = "สินค้า";
	public $tprice = "ราคา";
	public $quantity = "จำนวน";
	public $update_cart = "อัพเดทตะกร้าสินค้า";
	public $product_remove_cart = "สินค้าได้ถูกนำออกจากตะกร้าสินค้าเรียบร้อยแล้ว";
	public $confirm_remove_cart = "นำสินค้าชิ้นนี้ออกจากตะกร้าสินค้า?";
	public $no_register_email = "ไม่พบอีเมล์";
	public $found_register_email = "ส่งอีเมล์รีเซ็ตแล้ว";
	public $register_or_sign_in = "ลงทะเบียนใหม่ หรือ เข้าสู่ระบบ";
	public $my_account = "บัญชีของฉัน";
	public $text_in_login = "ข้อความแสดงในส่วนหน้าต่างการเข้าสู่ระบบ";
	public $text_in_register = "ลงทะเบียนสมัครบัญชีใหม่วันนี้ รับสิทธิประโยชน์มากมายในการช้อปปิ้ง พิเศษเฉพาะคุณเท่านั้น";
	public $login_error = "ไม่พบอีเมล์หรือพาสเวิร์ดนี้";
	public $login_fail_exceed = "เข้าสู่ระบบล้มเหลวเกินจำนวนครั้งที่กำหนด";
	public $register_error = "อีเมล์นี้ได้ถูกลงทะเบียนใช้งานแล้ว.";
	public $register_fail_exceed = "ลงทะเบียนล้มเหลวเกินจำนวนครั้งที่กำหนด";
	public $product_added = "เพิ่มสินค้าแล้ว";
	public $contact_us_call = " (+66) 02-114-7629 (5 คู่สาย)";
	public $na = "N/A";
	public $password_is_too_short = "รหัสผ่านต้องประกอบด้วย ตัวอักษรพิมพ์เล็กและใหญ่ ตัวเลข และอักขระพิเศษ อย่างละ 1 ตัว ความยาว 8 อักษร";
	public $status = "สถานะ";
	public $tdate = "วันที่ทำรายการ";
	public $order_detail = "รายละเอียดคำสั่งซื้อ";
	public $track_your_order = " ติดตามสถานะจัดส่งสินค้า";
	public $country = " ประเทศ";
	public $language = " ภาษา";
	public $tcurrency = " ค่าเงิน";
	public $save = " บันทึก";
	public $all_cat = " ทุกหมวดหมู่";
	public $contact1 = " เวลาทำการ Bungkie Call Center";
	public $contact2 = " วันจันทร์ - วันศุกร์ 09.00 – 18.00 น.";
	public $contel = "(+66) 02-114-7629 (5 คู่สาย)";
	public $safe_payment = " วิธีการชำระเงิน";
	public $find_it_fast = " ค้นหาอย่างรวดเร็ว";
	public $customer_care = " ศูนย์ดูแลลูกค้า";
	public $track_order = " ติดตามสถานการณ์จัดส่งสินค้า";
	public $wishlist = " รายการโปรด";
	public $aboutus = " เกี่ยวกับเรา";
	public $wholesale = "สั่งสินค้าราคาส่ง";
	public $contact_us = "ติดต่อเรา";
	public $secure_payment = "ชำระเงินปลอดภัย";
	public $wwcb = "ฐานลูกค้าทั่วทุกมุมโลก";
	public $we_ship = "จัดส่งทั่วโลกกว่า 200 ประเทศ และภูมิภาค";
	public $pay_with = "ชำระเงินด้วยระบบการจ่ายเงินชั้นนำระดับโลก";
	public $fast_deli = "ขนส่งรวดเร็ว";
	public $what_you_want = "ทุกอย่างที่คุณต้องการ เราจะสรรหามาให้คุณถึงที่";
	public $obb = "คัดสรรเฉพาะแบรนด์สินค้าที่ดีที่สุด";
	public $on_sale = "สินค้าราคาโปรโมชั่นมีพร้อมเสมอบนบังกี้";
	public $follow_us = "ติดตามเรา";
	public $dashboard = "หน้าประมวลผลงาน";
	public $torders = "คำสั่งซื้อ";
	public $addresses = "ที่อยู่";
	public $account_details = "รายละเอียดบัญชี";
	public $order_details = "รายละเอียดคำสั่งซื้อ";
	public $tview = "ดู";
	public $logout = "ออกจากระบบ";
	public $account_edit = "แก้ไขบัญชี";
	public $billing_address = "ที่อยู่ในการวางบิลล์";
	public $shipping_address = "ที่อยู่ในการจัดส่ง";
	public $hello = "สวัสดี";
	public $wholesale_submit = "ขอบคุณสำหรับข้อมูลการค้าส่ง";
	public $fname = "ชื่อ";
	public $lname = "นามสกุล";
	public $company_name = "ชื่อบริษัท";
	public $optional = 'เพิ่มเติม';
	public $street_address = "ถนน";
	public $town_city = "จังหวัด/เมือง";
	public $state = "รัฐ";
	public $postcode = "รหัสไปรษณีย์";
	public $phone = "หมายเลขโทรศัพท์";
	public $email_address = "อีเมล์";
	public $contact = "ข้อมูลติดต่อ";
	public $note = 'หมายเหตุ';
	public $unit = 'หน่วย';
	public $birthday = "วันเกิด";
	public $current_password = "รหัสผ่านปัจจุบัน (เว้นว่างหากไม่ต้องการเปลี่ยน)";
	public $new_password = "รหัสผ่านใหม่ (เว้นว่างหากไม่ต้องการเปลี่ยน)";
	public $confirm_new_password = "ยืนยันรหัสผ่านใหม่";
	public $password_change = "เปลี่ยนรหัสผ่าน";
	public $display_name = "ชื่อที่แสดง";
	public $display_notice = "เป็นชื่อที่ใช้แสดงในบัญชีนี้";
	public $default_address = "ที่อยู่ด้านล่างจะถูกใช้ตอน checkout โดยอัตโนมัติ";
	
}
?>