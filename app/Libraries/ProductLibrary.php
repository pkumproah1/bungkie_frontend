<?php
namespace App\Libraries;
use App\Models\Tb_product;
use App\User;
use App\Models\Tb_product_language;
use App\Models\Tb_product_option;
use App\Models\Tb_product_price;
class ProductLibrary {

    /**
     * generate sku
     */ 
    public function generate_sku($category_id = null, $prefix = "SKU")
    {
        if (!empty($category_id)) {
            $sku = $prefix . date('Ymd') . str_pad($category_id, 3, '0', STR_PAD_LEFT) . rand(00000,99999);
            $check = Tb_product::where('sku', $sku)->first();
            if ($check !== null) {
                self::generate_sku();
            }
            return $sku;
        }
    }
    
    /**
     * hasRole
     *
     * @param  mixed $role
     * @return void
     */
    public function hasRole($role = 'seller')
    {
        $userRoles = Auth()->user()->getRoleNames();
        $status = false;
        foreach($userRoles as $userRole){
            if($userRole == $role){
                $status = true;
                break;
            }
        }

        return $status;
    }

    // find product by product id
    public function find_product($id = null)
    {
        if (empty($id)) return;
        $response = null;
        // Product
        $product = Tb_product::where('tb_product.id', $id)
            ->select('id', 'category_id', 'brand', 'sku', 'tags', 'dimension', 'dimension_unit', 'weight', 'weight_unit', 'in_stock', 'status', 'hilight_flag', 'shipment_price', 'viewer', 'created_at');  
        
        if(self::hasRole('seller')) {
            $product->where('owner', Auth()->user()->id);
        }
        
        $product = $product->first();
        // verify product
        if (empty($product)) return;
        // product language
        $product_language = Tb_product_language::where('tb_product_language.product_id', $id)
            ->select('name', 'short_description', 'description', 'shipment_description', 'special_condition', 'language_id')
            ->get();
        // verify product language
        if (empty($product_language)) return;

        $product_price = Tb_product_price::where('tb_product_price.product_id', $id)
            ->select('type_price', 'base_price', 'front_price', 'sale_price')
            ->get();
        if (empty($product_price)) return;

        // product option [color, size]
        $product_option = new Tb_product_option();
        // Response Data
        $response['product'] = $product->toArray();
        $response['option'] = $product_option->get_options($id);
        // loop new variable for language detail
        foreach($product_language as $l) {
            $response['product_language'][$l['language_id']] = array(
                'name' => $l['name'],
                'short_description' => $l['short_description'],
                'description' => $l['description'],
                'shipment_description' => $l['shipment_description'],
                'special_condition' => $l['special_condition']
            );
        }
        // loop new variable for price
        foreach($product_price as $p) {
            $response['product_price'][$p['type_price']] = array(
                'base_price' => $p['base_price'],
                'front_price' => $p['front_price'],
                'sale_price' => $p['sale_price']
            );
        }

        return $response;
    }

}