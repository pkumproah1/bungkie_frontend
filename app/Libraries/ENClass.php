<?php 

namespace App\Libraries;

class ENClass {
	// if($_SESSION['currency'] == 'th'){
	// 	$currency = "THB ";
	// }elseif($_SESSION['currency'] == 'en'){
	// 	$currency = "USD ";
	// }
	public $month_full = array(
		1 => "January",
		2 => "February",
		3 => "March",
		4 => "April",
		5 => "May",
		6 => "June",
		7 => "July",
		8 => "August",
		9 => "September",
		10 => "October",
		11 => "November",
		12 => "December"
	);
	
	public $month_short = array(
		1 => "Jan",
		2 => "Feb",
		3 => "Mar",
		4 => "Apr",
		5 => "May",
		6 => "Jun",
		7 => "Jul",
		8 => "Aug",
		9 => "Sep",
		10 => "Oct",
		11 => "Nov",
		12 => "Dec"
	);
	
	public $day_full = array(
		1 => "Monday",
		2 => "Tuesday",
		3 => "Wednesday",
		4 => "Thursday",
		5 => "Friday",
		6 => "Saturday",
		7 => "Sunday"
	);
	
	public $day_short = array(
		1 => "Mon",
		2 => "Tue",
		3 => "Wed",
		4 => "Thu",
		5 => "Fri",
		6 => "Sat",
		7 => "Sun"
	);

	public $nocomment = "There are no reviews yet.";
	public $related_product = "Related products";
	public $description = "Description";
	public $review = "Reviews";
	public $out_of = "out of";
	public $rated = "Rated";
	public $your_review = "Your Review";
	public $your_rating = "Your Rating";
	public $add_a_review = "Add a review";
	public $name = "Name";
	public $email = "Email";
	public $recently_product = "Recently viewed products";
	public $sign_up_to_newsletter = "Sign up to Newsletter";
	public $sign_up = "Sign up";
	public $enter_your_email = "Enter your email address";
	public $add_to_wishlist = "Add to Wishlist";
	public $add_to_cart = "Add to cart";
	public $add_to_compare = "Add to compare";
	public $shop = "Shop";
	public $home = "Home";
	public $read_more = "read more";
	public $latest_product = "Latest Products";
	public $browse_category = "Browse Categories";
	public $filter = "Filters";
	public $filter_by_price = "Filter by price";
	public $show_all_category = "Show All Categories";
	public $search_for_product = "Search for products";
	public $search = "Search";
	public $search_result_for = "Search results for ";
	public $signup_alert = "Already signup";
	public $signup_complete = "Thank you for signup";
	public $subtotal = "Subtotal";
	public $view_cart = "View cart";
	public $checkout = "Checkout";
	public $your_cart = "Your Cart";
	public $proceed_to_checkout = "Proceed to checkout";
	public $shipping = "Shipping";
	public $flat_rate = "Flat rate";
	public $total = "Total";
	public $cart_total = "Cart totals";
	public $back_to_shopping = "Back to Shopping";
	public $tproduct = "Product";
	public $tprice = "Price";
	public $quantity = "Quantity";
	public $update_cart = "Update cart";
	public $product_remove_cart = "Product is removed from your cart";
	public $confirm_remove_cart = "Remove this product from your cart";
	public $no_register_email = "Email not exists";
	public $found_register_email = "Reset email was sent";
	public $register_or_sign_in = "Register/Sign in";
	public $my_account = "My account";
	public $text_in_login = "Text in login pane";
	public $text_in_register = "Create new account today to reap the benefits of a personalized shopping experience.";
	public $login_error = "Unknown email address or password.";
	public $login_fail_exceed = "Login failed exceeded";
	public $register_error = "Email was already registered.";
	public $register_fail_exceed = "Registration failed exceeded";
	public $product_added = "Product added";
	public $contact_us_call = " 02-114-7629 (5 lines)";
	public $na = "N/A";
	public $password_is_too_short = "รหัสผ่านต้องประกอบด้วย ตัวอักษรพิมพ์เล็กและใหญ่ ตัวเลข และอักขระพิเศษ อย่างละ 1 ตัว ความยาว 8 อักษร";
	public $status = "Status";
	public $tdate = "Date";
	public $order_detail = "Order Detail";
	public $track_your_order = "Track Your Order";
	public $country = "Country";
	public $language = "Language";
	public $tcurrency = "Currency";
	public $currency = "USD";
	public $save = "Save";
	public $all_cat = "All Categories";
	public $contact1 = "Bungkie Call Center Office Hours";
	public $contact2 = " Monday – Friday 9.00 AM – 6.00 PM";
	public $contel = "(+66) 02-114-7629 (5 lines)";
	public $safe_payment = "We are using safe payments";
	public $find_it_fast = "Find It Fast";
	public $customer_care = "Customer Care";
	public $track_order = "Track Order";
	public $wishlist = "Wishlist";
	public $aboutus = "About Us";
	public $contact_us = "CONTACT US";
	public $secure_payment = "Secure Payment";
	public $wwcb = "World Wide Customer Base";
	public $we_ship = "We ship to over 200 countries & regions.";
	public $pay_with = "Pay with the world's top payment methods.";
	public $fast_deli = "Fast Delivery";
	public $what_you_want = "What you want, delivered to where you want.";
	public $obb = "Only Best Brand";
	public $on_sale = "There's always something on sale!";
	public $follow_us = "FOLLOW US";
	public $dashboard = "Dashboard";
	public $torders = "Orders";
	public $addresses = "Addresses";
	public $account_details = "Account details";
	public $order_details = "Order details";
	public $tview = "View";
	public $logout = "Logout";
	public $account_edit = "Account edit";
	public $billing_address = "Billing address";
	public $shipping_address = "Shipping address";
	public $hello = "Hello";
	public $wholesale = "Wholesale Inquiry";
	public $wholesale_submit = "Wholesale Inquiry submitted";
	public $fname = "Firstname";
	public $lname = "Lastname";
	public $company_name = "Company name";
	public $optional = 'optional';
	public $street_address = "Street address";
	public $town_city = "Town/City";
	public $state = "State";
	public $postcode = "Postcode/ZIP";
	public $phone = "Phone";
	public $email_address = "Email address";
	public $contact = "Contact";
	public $note = 'Note';
	public $unit = 'unit';
	public $birthday = "Birthday";
	public $current_password = "Current password (leave blank to leave unchanged)";
	public $new_password = "New password (leave blank to leave unchanged)";
	public $confirm_new_password = "Confirm new password";
	public $password_change = "Password change";
	public $display_name = "Display name";
	public $display_notice = "This will be how your name will be displayed in the account section and in reviews";
	public $default_address = "The following addresses will be used on the checkout page by default.";
	
	
}

	
?>