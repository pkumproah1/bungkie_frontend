require("./bootstrap");

window.Vue = require("vue");

Vue.component(
    "main-component",
    require("./components/MainComponent.vue").default
);
Vue.component(
    "helle-component",
    require("./components/HelleComponent.vue").default
);

Vue.component(
    "slick-component",
    require("./components/SlickComponent.vue").default
);

Vue.component(
    "banner-component",
    require("./components/BannerComponent.vue").default
);

Vue.component(
    "featured-component",
    require("./components/FeaturedComponent.vue").default
);
Vue.component(
    "product-component",
    require("./components/ProductComponent.vue").default
);

window.jQuery = window.$ = require("jquery");

import "./../../node_modules/slick-carousel/slick/slick.css";

const app = new Vue({
    el: "#bungkie"
});
