@extends('layouts.bungkie.category')
@section('breadcrumb')
    @include('layouts.bungkie.2021.breadcrumb', [
        'name' => ucfirst(str_replace('-', ' ', $category_search)) ?? ''
    ])
@endsection
@section('content')
    <section class="section-product-categories">
        <header class="section-header">
            <h1 class="woocommerce-products-header__title page-title">{{ucfirst(str_replace('-', ' ', $category_search)) ?? ''}}</h1>
        </header>
        <div class="woocommerce columns-2">
            <div class="text-center not-found" style="display:none;" v-if="lists.length == 0" >
                <h1 class="text-danger" >{{__('messages.not_found')}}</h1>
            </div>

            <div class="product-carousel" v-if="lists">
                <div class="item list" v-for="(item, index) in lists" :key="index">
                    <div class="util-grid-product-item">
                        <div class="frame">
                            <a :href=" '{{ url('/') . '/product/' }}' + item.product_id + '/' + item.url  ">
                                <div class="content top">
                                    <span class="label success" v-if="item.currency.display_percent > 0">-@{{ item.currency.display_percent }}%</span>
                                    <div class="thumbnail">
                                        <img class="image" :src="item.images">  
                                    </div>
                                    <div class="detail">
                                        <div class="tags">
                                            <span v-if="item.product_type == 'B2C' || item.product_type == 'BOTH'" class="wholesale-item color-00BFB1" >{{__('messages.retail')}}</span><!--Retail -->
                                            <span v-if="item.product_type == 'B2B' || item.product_type == 'BOTH'" class="wholesale-item color-EBB500" >{{__('messages.wholesale_')}}</span>
                                        </div>
                                    </div>
                                    <div class="meta">
                                        <p class="text">@{{ item.name }}</p>
                                    </div>
                                </div>
                            </a>
                            <div class="content bottom">
                                <p class="price sale">@{{ currency }}@{{ item.currency.price }}</p>
                                <p class="price full" v-if="item.currency.display_percent > 0">@{{ currency }}@{{ item.currency.base }}</p>
                                
                                <p class="price full" style="text-decoration: none;" v-if="item.currency.display_percent <= 0">&nbsp;</p>
                                <div class="meta">
                                    <div class="left">
                                        <div class="rating"> 
                                            {{ get_rating_stars(0) }}
                                        </div>
                                    </div>
                                    <!-- <div class="right">
                                        <div class="action">
                                            <a class="link" href="#"><i class="icon tm tm-compare"></i></a>
                                            <a class="link" href="#"><i class="icon tm tm-favorites"></i></a>
                                        </div>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- <div class="d-flex justify-content-center mt-2">
                <button class="btn btn-primary btn-load-more" type="button" style="display:none;" v-if="!disabledBtn" :disabled="loading" @click="loadMore">{{__('messages.view_more')}}</button>
            </div> -->
        </div>
    </section>
@endsection

@section('css')
    <link rel="stylesheet" href="{{ pages_path('homepage/css/homepage.css') }}">
    <link rel="stylesheet" href="{{ pages_path('category/css/index.css') }}">
    <style>
        .wholesale-item {
        color: white;
        font-size: 0.7rem;
        font-weight: 400;
        padding: 0.1rem 0.2rem;
        margin-right: 0.4rem;
        }

        .wholesale-item.color-00BFB1 {
        background-color: #00BFB1;
        }

        .wholesale-item.color-EBB500 {
        background-color: #EBB500;
        }

        .list {         
            width: 50% !important;
            
        }
        @media (min-width: 576px) {
        .list {         
                width: 33% !important;
                
            }
        }          

        @media (min-width: 768px) {
        .list {         
            width: 25% !important;    
            }
        }
        @media (min-width: 1200px) {
        .list {         
            width: 20% !important;
            }
        }
    </style>
@endsection
@section('js')
    <script src="https://cdn.jsdelivr.net/npm/vue@2"></script>
    <script src="{{ pages_path('category/js/index.js') }}"></script>
@endsection