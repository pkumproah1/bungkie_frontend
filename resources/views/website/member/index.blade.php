@extends('layouts.bungkie.homepage')
@section('breadcrumb')
    
    @include('layouts.bungkie.2021.breadcrumb', [
        'name' => __('messages.my_account'),
        
    ])
@endsection

@section('content')
    
    <div class="type-page hentry">
        <header class="entry-header">
            <div class="page-header-caption" style="text-align: center;">
                <h1 class="entry-title">{{__('messages.my_account')}}</h1>
            </div>
        </header><!-- .entry-header -->

        <div class="entry-content">
            <div class="woocommerce">
                <nav class="woocommerce-MyAccount-navigation">
                <?php 
                    $maccount = "is-active";
                ?>
                @include('website.member.menu')
                </nav>


                <div class="woocommerce-MyAccount-content">
                    <div class="woocommerce-notices-wrapper"></div>
                <p>
                {{__('messages.hello')}} <strong>{{$profile['display_name']}}</strong> ({{__('messages.not_to')}} <strong>{{$profile['display_name']}}</strong>? <a href="member/logout">{{__('messages.logout')}}</a>)</p>

                <p>
                {{__('messages.intro')}}</p>

                </div>
            </div>
        </div><!-- .entry-content -->
    </div><!-- .hentry -->
    
@endsection


@section('css')
    <link rel="stylesheet" href="{{ pages_path('homepage/css/homepage.css') }}">
@endsection


@section('js')
    <script src="https://cdn.jsdelivr.net/npm/vue@2"></script>
    <script src="{{ pages_path('homepage/js/homepage.js') }}"></script>
    <script src="{{ pages_path('homepage/js/plugin/slider.js') }}"></script>
@endsection