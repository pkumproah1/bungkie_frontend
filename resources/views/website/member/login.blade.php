@extends('layouts.bungkie.pages')
{{-- breadcrumb --}}
@section('breadcrumb')
    @include('layouts.bungkie.2021.breadcrumb', [
        'name' => __('messages.lb_register_or_login'),
        'cat' => array(
            'url' => 'member',
            'name' => 'Member',
        )
    ])
@endsection
@section('content')
    <div class="type-page hentry">
        <div class="entry-content">
            <div class="woocommerce">
                <div class="customer-login-form">
                    <span class="or-text">{{ __('messages.lb_or') }}</span>
                    <div id="customer_login" class="u-columns col2-set">
                        <div class="u-column1 col-1">
                            <h2>{{ __('messages.btn_login') }}</h2>
                            <form method="post" class="woocomerce-form woocommerce-form-login login">
                                <p class="form-row form-row-wide">
                                    <label for="email">{{ __('messages.email_address') }}
                                        <span class="required">*</span>
                                    </label>
                                    <input type="email" class="input-text form-control" name="email" id="email" required>
                                </p>@csrf
                                <p class="form-row form-row-wide">
                                    <label for="password">{{ __('messages.lb_password') }}
                                        <span class="required">*</span>
                                    </label>
                                    <input class="input-text form-control" minlength="6" maxlength="20" type="password" name="password" id="password" required>
                                    <!-- <span toggle="#password" class="fa fa-fw fa-eye field-icon toggle-password"></span> -->
                                    <label><input class="woocommerce-Input" type="checkbox" onclick="myFunction()"> {{__('messages.show_password')}}</label>
                                </p>
                                <div class="login-footer form-row d-flex">
                                    <button class="woocommerce-Button button">{{ __('messages.btn_login') }}</button>
                                    <div class="woocommerce-LostPassword lost_password ml-auto">
                                        <a href="{{URL::to('/lost_password')}}">{{ __('messages.lost_password') }}</a>
                                    </div>
                                </div>

                                <div class="social-login form-row" data-text="หรือ">
                                    
                                    <button @click="loginFacebook" class="btn-facebook" type="button">
                                        <i class="fa fa-facebook" aria-hidden="true"></i>
                                        Login with facebook
                                    </button>
                                    <button @click="loginGoogle" class="btn-google" type="button">
                                        <i class="fa fa-google" aria-hidden="true"></i>
                                        Login with google
                                    </button>
                                </div>

                            </form>
                        </div>


                        <!-- .col-1 -->
                        <div class="u-column2 col-2">
                            <h2>{{ __('messages.btn_register') }}</h2>
                            <form class="register" method="post">
                                <p class="before-register-text">
                                    {{ __('messages.text_in_register')}}
                                </p>

                                <p class="form-row form-row-wide">
                                    <label for="fname">{{ __('messages.fname') }}
                                        <span class="required">*</span>
                                    </label>
                                    <input type="text" id="fname" name="fname" class="form-control woocommerce-Input woocommerce-Input--text input-text" required>
                                </p>

                                <p class="form-row form-row-wide">
                                    <label for="lname">{{ __('messages.lname') }}
                                        <span class="required">*</span>
                                    </label>
                                    <input type="text" id="lname" name="lname" class="form-control woocommerce-Input woocommerce-Input--text input-text" required>
                                </p>

                                <p class="form-row form-row-wide">
                                    <label for="phone">{{ __('messages.phone') }}
                                        <span class="required">*</span>
                                    </label>
                                    <input type="text" id="phone" name="phone" maxlength="10" minlength="10" class="numeric form-control woocommerce-Input woocommerce-Input--text input-text" required>
                                </p>

                                <p class="form-row form-row-wide">
                                    <label for="reg_email">{{ __('messages.email_address') }}
                                        <span class="required">*</span>
                                    </label>
                                    <input type="email" id="reg_email" name="email" class="form-control woocommerce-Input woocommerce-Input--text input-text" required>
                                </p>
                                <p class="form-row form-row-wide">
                                    <label for="reg_password">{{ __('messages.lb_password') }}
                                        <span class="required">*</span>
                                    </label>
                                    <input type="password" id="reg_password" name="password" minlength="6" maxlength="20" class="password form-control woocommerce-Input woocommerce-Input--text input-text" required>
                                </p>

                                <p class="form-row form-row-wide">
                                    <label for="reg_password">{{ __('messages.lb_conf_password') }}
                                        <span class="required">*</span>
                                    </label>
                                    <input type="password" id="reg_password_confirm" name="cpassword" minlength="6" maxlength="20" class="form-control woocommerce-Input woocommerce-Input--text input-text" required>
                                    <label><input class="woocommerce-Input" type="checkbox" onclick="myFunction2()"> {{__('messages.show_password')}}</label>
                                </p>


                                
                                
                                <p class="form-row">
                                    <button type="submit" class="woocommerce-Button button">{{ __('messages.btn_register') }}</button>
                                </p>
                                
                            </form>
                            <!-- .register -->
                        </div>
                        <!-- .col-2 -->
                    </div>
                    <!-- .col2-set -->
                </div>
                <!-- .customer-login-form -->
            </div>
            <!-- .woocommerce -->
        </div>
        <!-- .entry-content -->
    </div>

    <div class="modal fade" id="confirmData" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{__('messages.data_confirm')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form class="validate-phone-email">
                        <div class="form-group">
                            <label for="email">{{__('messages.email')}} <span class="text-danger">*</span></label>
                            <input type="email" class="form-control" name="confirm-email" id="email" required>
                        </div>
                        <div class="form-group">
                            <label for="phone">{{__('messages.phone')}} <span class="text-danger">*</span></label>
                            <input type="tel" class="form-control" name="confirm-phone" id="phone" required>
                        </div>
                            <input type="hidden" class="form-control" name="confirm-login-type" id="login-type" value="" required>
                        <button type="submit" class="btn btn-primary">{{__('messages.confirm')}}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('css')
    <link rel="stylesheet" href="{{ pages_path('member/css/login.css?version=100') }}">
@endsection
@section('js')
    <script src="https://apis.google.com/js/platform.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/additional-methods.min.js"></script>
    <script src="{{ asset('public/assets/vendor/jquery.inputmask/dist/jquery.inputmask.bundle.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue@2"></script>
    <script src="{{ pages_path('member/js/login.js') }}"></script>
    <script>        
        function myFunction() {
            var x = document.getElementById("password");
            if (x.type === "password") {
                x.type = "text";
            } else {
                x.type = "password";
            }
        }
        function myFunction2() {
            var x = document.getElementById("reg_password");
            var y = document.getElementById("reg_password_confirm");
            if (x.type === "password") {
                x.type = "text";
                y.type = "text";
            } else {
                x.type = "password";
                y.type = "password";
            }
        }
    </script>
@endsection