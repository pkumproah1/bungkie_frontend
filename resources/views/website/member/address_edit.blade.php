@extends('layouts.bungkie.homepage')
@section('breadcrumb')
    
    @include('layouts.bungkie.2021.breadcrumb', [
        'name' => __('messages.addresses'),
        
    ])
@endsection

@section('content')
    
    <div class="type-page hentry">
        <header class="entry-header">
            <div class="page-header-caption" style="text-align: center;">
                <h1 class="entry-title">{{__('messages.addresses')}}</h1>
            </div>
        </header><!-- .entry-header -->

        <div class="entry-content">
            <div class="woocommerce">
                <nav class="woocommerce-MyAccount-navigation">
                @include('website.member.menu')
                </nav>
                <div class="woocommerce-MyAccount-content">
                    <div class="woocommerce-notices-wrapper"></div>
                        <!-- <p>{{__('messages.default_address')}}</p> -->

                        <div class="u-columns woocommerce-Addresses col2-set addresses">

                        
                        <div class="content animate__fadeIn" >
                                    <!-- <div class="header">
                                        <h6><i class="icofont-ui-add"></i> {{__('messages.add_item')}}</h6>
                                        <a href="#" @click.prevent="address.page = 'list'" class="text-primary">{{__('messages.back')}}</a>
                                    </div> -->
                                    <div class="body">
                                        <form class="add-address" method="post">
                                            <div class="add">
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <input type="text" class="form-control" id="name" placeholder="{{__('messages.fname')}}" name="name" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <input type="text" maxlength="10" class="form-control numeric" name="phone" placeholder="{{__('messages.phone')}}" id="phone" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="form-group">
                                                            <textarea class="form-control" name="address" rows="3" placeholder="{{__('messages.addresses')}}" required></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <select name="province" id="province" class="form-control chosen" required>
                                                                @if (isset($province) && !empty($province))
                                                                    <option value="">{{__('messages.town_city')}}</option>
                                                                    @foreach ($province as $key => $item)
                                                                        <option value="{{ $item['province_code'] }}">
                                                                            {{ $item['name'] }}
                                                                        </option>
                                                                    @endforeach
                                                                @endif
                                                                
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <select name="city" id="city" class="form-control chosen" required>
                                                                <option value="">{{__('messages.amphur')}}</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <select name="district" id="district" class="form-control chosen" required>
                                                                <option value="">{{__('messages.tumbon')}}</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <input class="form-control" type="text" name="zipcode" id="zipcode" readonly required>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                            <div class="footer border-top d-flex pt-2">
                                                <button type="submit" @click="addAddress" class="btn btn-primary ml-auto">{{__('messages.save')}}</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                    </div>

                    
                </div>
            </div>
        </div><!-- .entry-content -->
    </div><!-- .hentry -->
    
@endsection


@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css">
    <link href="{{ asset('public/assets/vendor/chosen/chosen.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ pages_path('cart/css/checkout.css') }}">
@endsection

@section('js')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue@2"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/additional-methods.min.js"></script>
    <script src="{{ asset('public/assets/vendor/chosen/chosen.jquery.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendor/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js') }}"></script>
    <script src="{{ pages_path('member/js/addressAdd.js') }}"></script>
    @php
        $lang = (\Session::get('lang') == 'en') ? 'en' : 'th';
    @endphp
    
    <script src="{{ asset('public/assets/location/location_'. $lang .'.js') }}"></script>
@endsection