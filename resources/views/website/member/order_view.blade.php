@extends('layouts.bungkie.homepage')
@section('breadcrumb')
    
    @include('layouts.bungkie.2021.breadcrumb', [
        'name' => __('messages.torders'),
        
    ])
@endsection

@section('content')
    
    <div class="type-page hentry">
        <header class="entry-header">
            <div class="page-header-caption" style="text-align: center;">
                <h1 class="entry-title">{{__('messages.torders')}}</h1>
            </div>
        </header><!-- .entry-header -->

        <div class="entry-content">
            <div class="woocommerce">
                <nav class="woocommerce-MyAccount-navigation">
                @include('website.member.menu')
                </nav>
                <div class="woocommerce-MyAccount-content">
                    <!-- <div class="woocommerce-notices-wrapper"></div> -->

                    @if(isset($order) && !empty($order))
                        @foreach($order as $key => $o) 
                            @if($o['order_id'] == $order_id)
                                <p><button type="button"  class="close" onclick="location.href='{{url('member/order')}}'">&times;</button></p>
                                <p ><h4 style="text-align: right">{{__('messages.reference_id')}} {{$o['reference_id']}}</h4></p>
                                <p><h4>{{__('messages.shipping_address')}}</h4>
                                    {{$o['customer']['name']}}<BR>
                                    {{$o['customer']['address']}}<BR>
                                    {{$o['customer']['phone']}}<BR>
                                    {{$o['customer']['email']}}<BR>
                                </p>      
                                @if(isset($o['products']) && !empty($o['products']))
                                <p>
                                    <table class="woocommerce-orders-table woocommerce-MyAccount-orders shop_table shop_table_responsive my_account_orders account-orders-table table-borderless">                                    
                                        @php
                                        $total_price = 0;
                                        $total_ship = 0;
                                        
                                        @endphp
                                        <tbody>
                                            @foreach($o['products'] as $product_key => $product_detail)                                        
                                                @php 
                                                    $total_ship += $product_detail['price']['shipping'];
                                                @endphp
                                            <tr>
                                                <td  data-title="Product image" rowspan="2" >
                                                    <img class="col-12 col-lg-6 col-md-6 d-block"  src="{{$product_detail['images']}}" alt="figure" />
                                                </td>
                                                <td  data-title="Product name"><h4 style="text-align: left">{{$product_detail['name']}}</h4></td>
                                                <td data-title="Price" rowspan="2">
                                                @php 
                                                    $total_price += ($product_detail['price']['front_price']*$product_detail['price']['qty'])-$product_detail['price']['discount'];
                                                @endphp
                                                    <p class="price">{{$o['currency']}}{{number_format($product_detail['price']['front_price']-$product_detail['price']['discount'], 2)}} </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td data-title="{{__('messages.quantity')}}"><h4 style="text-align: left">x {{$product_detail['price']['qty']}}</h4></td>
                                            </tr>
                                            @endforeach
                                            <tr >
                                                <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-actions" data-title="{{__('messages.total_product')}}" colspan="2">
                                                    <h4 class="d-none d-md-block" style="text-align: right">{{__('messages.total_product')}}</h4>
                                                </td>
                                                <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-number" data-title="{{$o['currency']}}{{number_format($total_price, 2)}}">
                                                    <h4 class="d-none d-md-block" style="text-align: right">{{$o['currency']}}{{number_format($total_price, 2)}}</h4>
                                                </td>
                                            </tr>
                                            <tr class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-actions" data-title="{{__('messages.total_product')}}">
                                                <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-actions" data-title="{{__('messages.shipping_rate')}}" colspan="2">
                                                    <h4 class="d-none d-md-block" style="text-align: right">{{__('messages.shipping_rate')}}</h4>
                                                </td>
                                                <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-number" data-title="{{$o['currency']}}{{number_format($total_ship, 2)}}">
                                                    <h4 class="d-none d-md-block" style="text-align: right">{{$o['currency']}}{{number_format($total_ship, 2)}}<h4>
                                                </td>
                                            </tr>
                                            <tr >
                                                <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-actions" data-title="{{__('messages.total_price')}}" colspan="2">
                                                    <h4 class="d-none d-md-block" style="text-align: right">{{__('messages.total_price')}}</h4>
                                                </td>
                                                <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-number" data-title="{{$o['currency']}}{{number_format($total_ship+$total_price, 2)}}">
                                                    <h4 class="d-none d-md-block" style="text-align: right">{{$o['currency']}}{{number_format($total_ship+$total_price, 2)}}</h4>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </p>
                                @endif
                            @endif
                        @endforeach
                    @endif                    
                </div>
            </div>
        </div><!-- .entry-content -->
    </div><!-- .hentry -->
    
@endsection


@section('css')
    <link rel="stylesheet" href="{{ asset('public/assets/vendor/froala_editor_3.2.5/css/froala_editor.pkgd.css') }}">
    <link rel="stylesheet" href="{{ pages_path('product/css/detail.css') }}">
    <link rel="stylesheet" href="{{ pages_path('category/css/index.css') }}">
@endsection


@section('js')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue@2"></script>
    <script src="{{ pages_path('homepage/js/homepage.js') }}"></script>
@endsection