@extends('layouts.bungkie.homepage')
@section('breadcrumb')
    
    @include('layouts.bungkie.2021.breadcrumb', [
        'name' => __('messages.addresses'),
        
    ])
@endsection

@section('content')
    
    <div class="type-page hentry">
        <header class="entry-header">
            <div class="page-header-caption" style="text-align: center;">
                <h1 class="entry-title">{{__('messages.addresses')}}</h1>
            </div>
        </header><!-- .entry-header -->

        <div class="entry-content">
            <div class="woocommerce">
                <nav class="woocommerce-MyAccount-navigation">
                @include('website.member.menu')
                </nav>
                <div class="woocommerce-MyAccount-content">
                <div class="address">
                                <div class="content animate__fadeIn" v-if="address.page == 'select'">
                                    <div class="header">
                                        <h6><i class="icofont-location-pin"></i> {{__('messages.shipping_address')}}</h6>
                                        <a href="#" @click.prevent="address.page = 'list'" class="text-info" v-if="address.default.info"><font size="4">{{__('messages.change')}}</font></a>
                                    </div>
                                    <BR>
                                    <div class="body">
                                        <span class="selected" v-if="address.default.info">
                                            <i class="icofont-checked"></i>
                                            <strong><font size="4">@{{ address.default.info.display_name }}, @{{ address.default.info.phone }}</strong>
                                            @{{ address.default.info.address }}, @{{ address.default.info.province }}, @{{ address.default.info.city }}, @{{ address.default.info.district }}, @{{ address.default.info.zipcode }}</font>
                                        </span>
                                        <small>
                                            <font size="4">
                                                <a href="#" @click.prevent="address.page = 'add'" class="text-danger" v-if="!address.default.info">{{__('messages.click_to_change')}}</a>
                                            </font>
                                        </small>
                                    </div>
                                </div>

                                <div class="content animate__fadeIn" v-if="address.page == 'list'">
                                    <div class="header">
                                        <h6><i class="icofont-address-book"></i> {{__('messages.selected')}}</h6>
                                        <font size="4">
                                            <a href="#" @click.prevent="address.page = 'select'" class="text-primary">{{__('messages.back')}}</a>&nbsp;&nbsp;&nbsp;
                                            <a href="#" @click.prevent="address.page = 'add'" class="text-dagner">{{__('messages.add')}}</a>
                                        </font>
                                    </div>
                                    <BR>
                                    <div class="body">
                                        <div class="lists">
                                            <ul class="nav flex-column">
                                                <li class="nav-item" v-for="(item, index) in address.list" :key="index" v-if="address.list">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="selected" v-model="address.default.id" :id="'address_' + item.address_id" :value="item.address_id">
                                                        <label class="form-check-label" :for="'address_' + item.address_id">
                                                        <font size="4">
                                                            @{{ item.display_name }}, @{{ item.phone }}
                                                            @{{ item.address }}, @{{ item.province }}, @{{ item.city }}, @{{ item.district }}, @{{ item.zipcode }} &nbsp;&nbsp;&nbsp;
                                                            <a title="{{__('messages.edit')}}" href="#" @click="addressEdit(item.address_id)" @click.prevent="address.page = 'edit'" class="text-primary"><i class="icofont-edit-alt"></i></a>&nbsp;&nbsp;&nbsp;
                                                            <a title="{{__('messages.delete')}}" v-if="address.default.id != item.address_id" href="#"  @click="addressDelete(item.address_id)" class="text-dagner"><i class="icofont-ui-delete"></i></a>
                                                        </font>
                                                        </label>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <div class="content animate__fadeIn" v-if="address.page == 'add' || address.page == 'edit'">
                                    <div class="header">
                                        <h6><i class="icofont-ui-add"></i> {{__('messages.add_item')}}</h6>
                                        <font size="4"><a href="#" @click.prevent="address.page = 'list'" class="text-primary">{{__('messages.back')}}</a></font>
                                    </div>
                                    <BR>
                                    <div class="body">
                                        <form class="add-address" method="post">
                                            <div class="add">
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <input type="text" class="form-control" id="name" placeholder="{{__('messages.fname')}}" name="name" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <input type="text" maxlength="10" class="form-control numeric" name="phone" placeholder="{{__('messages.phone')}}" id="phone" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="form-group">
                                                            <textarea class="form-control" name="address" id="address" rows="3" placeholder="{{__('messages.addresses')}}" required></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <select name="province" id="province" class="form-control chosen" required>
                                                                @if (isset($province) && !empty($province))
                                                                    <option value="">{{__('messages.town_city')}}</option>
                                                                    @foreach ($province as $key => $item)
                                                                        <option value="{{ $item['province_code'] }}">
                                                                            {{ $item['name'] }}
                                                                        </option>
                                                                    @endforeach
                                                                @endif
                                                                
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <select name="city" id="city" class="form-control chosen" required>
                                                                <option value="">{{__('messages.amphur')}}</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <select name="district" id="district" class="form-control chosen" required>
                                                                <option value="">{{__('messages.tumbon')}}</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <input class="form-control" type="text" name="zipcode" id="zipcode" readonly required>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                            <div class="footer border-top d-flex pt-2">
                                                <button type="submit" @click="addAddress(address_id)" class="btn btn-primary ml-auto">{{__('messages.save')}}</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                                
                            </div>

                    
                </div>
            </div>
        </div><!-- .entry-content -->
    </div><!-- .hentry -->
    
@endsection


@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css">
    <link href="{{ asset('public/assets/vendor/chosen/chosen.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ pages_path('cart/css/checkout.css') }}">
@endsection

@section('js')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue@2"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/additional-methods.min.js"></script>
    <script src="{{ asset('public/assets/vendor/chosen/chosen.jquery.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendor/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js') }}"></script>
    <script src="{{ pages_path('member/js/address.js') }}"></script>
    @php
        $lang = (\Session::get('lang') == 'en') ? 'en' : 'th';
    @endphp
    <script src="{{ asset('public/assets/location/location_'. $lang .'.js') }}"></script>
@endsection