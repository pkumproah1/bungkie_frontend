@extends('layouts.bungkie.homepage')
@section('breadcrumb')
    
    @include('layouts.bungkie.2021.breadcrumb', [
        'name' => __('messages.account_details'),
        
    ])
@endsection

@section('content')
    
    <div class="type-page hentry">
        <header class="entry-header">
            <div class="page-header-caption" style="text-align: center;">
                <h1 class="entry-title">{{__('messages.account_details')}}</h1>
            </div>
        </header><!-- .entry-header -->

        <div class="entry-content">
            <div class="woocommerce">
                <nav class="woocommerce-MyAccount-navigation">
                @include('website.member.menu')
                </nav>
                <div class="woocommerce-MyAccount-content">
                    <div class="woocommerce-notices-wrapper"></div>
                    <form class="update-detail" method="post"  >
                        @csrf
                        <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                            <label for="account_first_name">{{__('messages.fname')}}&nbsp;<span class="required">*</span></label>
                            <input type="text" class="woocommerce-Input woocommerce-Input--text input-text form-control" name="account_firstname" id="accountFirstname" minlength="6" autocomplete="accountFirstname" value="{{$profile['first_name']}}" required />
                        </p>
                        <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                            <label for="account_last_name">{{__('messages.lname')}}&nbsp;<span class="required">*</span></label>
                            <input type="text" class="woocommerce-Input woocommerce-Input--text input-text form-control" name="account_lastname" id="accountLastname" minlength="6" autocomplete="accountLastname" value="{{$profile['last_name']}}" required/>
                        </p>
                        <div class="clear"></div>
                        
                        <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                            <label for="account_display_name">{{__('messages.display_name')}}&nbsp;<span class="required">*</span></label>
                            <input type="text" class="woocommerce-Input woocommerce-Input--text input-text form-control" name="account_display_name" id="accountDisplayName" minlength="6" required value="{{$profile['display_name']}}" /> <span><em>{{__('messages.display_notice')}}</em></span>
                        </p>
                        <div class="clear"></div>
                        
                        <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                            <label for="phone">{{__('messages.phone')}}&nbsp;<span class="required">*</span></label>
                            <input type="text" class="woocommerce-Input woocommerce-Input--text input-text numeric form-control" name="account_phone" id="accountPhone" minlength="9" maxlength="15" autocomplete="accountPhone" value="{{$profile['phone']}}" required/>
                        </p>

                        <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                            <label for="birthday">{{__('messages.birthday')}}<span class="required">*</span></label>
                            <input type="text" class="woocommerce-Input woocommerce-Input--text input-text form-control" name="account_birthday" id="accountBirthday" autocomplete="accountBirthday" required value="{{$profile['birthday'] ?? ''}}" />
                        </p>
                        <div class="clear"></div>
                        
                        <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                            <label for="account_email">{{__('messages.email_address')}}&nbsp;</label>
                            <input type="email" class="woocommerce-Input woocommerce-Input--email input-text form-control" name="account_email" id="accountEmail" autocomplete="accountEmail" required value="{{$profile['email']}}" readonly/>
                        </p>
                        
                        <p>
                            <button type="submit" class="woocommerce-Button button" name="save_account_details" value="{{__('messages.save')}}">{{__('messages.save')}}</button>
                            <input type="hidden" name="action" value="save_account_details" />
                        </p>
                    </form>
                </div>
            </div>
        </div><!-- .entry-content -->
    </div><!-- .hentry -->
    
@endsection


@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css">
    <link href="{{ asset('public/assets/vendor/chosen/chosen.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ pages_path('cart/css/checkout.css') }}">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('js')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue@2"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/additional-methods.min.js"></script>
    <script src="{{ asset('public/assets/vendor/chosen/chosen.jquery.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendor/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js') }}"></script>
    <script src="{{ pages_path('member/js/detail.js') }}"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $( function() {
            $( "#accountBirthday" ).datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "yy-mm-dd",
                yearRange: "-100:+0",
                minDate: "-100Y"
            });
        } );
    </script>
@endsection
@php
    $lang = (\Session::get('lang') == 'en') ? 'en' : 'th';
@endphp
<script src="{{ asset('public/assets/location/location_'. $lang .'.js') }}"></script>