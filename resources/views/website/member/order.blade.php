@extends('layouts.bungkie.homepage')
@section('breadcrumb')
    
    @include('layouts.bungkie.2021.breadcrumb', [
        'name' => __('messages.torders'),
        
    ])
@endsection

@section('content')
    
    <div class="type-page hentry">
        <header class="entry-header">
            <div class="page-header-caption" style="text-align: center;">
                <h1 class="entry-title">{{__('messages.torders')}}</h1>
            </div>
        </header><!-- .entry-header -->

        <div class="entry-content">
            <div class="woocommerce">
                <nav class="woocommerce-MyAccount-navigation">
                @include('website.member.menu')
                </nav>
                <div class="woocommerce-MyAccount-content">
                    <div class="woocommerce-notices-wrapper"></div>
                        <table class="woocommerce-orders-table woocommerce-MyAccount-orders shop_table shop_table_responsive my_account_orders account-orders-table">
                            <thead>
                                <tr>
                                    <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-number"><span class="nobr">{{__('messages.reference_id')}}</span></th>
                                    <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-date"><span class="nobr">{{__('messages.tdate')}}</span></th>
                                    <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-total"><span class="nobr">{{__('messages.total')}}</span></th>
                                    <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-total"><span class="nobr">{{__('messages.pay_way')}}</span></th>
                                    <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-total"><span class="nobr">{{__('messages.pay_status')}}</span></th>
                                    <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-actions"><span class="nobr">Actions</span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="(item, index) in lists" :key="index" class="woocommerce-orders-table__row woocommerce-orders-table__row--status-on-hold order">
                                    <td id="" class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-number" data-title="{{__('messages.reference_id')}}">
                                        <span id="reference_id">@{{item.reference_id}}</span>
                                    </td>
                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-date" data-title="{{__('messages.tdate')}}">
                                        <span id="tranaction_date">@{{item.transaction_datetime}}</span>
                                    </td>
                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-total" data-title="{{__('messages.total')}}">
                                        <span id="amount">@{{item.currency}}@{{item.amount}} </span>
                                    </td>
                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-total" data-title="{{__('messages.pay_way')}}">
                                        <span id="payment_channel">@{{item.payment_channel}}</span>
                                    </td>
                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-total" data-title="{{__('messages.pay_status')}}">
                                        <span id="payment_status">@{{item.channel_response_desc}}</span>
                                    </td>                                    
                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-actions" data-title="{{__('messages.tview')}}">
                                        <a class="woocommerce-button button view" :href=" '{{ url('/') . '/member/order/view/' }}' + item.order_id">{{__('messages.tview')}}</a>
                                    </td>
                                </tr>
                                <!-- <tr>
                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-actions"  colspan="6">
                                        <button class="btn btn-primary" type="button" v-if="!disabledBtn" :disabled="loading"  @click="loadMore">โหลดเพิ่ม</button>
                                    </td>
                                </tr> -->
                            </tbody>
                        </table>
                    </div>                        
                </div>
            </div>
        </div><!-- .entry-content -->
    </div><!-- .hentry -->
    
@endsection


@section('css')
    <link rel="stylesheet" href="{{ asset('public/assets/vendor/froala_editor_3.2.5/css/froala_editor.pkgd.css') }}">
    <link rel="stylesheet" href="{{ pages_path('product/css/detail.css') }}">
    <link rel="stylesheet" href="{{ pages_path('category/css/index.css') }}">
@endsection


@section('js')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue@2"></script>
    <script src="{{ pages_path('member/js/order.js') }}"></script>
@endsection