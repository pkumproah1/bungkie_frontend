@extends('layouts.bungkie.homepage')
@section('breadcrumb')
    
    @include('layouts.bungkie.2021.breadcrumb', [
        'name' => __('messages.account_details'),
        
    ])
@endsection

@section('content')
    
    <div class="type-page hentry">
        <header class="entry-header">
            <div class="page-header-caption" style="text-align: center;">
                <h1 class="entry-title">{{__('messages.password_change')}}</h1>
            </div>
        </header><!-- .entry-header -->

        <div class="entry-content">
            <div class="woocommerce">
                <nav class="woocommerce-MyAccount-navigation">
                @include('website.member.menu')
                </nav>
                <div class="woocommerce-MyAccount-content">
                    <div class="woocommerce-notices-wrapper"></div>
                    <form class="woocommerce-EditAccountForm edit-account" method="post">
                    

                        <fieldset>

                            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                <label for="password_current">{{__('messages.current_password')}}</label>
                                <input type="password" class="woocommerce-Input woocommerce-Input--password input-text form-control" maxlength="20" minlength="6" name="passwordOld" id="passwordCurrent" autocomplete="off"  required/>
                            </p>
                            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                <label for="password_1">{{__('messages.new_password')}}</label>
                                <input type="password" class="woocommerce-Input woocommerce-Input--password input-text form-control" maxlength="20" minlength="6" name="password" id="password" autocomplete="off"  required/>
                            </p>
                            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                <label for="password_2">{{__('messages.confirm_new_password')}}</label>
                                <input type="password" class="woocommerce-Input woocommerce-Input--password input-text form-control" maxlength="20" minlength="6" name="conf-password" id="confPassword" autocomplete="off" required/>
                                
                            </p>
                            <label><input class="woocommerce-Input" type="checkbox" onclick="myFunction()"> {{__('messages.show_password')}}</label>
                        </fieldset>
                        
                        <div class="clear"></div>
                        

                        @csrf
                        <p>

                            <button type="submit" class="woocommerce-Button button">{{__('messages.save')}}</button>
                            <input type="hidden" name="email_log" id="email_log" value="{{$profile['email']}}">
                        </p>

                        </form>

                </div>
            </div>
        </div><!-- .entry-content -->
    </div><!-- .hentry -->
    
@endsection
@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css">
    <link href="{{ asset('public/assets/vendor/chosen/chosen.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ pages_path('cart/css/checkout.css') }}">
@endsection

@section('js')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue@2"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/additional-methods.min.js"></script>
    <script src="{{ asset('public/assets/vendor/chosen/chosen.jquery.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendor/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js') }}"></script>
    <script src="{{ pages_path('member/js/password.js') }}"></script>
    @php
        $lang = (\Session::get('lang') == 'en') ? 'en' : 'th';
    @endphp
    <script src="{{ asset('public/assets/location/location_'. $lang .'.js') }}"></script>
    <script>
        function myFunction() {
            var x = document.getElementById("passwordCurrent");
            var y = document.getElementById("password");
            var z = document.getElementById("confPassword");
            if (x.type === "password") {
                x.type = "text";
                y.type = "text";
                z.type = "text";
            } else {
                x.type = "password";
                y.type = "password";
                z.type = "password";
            }
        }
    </script>
@endsection