<ul>
    <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--dashboard {{$maccount}}">
        <a href="{{URL::to('/member')}}">{{__('messages.dashboard')}}</a>
    </li>
    <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--orders {{$morder}}">
        <a href="{{URL::to('/member/order')}}">{{__('messages.torders')}}</a>
    </li>
    <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--edit-address {{$maddress}}">
        <a href="{{URL::to('/member/address')}}">{{__('messages.addresses')}}</a>
    </li>
    <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--edit-account {{$mdetail}}">
        <a href="{{URL::to('/member/detail')}}">{{__('messages.account_details')}}</a>
    </li>
    @if($password_change)
    <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--edit-password {{$mpassword}}">
        <a href="{{URL::to('/member/password')}}">{{__('messages.password_change')}}</a>
    </li>
    @endif
    <li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--customer-logout">
        <a href="{{URL::to('/member/logout')}}" >{{__('messages.logout')}}</a>
    </li>
</ul>