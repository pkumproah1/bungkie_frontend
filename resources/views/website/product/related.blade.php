@php
$carousel_args = array(
	'slidesToShow'		=> 8,
	'slidesToScroll'	=> 5,
	'dots'				=> false,
	'arrows'			=> true,
	'prevArrow'			=> '<a href="#"><i class="tm tm-arrow-left"></i></a>',
	'nextArrow'			=> '<a href="#"><i class="tm tm-arrow-right"></i></a>',
	'appendArrows'		=> '#tm-related-products-carousel .custom-slick-nav',
	'responsive'		=> array(
		array(
			'breakpoint'	=> 575,
			'settings'		=> array(
				'slidesToShow'		=> 2,
				'slidesToScroll'	=> 2
			)
		),
		array(
			'breakpoint'	=> 768,
			'settings'		=> array(
				'slidesToShow'		=> 3,
				'slidesToScroll'	=> 3
			)
		),
		array(
			'breakpoint'	=> 992,
			'settings'		=> array(
				'slidesToShow'		=> 4,
				'slidesToScroll'	=> 2
			)
		),
		array(
			'breakpoint'	=> 1200,
			'settings'		=> array(
				'slidesToShow'		=> 5,
				'slidesToScroll'	=> 3
			)
		)
	)
);
$lim = 'limit 0, 10';
@endphp

<!-- <div class="tm-related-products-carousel section-products-carousel" id="tm-related-products-carousel" data-ride="tm-slick-carousel" data-wrap=".products" data-slick="{{htmlspecialchars(json_encode($carousel_args), ENT_QUOTES, 'UTF-8')}}"> -->
	<section class="related" >
		<header class="section-header">
			<h2 class="section-title">{{__('messages.related_product')}}</h2>
			@if(count($products) > 18)<nav class="custom-slick-nav"><a class="link-more" href="{{url('categories').'/'.$category_id.'/'.$category_url}}">{{__('messages.view_more')}}<ion-icon class="icon" name="chevron-forward-outline"></ion-icon></a></nav>@endif
		</header><!-- .section-header -->
    
    <div class='row'>
		<div class="products ">
            @php
                $round = 1;
                shuffle($products);
            @endphp
                    @foreach($products as $product)  
                        @if($product['product_id'] != $product_id && $round <= 18)
                        <div class="util-grid-product-item" style="width: 180px">
                            <div class="frame">
                                <a href="{{ url('/') . '/product/'.$product['product_id'].'/'.$product['url'] }} ">
                                    <div class="content top">
                                        @if($product['currency']['display_percent'] > 0)<span class="label success">-{{$product['currency']['display_percent']}}%</span>@endif
                                        <div class="thumbnail">
                                            <img class="image" src="{{$product['images']}}" >  
                                        </div>
                                        <div class="detail">
                                            <div class="tags">
                                                @if($product['product_type'] == 'B2C' || $product['product_type'] == 'BOTH')
                                                    <span class="wholesale-item color-00BFB1" >{{__('messages.retail')}}</span>
                                                @endif
                                                @if($product['product_type'] == 'B2B' || $product['product_type'] == 'BOTH')
                                                    <span class="wholesale-item color-EBB500" >{{__('messages.wholesale_')}}</span>
                                                @endif
                                            </div>
                                        </div>
                                                            
                                        <div class="meta">
                                            <p class="text">{{$product['name']}}</p>
                                        </div>
                                    </div>
                                </a>
                                <div class="content bottom">
                                    <p class="price sale">{{$product['currency']['price']}}</p>
                                    @if($product['currency']['display_percent'] > 0)<p class="price full">{{$product['currency']['base']}}</p>@endif
                                    
                                    <!-- <p class="price full" style="text-decoration: none;">&nbsp;</p> -->
                                    <div class="meta">
                                        <div class="left">
                                            <div class="rating"> 
                                                {{ get_rating_stars(0) }}
                                            </div>
                                        </div>
                                        <!-- <div class="right">
                                            <div class="action">
                                                <a class="link" href="#"><i class="icon tm tm-compare"></i></a>
                                                <a class="link" href="#"><i class="icon tm tm-favorites"></i></a>
                                            </div>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        @php 
                            $round++;
                        @endphp
                        @endif
                    @endforeach
                <!-- <div class="d-flex justify-content-center mt-4">
                    <button class="btn btn-primary btn-load-more" type="button" >โหลดเพิ่ม</button>
                </div> -->
        </div>
    </div>
	</section><!-- .single-product-wrapper -->
<!-- </div>.tm-related-products-carousel -->


