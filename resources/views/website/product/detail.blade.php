@extends('layouts.bungkie.pages')
{{-- breadcrumb --}}
@section('breadcrumb')
    @if(isset($product))
    @include('layouts.bungkie.2021.breadcrumb', [
        'name' => $product['name'] ?? '',
        'cat' => array(
            'url' => str_replace('/', '/'.$product['category']['category_id'].'/', $product['category']['url']) ?? '',
            'name' => $product['category']['name'] ?? '',
        )
    ])
    @endif
@endsection

@section('content')
<section id="product-detail" >
    <div class="row" id="foto_close" style="display: none;"><button type="button"  class="close" onclick="document.getElementById('foto').style.display = 'none';document.getElementById('foto_close').style.display = 'none'" data-dismiss="foto">&times;</button></div>
    <div class="fotorama" id="foto" data-width="100%"
       data-height="100%"  data-arrows="true" data-click="true" data-swipe="true" style="display: none;">
        @foreach($product['images'] as $key => $image)
            <img src="{{ $image }}" alt="{{ $product['name'] ?? '' }}" title="{{ $product['name'] ?? '' }}">                                   
        @endforeach
    </div>
    <div class="product product-type-simple" >
        <div class="single-product-wrapper">
            <div class="product-images-wrapper thumb-count-4">
                @if($product['currency']['display_percent'])
                <span class="onsale">-
                    <span class="woocommerce-Price-amount amount">
                        {{ $product['currency']['display_percent'] ?? '' }}%
                    </span>
                </span>
                @endif
                <!-- .onsale -->
                <div id="techmarket-single-product-gallery" class="techmarket-single-product-gallery techmarket-single-product-gallery--with-images techmarket-single-product-gallery--columns-4 images" data-columns="4">
                    <div class="techmarket-single-product-gallery-images" data-ride="tm-slick-carousel" data-wrap=".woocommerce-product-gallery__wrapper" data-slick="{&quot;infinite&quot;:false,&quot;slidesToShow&quot;:1,&quot;slidesToScroll&quot;:1,&quot;dots&quot;:false,&quot;arrows&quot;:false,&quot;asNavFor&quot;:&quot;#techmarket-single-product-gallery .techmarket-single-product-gallery-thumbnails__wrapper&quot;}">
                        <div class="woocommerce-product-gallery woocommerce-product-gallery--with-images woocommerce-product-gallery--columns-4 images" data-columns="4">
                            <figure class="woocommerce-product-gallery__wrapper">
                                @if($product['images'])
                                    @foreach($product['images'] as $key => $image)
                                    <div data-thumb="{{ $image }}" class="woocommerce-product-gallery__image">
                                        <a style="cursor: zoom-in;" href="#" onclick="document.getElementById('foto').style.display = 'block'; document.getElementById('foto_close').style.display = 'block'">
                                            <img src="{{ $image }}" class="attachment-shop_single size-shop_single wp-post-image" alt="">                                   
                                        </a>
                                    </div>
                                    @endforeach
                                @endif
                            </figure>
                        </div>
                    </div>

                    <div class="techmarket-single-product-gallery-thumbnails" data-ride="tm-slick-carousel" data-wrap=".techmarket-single-product-gallery-thumbnails__wrapper" data-slick="{&quot;infinite&quot;:false,&quot;slidesToShow&quot;:4,&quot;slidesToScroll&quot;:1,&quot;dots&quot;:false,&quot;arrows&quot;:true,&quot;vertical&quot;:true,&quot;verticalSwiping&quot;:true,&quot;focusOnSelect&quot;:true,&quot;touchMove&quot;:true,&quot;prevArrow&quot;:&quot;&lt;a href=\&quot;#\&quot;&gt;&lt;i class=\&quot;tm tm-arrow-up\&quot;&gt;&lt;\/i&gt;&lt;\/a&gt;&quot;,&quot;nextArrow&quot;:&quot;&lt;a href=\&quot;#\&quot;&gt;&lt;i class=\&quot;tm tm-arrow-down\&quot;&gt;&lt;\/i&gt;&lt;\/a&gt;&quot;,&quot;asNavFor&quot;:&quot;#techmarket-single-product-gallery .woocommerce-product-gallery__wrapper&quot;,&quot;responsive&quot;:[{&quot;breakpoint&quot;:765,&quot;settings&quot;:{&quot;vertical&quot;:false,&quot;horizontal&quot;:true,&quot;verticalSwiping&quot;:false,&quot;slidesToShow&quot;:4}}]}">
                        <figure class="techmarket-single-product-gallery-thumbnails__wrapper">
                            @if($product['images'])
                                @foreach($product['images'] as $key => $image)
                                <figure data-thumb="{{ $image }}" class="techmarket-wc-product-gallery__image">
                                    <img  src="{{ $image }}" class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" alt="">
                                </figure>
                                @endforeach
                            @endif
                        </figure>
                    </div>
                </div>
            </div>
            {{-- detail product --}}
            <div class="summary entry-summary">
                <div class="single-product-header">
                    <h1 class="product_title entry-title">{{ $product['name'] ?? '' }}</h1>
                    <span class="sku_wrapper">
                        SKU: <span class="sku">{{ $product['sku'] ?? '' }}</span>
                    </span>
                    @if($product['rating'])
                    <div class="rating-and-sharing-wrapper">
                        <div class="woocommerce-product-rating">
                            <div class="star-rating">
                                <span style="width:{{ $product['rating']['percent'] }}%">Rated
                                    <strong class="rating">5.00</strong> out of 5 based on
                                    <span class="rating">1</span> customer rating
                                </span>
                            </div>
                            <a rel="nofollow" class="woocommerce-review-link" href="#reviews">(<span class="count">{{ $product['rating']['count'] }}</span> customer review)</a>
                        </div>
                    </div>
                    @endif
                    <!-- <a class="add-to-wishlist" href="#"> Add to Wishlist</a> -->
                </div>

                <!-- .single-product-header -->
                <div class="single-product-meta">
                    <div class="brand">
                        <a href="#">
                            @php 
                                $img = (file_exists($product['seller']['logo'])) ? $product['seller']['logo'] : asset('public/assets/images/favicon.png');
                            @endphp
                            <img alt="{{ $product['seller']['name'] }}" src="{{ $img }}">
                        </a>
                    </div>

                    <div class="cat-and-sku">
                        <span class="posted_in categories">
                            <!-- <a rel="tag" href="{{ url('/shop/') . '/' .$product['seller']['link'] }}">{{ $product['seller']['name'] }}</a> -->
                            <a rel="tag" href="#">{{ $product['seller']['name'] }}</a>
                        </span>
                    </div>
                    <div class="product-label">
                        <div class="ribbon label green-label">
                            <span>A+</span>
                        </div>
                    </div>
                </div>
                <div class="woocommerce-product-details__short-description mb-2 fr-view">
                    {!! strip_tags($product['short_description']) ?? '' !!}
                </div>
                <div class="product-actions-wrapper">
                    <div class="product-actions">
                        <p class="price">
                            @if($product['currency']['discount'])
                            <del>
                                <span class="woocommerce-Price-amount amount">
                                    {{ $currency }}{{ $product['currency']['base'] }}
                                </span>
                            </del>
                            @endif
                            <ins>
                                <span class="woocommerce-Price-amount amount">
                                    {{ $currency }}{{ $product['currency']['price'] }}
                                </span>
                            </ins>
                        </p>

                        <script>
                            fbq('track', 'AddToCart', {
                                currency: '{{ $fbq_currency }}', 
                                value: '{{ $product['currency']['price'] }} '
                                });
                        </script>

                        @if (isset($product['option']) && !empty($product['option']))
                            <div class="group-option mb-3">
                            @foreach($product['option'] as $key => $option)
                                <div class="option">
                                    <div class="name">{{ $option['name'] }}</div>
                                    <div class="items" id="items_{{ $key }}">
                                        <ul class="nav">
                                            @if (isset($option['option']) && !empty($option['option']))
                                                @foreach($option['option'] as $k => $item)
                                                <li>
                                                    <a href="#" data-option="{{ $option['name'] }}" data-value="{{ $item }}" class="{{ ($k == 0) ? 'selected' : '' }}">{{ $item }}</a>
                                                </li>
                                                @endforeach
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                            @endforeach
                            </div>
                        @endif
                        <div class="quantity">
                            @if($product['product_type'] === "B2C" || $product['product_type'] === "BOTH") 
                                <div class="left">
                                    <label for="quantity-input">{{ __('messages.quantity') }}</label>
                                    <div class="add-qty">
                                        <button type="button" @click="decrement" class="decrement">
                                            <span>-</span>
                                        </button>
                                        <span class="qty" data-qty="{{ $product['in_stock'] ?? '' }}">@{{ qty }}</span>
                                        <button type="button" @click="increment" class="increment">
                                            <span>+</span>
                                        </button>
                                    </div>
                                    <div class="item-qty">
                                        <span>{{ __('messages.all_product') }} {{ $product['in_stock'] ?? '' }} {{ __('messages.unit') }}</span>
                                    </div>
                                </div>
                            @endif
                            <div class="action-button">
                                @if ($product['in_stock'] > 0)
                                    @if($product['product_type'] === "B2C" || $product['product_type'] === "BOTH") 
                                        @if (session()->get('logged') === true)
                                            <button type="button" @click="addCart({{ $product['product_id'] }})" class="single_add_to_cart_button button alt" name="add-to-cart" type="submit">
                                                <i class="fa fa-shopping-cart"></i>
                                                {{ __('messages.add_to_cart') }}
                                            </button>
                                        @else  
                                            @php 
                                                \Session::put('logintogo', url()->current());
                                            @endphp
                                            <a class="single_add_to_cart_button button alt" href="{{ url('member') }}">
                                                <i class="fa fa-shopping-cart"></i>
                                                {{ __('messages.add_to_cart') }}
                                            </a>
                                        @endif
                                    @endif
                                @else
                                <button type="button" class="single_add_to_cart_button button alt disabled" disabled name="add-to-cart" type="submit">
                                    <i class="fa fa-shopping-cart"></i>
                                    {{ __('messages.out_of_stock') }}
                                </button>
                                @endif
                                @if($product['product_type'] === "B2B" || $product['product_type'] === "BOTH") 
                                <a href="{{url('wholesale/'.$product['product_id'])}}" class="single_add_to_cart_button button alt">
                                    <i class="fa fa-comment"></i> 
                                    {{ __('messages.wholesale') }}
                                </a>
                                @endif
                            </div>
                        </div>
                        <!-- <a class="add-to-compare-link mt-5" href="#">{{ __('messages.add_to_compare') }}</a>-->
                    </div>
                </div>
            </div>
        </div>
        
        {{-- Detail and review --}}
        @includeChild('detail.product-detail-tab', ['product' => $product])   
        @if(isset($products) && !empty($products))
            @includeChild('related', ['products' => $products])   
        @endif
            
        
    </div>
</section>
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('public/assets/vendor/froala_editor_3.2.5/css/froala_editor.pkgd.css') }}">
    <link rel="stylesheet" href="{{ pages_path('product/css/detail.css?version=100') }}">
    <link  href="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css" rel="stylesheet">
    <style>
        .wholesale-item {
        color: white;
        font-size: 0.7rem;
        font-weight: 400;
        padding: 0.1rem 0.2rem;
        margin-right: 0.4rem;
        }

        .wholesale-item.color-00BFB1 {
        background-color: #00BFB1;
        }

        .wholesale-item.color-EBB500 {
        background-color: #EBB500;
        }

        .list {         
            width: 50% !important;
            
        }
        @media (min-width: 576px) {
        .list {         
                width: 33% !important;
                
            }
        }          

        @media (min-width: 768px) {
        .list {         
            width: 25% !important;    
            }
        }
        @media (min-width: 1200px) {
        .list {         
            width: 20% !important;
            }
        }

        @media (min-width: 1400px) {
        .list {         
            width: 12.3334% !important;    
            }
        }
    </style>
@endsection

@section('js')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue@2"></script>
    <script src="{{ pages_path('product/js/detail.js?version=100') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js"></script>
@endsection