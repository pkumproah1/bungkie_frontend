@extends('layouts.bungkie.pages')
{{-- breadcrumb --}}
@section('breadcrumb')
    @if(isset($product))
    @include('layouts.bungkie.2021.breadcrumb', [
        'name' => $product['name'] ?? '',
        'cat' => array(
            'url' => '#',
            'name' => __('messages.wholesale'),
        )
    ])
    @endif
@endsection

@section('content')
<section id="productList" class="mt-5">
    <div class="bungkie">
        <div id="primary" class="content-area">
            <main id="main" class="site-main">
                <div id="post-7" class="post-7 page type-page status-publish hentry">
                    <header class="entry-header">
                        <div class="page-header-caption">
                            <h1 class="entry-title">{{ $product['name'] }}</h1>
                        </div>
                    </header><!-- .entry-header -->
                    <div class="entry-content">
                        <div class="woocommerce" id="wholesale">
                            <div class="woocommerce-MyAccount-content" style="width: 100%;">
                                <form  class="woocommerce-EditAccountForm wholesale-form" method="post" autocomplete="off">
                                    @csrf
                                    <div class="row">
                                        <div class="col-lg-4 left">
                                            <div class="thumbnail">
                                                <div class="show-img">
                                                    <img src="{{$product['images'][0]}}">  
                                                </div>
                                            </div>
                                            <div class="woocommerce-form-row woocommerce-form-row--wide">
                                                <h6>{{ __('messages.description') }}</h6>
                                                <p class="d-block">
                                                    {!! strip_tags($product['short_description']) !!}
                                                </p>
                                            </div>
                                        </div>

                                        <div class="col-lg-8">
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <fieldset>
                                                        <div class="woocommerce-form-row woocommerce-form-row--first">
                                                            <label for="wholesale-product-amount">{{__('messages.quantity')}}&nbsp;<span class="required">*</span> </label>
                                                            <input type="number" class="woocommerce-Input woocommerce-Input--text input-text form-control" name="qty" id="wholesaleProductAmount" required />
                                                        </div>
                                                        <div class="woocommerce-form-row woocommerce-form-row--last">
                                                            <label for="wholesale-product-note">{{__('messages.unit')}}&nbsp;<span class="required">*</span></label>
                                                            <input type="text" class="woocommerce-Input woocommerce-Input--text input-text form-control" name="unit" id="wholesaleProductUnit" required />
                                                        </div>

                                                        <div class="woocommerce-form-row woocommerce-form-row--first">
                                                            <label for="wholesale_contact_company">{{__('messages.company_name')}}<span class="required">*</span>&nbsp;</label>
                                                            <input type="text" class="woocommerce-Input woocommerce-Input--text input-text form-control" name="company_name" id="wholesaleContactCompany" autocomplete="off" required/>
                                                        </div>
                                                    
                                                        <div class="woocommerce-form-row woocommerce-form-row--first">
                                                            <label for="wholesale_product_contact_country">{{__('messages.addresses')}}&nbsp;<span class="required">*</span></label>
                                                            <textarea  class="form-control" name="address" rows="2" placeholder="{{__('messages.addresses')}}" required></textarea>
                                                        </div>

                                                        
                                                        <div class="woocommerce-form-row woocommerce-form-row--last">
                                                            <label for="wholesale_product_contact_country">{{__('messages.town_city')}}&nbsp;<span class="required">*</span></label>
                                                            <select name="province" id="province" class="country_select form-control chosen" required>
                                                                @if (isset($province) && !empty($province))
                                                                    <option value="">{{__('messages.town_city')}}</option>
                                                                    @foreach ($province as $key => $item)
                                                                        <option value="{{ $item['province_code'] }}">
                                                                            {{ $item['name'] }}
                                                                        </option>
                                                                    @endforeach
                                                                @endif
                                                            </select>
                                                        </div>

                                                        <div class="woocommerce-form-row woocommerce-form-row--first">
                                                            <label for="wholesale_product_contact_country">{{__('messages.amphur')}}&nbsp;<span class="required">*</span></label>
                                                            <select name="city" id="city" class="country_select form-control chosen" required>
                                                                <option value="">{{__('messages.amphur')}}</option>
                                                            </select>
                                                        </div>
                                                        <div class="woocommerce-form-row woocommerce-form-row--last">
                                                            <label for="wholesale_product_contact_country">{{__('messages.tumbon')}}&nbsp;<span class="required">*</span></label>
                                                            <select name="district" id="district" class="country_select form-control chosen" required>
                                                                <option value="">{{__('messages.tumbon')}}</option>
                                                            </select>
                                                        </div>
                                                        <div class="woocommerce-form-row woocommerce-form-row--first">
                                                            <label for="wholesale_product_contact_country">{{__('messages.postcode')}}&nbsp;<span class="required">*</span></label>
                                                            <input class="form-control" type="text" name="zipcode" id="zipcode" readonly required>
                                                        </div>
                                                    </fieldset>
                                                </div>

                                                <div class="col-lg-6">
                                                    <div class="woocommerce-form-row woocommerce-form-row--first">
                                                        <label for="wholesale_contact_first_name">{{__('messages.fname')}}&nbsp;<span class="required">*</span></label>
                                                        <input type="text" class="woocommerce-Input woocommerce-Input--text input-text form-control" name="fname" id="wholesaleContactFirstName" autocomplete="on" required />
                                                    </div>
                                                    <div class="woocommerce-form-row woocommerce-form-row--last">
                                                        <label for="wholesale_contact_last_name">{{__('messages.lname')}}&nbsp;<span class="required">*</span></label>
                                                        <input type="text" class="woocommerce-Input woocommerce-Input--text input-text form-control" name="lname" id="wholesaleContactLastName" autocomplete="off" required />
                                                    </div>
                                                    <div class="woocommerce-form-row woocommerce-form-row--first">
                                                        <label for="wholesale_contact_phone">{{__('messages.phone')}}&nbsp;<span class="required">*</span></label>
                                                        <input type="number" class="woocommerce-Input woocommerce-Input--text input-text form-control numeric" name="phone" id="wholesale_contact_phone" autocomplete="off"  value="" required/>
                                                    </div>
                                                    <div class="woocommerce-form-row woocommerce-form-row--last">
                                                        <label for="wholesale_contact_email">{{__('messages.email')}}&nbsp;<span class="required">*</span></label>
                                                        <input type="email" class="woocommerce-Input woocommerce-Input--text input-text form-control" name="email" id="wholesale_contact_email" autocomplete="off" value="" required/>
                                                    </div>
                                                    <div class="woocommerce-form-row woocommerce-form-row--first">
                                                        <label for="wholesale_contact_phone">LINE&nbsp;</label>
                                                        <input type="text" class="woocommerce-Input woocommerce-Input--text input-text form-control" name="line" id="wholesale_line" autocomplete="off" value="" />
                                                    </div>
                                                    <div class="woocommerce-form-row woocommerce-form-row--last">
                                                        <label for="wholesale_contact_email">SKYPE&nbsp;</label>
                                                        <input type="text" class="woocommerce-Input woocommerce-Input--text input-text form-control" name="skype" id="wholesale_skype" autocomplete="off" value="" />
                                                    </div>                            

                                                    <div class="woocommerce-form-row woocommerce-form-row--last">
                                                        <label for="wholesale_contact_postcode">Whatsapp&nbsp;</label>
                                                        <input type="text" class="woocommerce-Input woocommerce-Input--text input-text form-control" name="whatsapp" id="wholesale_whatsapp" autocomplete="off" value="" />
                                                    </div>             

                                                    <div class="woocommerce-form-row woocommerce-form-row--first">
                                                        <label for="wholesale_product_contact_country">{{__('messages.remark')}}&nbsp;</label>
                                                        <textarea class="form-control" name="remark" rows="3" placeholder="{{__('messages.remark')}}" ></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="button-footer">
                                                        <button type="submit" class="btn button" id="save_wholesale"  name="save_wholesale">
                                                            {{__('messages.save')}}
                                                        </button>
                                                    </div>
                                                    <input type="hidden" name="product_id" value="{{$product['product_id']}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div><!-- .entry-content -->
                </div><!-- #post-## -->

            </main><!-- #main -->
        </div><!-- #primary -->
    </div>
</section>
@endsection

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css">
    <link href="{{ asset('public/assets/vendor/chosen/chosen.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ pages_path('product/css/wholesale.css') }}">
@endsection

@section('js')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue@2"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/additional-methods.min.js"></script>
    <script src="{{ asset('public/assets/vendor/chosen/chosen.jquery.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendor/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js') }}"></script>
    <script src="{{ pages_path('product/js/wholesale.js') }}"></script>
    @php
        $lang = (\Session::get('lang') == 'en') ? 'en' : 'th';
    @endphp
    <script src="{{ asset('public/assets/location/location_'. $lang .'.js') }}"></script>
@endsection