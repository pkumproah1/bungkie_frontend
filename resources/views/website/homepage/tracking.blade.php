@extends('layouts.bungkie.homepage')
@section('breadcrumb')
    
    @include('layouts.bungkie.2021.breadcrumb', [
        'name' => __('messages.track_your_order'),
        
    ])
@endsection

@section('content')
    
    <div class="type-page hentry">
        <header class="entry-header">
            <div class="page-header-caption" style="text-align: center;">
                <h1 class="entry-title">{{__('messages.track_your_order')}}</h1>
            </div>
        </header><!-- .entry-header -->

        <div class="entry-content">
            <div class="woocommerce" style="text-align: center;">
                <form class="track_order" v-on:submit.prevent>

                    <p>{{__('messages.tracking_detail')}}</p>

                    <p class="form-row form-row-first" >
                        <label for="orderid">Order ID</label> 
                        <input  type="text" required minlength="17" placeholder="Order ID" id="orderid" name="orderid" class=" form-control input-text" >
                        
                    </p>

                    <div class="clear"></div>

                    <p class="form-row form-row-last">
                        <input type="button" @click="search" class="btn" name="track" value="{{__('messages.track_btn')}}" />
                    </p>
                </form><!-- .track_order -->
                </div>
                <table style="display:none;" class="woocommerce-orders-table woocommerce-MyAccount-orders shop_table shop_table_responsive my_account_orders account-orders-table" id="result">
                    <thead>
                        <tr>
                            <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-number"><span class="nobr">{{__('messages.tdate')}}</span></th>
                            <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-date"><span class="nobr">{{__('messages.activity')}}</span></th>
                            
                        </tr>
                    </thead>
                    <tbody>

                        <tr v-for="(item, index) in lists" :key="index" class="woocommerce-orders-table__row woocommerce-orders-table__row--status-on-hold order">
                            <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-number" data-title="Order">
                                <span id="reference_id">@{{ item.created_at }}</span>
                            </td>
                            <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-date" data-title="Date">
                                <span id="tranaction_date">@{{ item.message }}</span>
                            </td>
                            
                        </tr>

                    </tbody>
                </table>
            </div><!-- .woocommerce -->
        </div><!-- .entry-content -->
    </div><!-- .hentry -->
    
@endsection


@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css">
    <link href="{{ asset('public/assets/vendor/chosen/chosen.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ pages_path('homepage/css/homepage.css') }}">
@endsection


@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/additional-methods.min.js"></script>
    <script src="{{ asset('public/assets/vendor/chosen/chosen.jquery.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendor/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue@2"></script>
    <script src="{{ pages_path('homepage/js/plugin/slider.js') }}"></script>
    <script src="{{ pages_path('product/js/tracking.js') }}"></script>
@endsection