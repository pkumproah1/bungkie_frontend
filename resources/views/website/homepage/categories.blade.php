<section id="featured" class="mt-5">
    <div class="bungkie">
        <div class="bungkie-header">
            <span class="text">Featured <b>Categories</b></span>
        </div>
        <div class="bungkie-content">
            {{-- <nav id="categories-carousel-arrows" class="util-custom-slick-nav center"></nav> --}}
            <div ref="slick" class="categories-carousel">
                @if(isset($category_all) && !empty($category_all))
                    @foreach($category_all as $key => $cat)
                        <div id="categories-carousel-items" class="items">
                            <a href="{{ url('/') . '/categories/'.$cat['id'].'/' . $cat['url']  }}" title="{{ $cat['name'] }}" >
                                <div class="icon-frame">
                                    <div class="icon">
                                        <div class="img">
                                            <img src="{{ $cat['image'] }}" alt="{{ $cat['name'] }}" />
                                        </div>
                                    </div>
                                    <p class="text">{{ $cat['name'] }}</p>
                                </div>
                            </a>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
</section>