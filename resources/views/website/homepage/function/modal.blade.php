@if(isset($modal) && $modal['status'] === true)
<div class="modal image-modal" data-modal tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    &times;
                </button>
                <a href="{{ $modal['url'] ?? '' }}" class="image">
                    <img src="{{ $modal['image'] ?? '' }}" alt="{{ $modal['name'] ?? '' }}">
                </a>
            </div>
        </div>
    </div>
</div>
@endif
