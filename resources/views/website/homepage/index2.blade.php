<!DOCTYPE html>
<html lang="en" class="hydrated"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style data-styles="">ion-icon{visibility:hidden}.hydrated{visibility:inherit}</style>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
<title>Bungkie | Jump into Happy Jungle | ซื้อขายออนไลน์ B2B2C</title>
<meta name="baseUrl" content="{{ url('/') }}">
<meta name="baseApi" content="{{ config('constants.web_api') }}">
@if(\Session::has('logged'))
    <meta name="token" content="{{ \Session::get('token') }}">
@endif
<meta name="csrf-token" content="ruaIEjWw1HGB8AADnOPRwiWznFjszCIalm8YLpBh" />
<meta name="robots" content="noindex, nofollow">
<meta name="keywords" content="bungkie, e-commerce, ขายของออนไลน์, ซื้อของออนไลน์, ซื้อขายออนไลน์ B2B2C">
<meta name="description" content="Bungkie แพลตฟอร์ม E-commerce ที่นำเสนอรูปแบบการขายสินค้าและบริการแบบออนไลน์ ผสานความต้องการของผู้ซื้อและผู้ขายไว้ด้วยกันทั้งในรูปแบบของ B2B2C">
<meta property="og:title" content="Bungkie | Jump into Happy Jungle | ซื้อขายออนไลน์ B2B2C">
<meta property="og:description" content="Bungkie แพลตฟอร์ม E-commerce ที่นำเสนอรูปแบบการขายสินค้าและบริการแบบออนไลน์ ผสานความต้องการของผู้ซื้อและผู้ขายไว้ด้วยกันทั้งในรูปแบบของ B2B2C">
<meta property="og:image" content="{{ asset('public/assets/images/bungkie.png') }}">
<meta property="og:url" content="{{ url()->current() }}"">
<meta property="og:site_name" content="Bungkie.com">
<meta name="twitter:card" content="{{ asset('public/assets/images/bungkie.png') }}">
<meta name="twitter:image:alt" content="Bungkie แพลตฟอร์ม E-commerce ที่นำเสนอรูปแบบการขายสินค้าและบริการแบบออนไลน์ ผสานความต้องการของผู้ซื้อและผู้ขายไว้ด้วยกันทั้งในรูปแบบของ B2B2C">
<meta name="twitter:site" content="Bungkie | Jump into Happy Jungle | ซื้อขายออนไลน์ B2B2C">
<link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />
<link rel="canonical" href="{{ url()->current() }}"/>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="{{asset('public/assets/css/new_template/fonts.css')}}" rel="stylesheet">
<link href="{{asset('public/assets/css/new_template/bootstrap.min.css')}}" rel="stylesheet">
<link href="{{asset('public/assets/css/new_template/custom.css')}}" rel="stylesheet">
<script src="https://unpkg.com/ionicons@5.0.0/dist/ionicons.js"></script>
<link rel="stylesheet" href="{{asset('public/assets/css/new_template/swiper-bundle.min.css')}}">
<link href="{{asset('public/assets/css/new_template/index.css')}}" rel="stylesheet">
<script type="module" src="{{asset('public/assets/js/new_template/ionicons.esm.js')}}" data-stencil-namespace="ionicons"></script><script nomodule="" src="{{asset('public/assets/js/new_template/ionicons.js')}}" data-stencil-namespace="ionicons"></script>
<link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,900" rel="stylesheet">
<link rel="shortcut icon" href="{{ asset('public/assets/favicon.ico') }}" type="image/x-icon">
<link rel="icon" href="{{ asset('public/assets/favicon.ico') }}" type="image/x-icon">
<!-- Google Tag Manager -->
<script>(function (w, d, s, l, i) {
        w[l] = w[l] || []; w[l].push({
            'gtm.start':
                new Date().getTime(), event: 'gtm.js'
        }); var f = d.getElementsByTagName(s)[0],
            j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-NFRKHZJ');</script>
<!-- End Google Tag Manager -->
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NFRKHZJ" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '2835383683395893');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=2835383683395893&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
<header class="site-nav" style="background-image: url({{asset('public/assets/images/new_template/BGred.png')}});">
<div id="app" class="container">
    <nav class="nav">
        <div class="topbar">
            <div class="row justify-content-between align-items-center">
                <div class="col d-none d-md-block">
                    <div class="links">
                        <a href="#howto_banner" class="item">{{__('messages.sell_on_bungkie')}}</a>
                        <a href="#" class="item">{{__('messages.help_center')}}</a>
                        <a href="{{url('/contactus')}}" class="item">{{__('messages.contact_us')}}</a>
                        @if(!\Session::has('logged'))
                        <a href="{{url('/tracking')}}" class="item">{{__('messages.track_your_order')}}</a>
                        @endif
                        <span href="#" class="socials">
                            <a href="https://www.facebook.com/bungkieofficial" class="link"><img class="icon" src="{{asset('public/assets/images/new_template/FB@2x.png')}}" alt="Facebook"></a>
                            <a href="https://lin.ee/ON8SJz5" class="link"><img class="icon" src="{{asset('public/assets/images/new_template//Line@2x.png')}}" alt="Line"></a>
                            <a href="https://www.instagram.com/bungkieofficial/" class="link"><img class="icon" src="{{asset('public/assets/images/new_template/IG@2x.png')}}" alt="Instagram"></a>
                            <a href="https://www.youtube.com/channel/UCCv8pvQDWx-rkBUYh9FzJqg?view_as=subscriber" class="link"><img class="icon" src="{{asset('public/assets/images/new_template/Youtube@2x.png')}}" alt="Youtube"></a>
                        </span>
                    </div>
                </div>
                @php
                    $language = (App::getLocale() == 'th') ? array(
                        'name' => 'ภาษาไทย',
                        'icon' => 'public/assets/images/new_template/TH_Flag@2x.png'
                    ) : array(
                        'name' => 'English',
                        'icon' => 'public/assets/images/new_template/EN_Flag@2x.png'
                    );
                @endphp
                <div class="col-auto">
                    <div class="dropdown">
                        <a class="btn dropdown-toggle" title="{{ $language['name'] }}" data-toggle="dropdown" class="dropdown-toggle"  href="#" role="button" id="dropdownLanguage" data-bs-toggle="dropdown" aria-expanded="false">
                            <img class="icon" src="{{asset($language['icon'])}}"> <span class="language-text">{{ $language['name'] }}</span>
                        </a>
                        <ul role="menu" class="dropdown-menu" aria-labelledby="dropdownLanguage">
                            <li class="menu-item animate-dropdown">
                                <a title="thailand" class="dropdown-item" href="#" data-language="th" data-name="ภาษาไทย" data-current="{{ App::getLocale() }}">
                                    <img class="icon" src="{{asset('public/assets/images/new_template/TH_Flag@2x.png')}}"><span class='text'> ภาษาไทย</span>
                                </a>
                            </li>
                            <li class="menu-item animate-dropdown">
                                <a title="english" class="dropdown-item" href="#" data-language="en" data-name="English" data-current="{{ App::getLocale() }}">
                                    <img class="icon" src="{{asset('public/assets/images/new_template/EN_Flag@2x.png')}}"><span class="text"> English</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="dropdown">
                        <a class="btn dropdown-toggle" href="#" role="button" id="dropdownCurrency" data-bs-toggle="dropdown" title="{{ $currency }}" data-toggle="dropdown" class="dropdown-toggle"aria-expanded="false" style="padding-top: 0.55rem; padding-bottom: 0.5rem;">
                        @php
                            $currency = (session()->get('currency') === 'usd') ? "USD" : "THB";
                        @endphp
                            {{$currency}}
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dropdownCurrency">
                            <li><a class="dropdown-item" href="#" title="Baht (THB)" data-currency="thb" data-name="Baht (THB)" data-current="{{ session()->get('currency') }}"><span class="text">Baht (THB)</span></a></li>
                            <li><a class="dropdown-item" href="#" title="Dollar (US)" data-currency="usd" data-name="Dollar (US)" data-current="{{ session()->get('currency') }}"><span class="text">Dollar (US)</span></a></li>
                        </ul>
                    </div>
                </div>
                <!-- <div class="col-auto d-md-none">
                    <img class="menu-icon" src="{{asset('public/assets/images/new_template/menu.png')}}" alt="menu" data-menu-toggle="menu">
                </div> -->
            </div>
        </div>
        <div class="core-content">
            <div class="row justify-content-between align-items-center">
                <div class="col-auto">
                    <img class="logo" src="{{asset('public/assets/images/new_template/Bungkie_logo@2x.png')}}" alt="logo">
                    <!-- <img class="d-none d-md-inline-block menu-icon" src="{{asset('public/assets/images/new_template/menu.png')}}" alt="menu" data-menu-toggle="menu"> -->
                </div>
                <div class="d-none d-md-block col col-md-12 col-lg order-md-first order-lg-0 pb-md-4 pb-lg-0 ">
                    <form class="search-form" method='get' action="{{ url('search') }}">
                        <div class="input-group">
                            <input type="text" name="s" class="form-control" value="{{$search ?? ""}}" placeholder="{{__('messages.what_are_you_looking_for')}}">
                            <button class="btn" type="submit"><img class="icon" src="{{asset('public/assets/images/new_template/search3.png')}}" alt="search"></button>
                            <input type="hidden" name="cat_id" value="0">
                        </div>
                    </form>
                </div>
                <div class="col-auto">
                    <div class="icon-links">
                        <!-- <a class="link">
                            <img class="icon" src="{{asset('public/assets/images/new_template/favorite@2x.png')}}" alt="favorite">
                            <span class="text">{{__('messages.save')}}</span>
                        </a> -->
                        <a class="link" v-if="carts"  href="{{ url('cart') }}">
                            <span class="noti-icon" ></span>
                            <img class="icon" src="{{asset('public/assets/images/new_template/card@2x.png')}}" alt="cart">
                            <span class="text">{{__('messages.cart')}}</span>
                        </a>
                        @if(!\Session::has('logged'))
                        <a class="link" href="{{url('member/')}}">
                            <img class="icon" src="{{asset('public/assets/images/new_template/login-signin@2x.png')}}" alt="log-in | log-out">
                            <span class="text">{{ __('messages.lb_register_or_login') }}</span>
                        </a>
                        @else
                        <a class="link" href="{{url('/member/')}}">
                            <img class="icon" src="{{asset('public/assets/images/new_template/login-signin@2x.png')}}" alt="log-in | log-out">
                            <span class="text">{{__('messages.my_account')}}</span>
                        </a>
                        @endif
                        <a class="d-md-none link" id="nav-search-link" onclick="openPane();">                                
                            <span class="search-icon">
                                <img class="icon search" src="{{asset('public/assets/images/new_template/search2@2x.png')}}" alt="{{__('messages.search')}}" />
                                <img class="icon close" src="{{asset('public/assets/images/new_template/close.png')}}" alt="{{__('messages.search')}}" />
                            </span>
                            <span class="text">{{__('messages.search')}}</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="d-md-none nav-search-collapse">
            <form class="search-form" method='get' action="{{ url('search') }}">
                <div class="input-group">
                    <input type="text" class="form-control" name='s' placeholder="{{__('messages.what_are_you_looking_for')}}" value="{{$search ?? ''}}">
                    <button class="btn" type="submit"><img class="icon" src="{{asset('public/assets/images/new_template/search3.png')}}" alt="search" /></button>
                    <input type="hidden" name="cat_id" value="0">
                </div>
            </form>
        </div>
    </nav>
</div>
</header>
    <main class="index-page pt-4 pt-lg-5">
        <div class="section shift-up level-1">
            <div class="top-banners ">
                <div class="container">
                    <div class="row g-2">
                    @php 
                        $rr = 0; 
                        $prod = $product['category'];
                        $alive_cat = array();
                        //shuffle($category_all);
                    @endphp
                    @foreach($prod as $key => $pr)
                        @php 
                            $alive_cat[$rr] = $pr['category']['id'];
                            $rr++;
                        @endphp
                    @endforeach
                        <div class="position-relative d-none d-lg-block col-lg">
                            <div class="parent-categories">
                                <p class="title">Shop By Categories</p>
                                @if(isset($category_all) && !empty($category_all))
                                    @foreach($category_all as $key => $cat)
                                    @if(in_array($cat['id'], $alive_cat))
                                    <a href="#" class="link" onmouseover="showChildCategories('category{{$cat['id']}}')" >
                                        <p class="text">{{ $cat['name'] }} <ion-icon class="icon" name="chevron-forward-outline"></ion-icon>
                                        </p>
                                    </a>
                                    @endif
                                    @endforeach
                                @endif
                            </div>                            
                            @if(isset($category_all) && !empty($category_all))
                                @foreach($category_all as $key => $cat)
                                <div id="category{{$cat['id']}}" style="display: none;" class="child-categories" onmouseenter="clearShowChildCategoriesDelay('category{{$cat['id']}}')" onmouseleave="hideChildCategories('category{{$cat['id']}}')">
                                    <div class="row">
                                    @php
                                        $img_array = array();
                                    @endphp
                                    @if(isset($cat['parent']) && !empty($cat['parent']) )
                                        @foreach($cat['parent'] as $c_key => $c_parent)
                                        @if(in_array($c_parent['id'], $alive_cat))
                                        <div class="col-12 col-md-6 col-lg-4 child-links">
                                            <div class="group-section">
                                                @php 
                                                    $img_array[] = $c_parent;
                                                @endphp
                                                <p class="title"><a href="{{url('categories/'.$c_parent['id'].'/'.$c_parent['url'])}}">{{ $c_parent['name'] }}</a></p>
                                                @if(isset($c_parent['parent']) && !empty($c_parent['parent']))
                                                    @foreach($c_parent as $key_lv3 => $cat_lv3) 
                                                        <div class="links">
                                                        @if(is_array($cat_lv3) && $key_lv3 == "parent")
                                                            @foreach($cat_lv3 as $last_key => $last_lv) 
                                                                @if(in_array($last_lv['id'], $alive_cat))
                                                                <a href="{{url('categories/'.$last_lv['id'].'/'.$last_lv['url'])}}" class="link">
                                                                    <p class="text">{{$last_lv['name']}}</p>
                                                                </a>
                                                                @endif
                                                            @endforeach
                                                        @endif    
                                                        </div>                                                        
                                                    @endforeach
                                                @endif            
                                            </div>
                                        </div>
                                        @endif
                                        @endforeach
                                    @endif
                                        <!-- <div class="col-12 banners">
                                            <div class="row g-2">
                                                @if($img_array)
                                                    @for($idx=0;$idx<count($img_array) && $idx < 4;$idx++)
                                                    <div class="col-6 col-md-3">
                                                        <a href="{{url('categories/'.$img_array[$idx]['id'].'/'.$img_array[$idx]['name'])}}">
                                                            <span class="frame">
                                                                <img class="banner" src="{{$img_array[$idx]['image']}}" alt="">
                                                            </span>
                                                        </a>
                                                    </div>
                                                    @endfor
                                                @endif
                                            </div>
                                        </div> -->
                                    </div>
                                </div>
                                @endforeach
                            @endif
                        </div>
                        @php
                            $imgs = array(
                                'WW1515x755.jpg' => url('search?s=what&cat_id=0'),
                                'vistra1515x755.jpg' => url('search?s=vistra&cat_id=0'),
                                'sunnyC1515x755.jpg' => url('search?s=sunny c&cat_id=0'),
                                'happyYourLife1515x755.jpg' => url('#'),
                                'happy-jungle1515x755.jpg' => url('#'),
                                'bungkieSleepy1515x755.jpg' => url('#'),
                                'betagro1515x755.jpg'  => url('search?s=betagro&cat_id=0')
                            );
                            //shuffle($imgs);
                        @endphp
                        <div class="col-12 col-lg-auto head-banners">
                            <div class="head-banners-swiper">
                                <div class="swiper-container">
                                    <div class="swiper-wrapper" id="swiper-wrapper-675aa2df6c516ce7">
                                        @foreach($imgs as $key => $img)
                                        <div class="swiper-slide">
                                            <a href="{{$img}}">
                                                <div class="thumbnail">
                                                    <div class="frame">
                                                        <img class="image full-screen" src="{{ url('/') . '/public/assets/images/banner/'.$key}}" alt="">
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        @endforeach
                                    </div>
                                    <div class="swiper-pagination swiper-pagination-clickable swiper-pagination-bullets swiper-pagination-bullets-dynamic" style="width: 85px;"><span class="swiper-pagination-bullet swiper-pagination-bullet-active swiper-pagination-bullet-active-main" tabindex="0" role="button" aria-label="Go to slide 1" style="left: 34px;"></span><span class="swiper-pagination-bullet swiper-pagination-bullet-active-next" tabindex="0" role="button" aria-label="Go to slide 2" style="left: 34px;"></span><span class="swiper-pagination-bullet swiper-pagination-bullet-active-next-next" tabindex="0" role="button" aria-label="Go to slide 3" style="left: 34px;"></span><span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 4" style="left: 34px;"></span><span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 5" style="left: 34px;"></span><span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 6" style="left: 34px;"></span></div>
                                <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section shift-up level-1 d-lg-none">
            <div class="categories-icon">
                <div class="container">
                    <div class="categories-icon-swiper">
                        <div class="swiper-container">
                            <div class="swiper-wrapper">                     
                            @if(isset($category_all) && !empty($category_all))                                          
                                @php
                                    $mk = 1;
                                    $index_display = 1;
                                    $cat_index = 0;
                                @endphp
                                @while($cat_index < count($category_all))
                                
                                <div class="swiper-slide">
                                    <div class="row">
                                    @for($i=$cat_index;$i<count($category_all);$i++)
                                        @php
                                            if($index_display > 2) {
                                                $index_display = 1;
                                                break;                                            
                                            }
                                        @endphp
                                        @if($index_display <= 2)
                                        @if(in_array($category_all[$i]['id'], $alive_cat))
                                        <div class="col-12">
                                            <div class="item">
                                                <a href="{{url('categories/'.$category_all[$i]['id'].'/'.$category_all[$i]['url'])}}" class="link">
                                                    <img class="icon" src="@php if(file_exists('public/assets/images/new_template/'.$category_all[$i]['id'].'.png')) echo asset('public/assets/images/new_template/'.$category_all[$i]['id'].'.png'); else echo $category_all[$i]['image']; @endphp" alt="" />
                                                    <p class="name">{{$category_all[$i]['name']}}</p>
                                                </a>
                                            </div>
                                        </div>
                                        @endif
                                        @php
                                            $index_display++;
                                            $cat_index++;
                                        @endphp
                                        @endif                                        
                                    @endfor                                      
                                    </div>
                                </div>
                                
                                @endwhile
                            @endif
                            </div>
                            <!-- <div class="swiper-pagination"></div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section shift-up level-1">
            <div class="news">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-lg-12 col-md-6">
                            <a href="https://today.line.me/th/v2/amp/article/wXqEnY">
                                <div class="item">
                                    <div class="row">
                                        <div class="col-12">
                                            <img class="icon" src="{{asset('public/assets/images/new_template/news.png')}}" alt="title" />
                                        </div>
                                        <div class="col-12">
                                            <div class="frame">
                                                <p class="text">SME D Bank จับมือ Bungkie.com สัมมนาออนไลน์ หนุนเอสเอ็มอีเจาะตลาดอีคอมเมิร์ซอินเดีย</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if(isset($product) && !empty($product))
        <div class="section">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="flash-sale">
                            <div class="head">@php shuffle($product['category'][1]['data']); @endphp 
                                <div class="row justify-content-between align-items-center">
                                    <div class="col-auto">
                                        <h1 class="title">{{$product['category'][1]['category']['name']}} <!-- <img class="icon" src="{{asset('public/assets/images/new_template/Flashsale@2x.png')}}" alt=""> --></h1>
                                    </div>
                                    <div class="order-last order-md-0 col-12 col-md">
                                        <!-- <div class="end-of">
                                            <span class="text d-none d-md-inline">End of</span>
                                            <span class="text time">00</span>
                                            <span class="text time">00</span>
                                            <span class="text time">00</span>
                                        </div> -->
                                    </div>
                                    <div class="col-auto">
                                        <a class="link-more" href="{{url('categories/'.$product['category'][1]['category']['id'].'/'.$product['category'][1]['category']['url'])}}">{{__('messages.view_more')}}<ion-icon class="icon" name="chevron-forward-outline" role="img" aria-label="chevron forward outline"></ion-icon></a>
                                    </div>
                                </div>
                            </div>
                            <div class="content">
                                <div class="product-flash-sale-swiper">
                                    <div class="swiper-container">
                                        <div class="swiper-wrapper" id="swiper-wrapper-44f61fc70bb92aa2">
                                        @if($product['category'][1]['data'])
                                            @foreach($product['category'][1]['data'] as $data) 
                                            <div class="swiper-slide">
                                                <div class="product-grids-item">
                                                    <div class="thumbnail">
                                                        <div class="frame">
                                                            <img class="image" src="{{$data['images']}}" alt="">
                                                            <!-- <img class="shipping" src="{{asset('public/assets/images/new_template/Batch_24hoursship.png')}}" alt="shipping"> --> 
                                                            <a href="#"><img class="favorite" src="{{asset('public/assets/images/new_template/favorite_active@2x.png')}}" alt="favorite"></a>
                                                        </div>
                                                    </div>
                                                    <div class="detail">
                                                        <div class="tags">
                                                            @if($data['product_type'] == 'B2C' || $data['product_type'] == 'BOTH')<span class="item color-00BFB1">{{__('messages.retail')}}</span>@endif<!--Retail -->
                                                            @if($data['product_type'] == 'B2B' || $data['product_type'] == 'BOTH')<span class="item color-EBB500">{{__('messages.wholesale_')}}</span>@endif
                                                        </div>
                                                        <a href="{{url('product/'.$data['product_id'].'/'.$data['url'])}}" class="stretched-link">
                                                            <p class="name">{{$data['name']}}</p>
                                                        </a>
                                                        @if($data['currency']['display_percent'] > 0)
                                                        <div class="discount">
                                                            <span class="tag">-{{$data['currency']['display_percent']}}%</span>
                                                            <span class="rebate">{{$currency}}{{$data['currency']['base']}} </span>
                                                        </div>
                                                        @else
                                                        <div class="discount"></div>
                                                        @endif
                                                        <p class="price">{{$currency}}{{$data['currency']['price']}} </p>
                                                        <div class="progress"> @php $rand_sold = rand(1, 10); @endphp
                                                            <div class="progress-bar" style="width: {{$rand_sold/11*100}}%;">
                                                                <p class="text">{{__('messages.sold_number')}} {{$rand_sold}}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach
                                        @endif                                            
                                        </div>
                                    <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span></div>
                                    <div class="swiper-button-next" tabindex="0" role="button" aria-label="Next slide" aria-controls="swiper-wrapper-44f61fc70bb92aa2" aria-disabled="false">
                                        <img class="icon" src="{{asset('public/assets/images/new_template/arrow@2x.png')}}" alt="next">
                                    </div>
                                    <div class="swiper-button-prev swiper-button-disabled" tabindex="-1" role="button" aria-label="Previous slide" aria-controls="swiper-wrapper-44f61fc70bb92aa2" aria-disabled="true">
                                        <img class="icon" src="{{asset('public/assets/images/new_template/arrow-prev.png')}}" alt="prev">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
        <div class="section shift-up">
            <div class="container">
                <a href="#"><img class="w-100" src="{{asset('public/assets/images/new_template/JustClick02.jpg')}}" alt="ads"></a>
            </div>
        </div>
        @if(isset($product['newArrival']) && !empty($product['newArrival']))
        <div class="section">
            <div class="container">
                <div class="row justify-content-between align-items-center section-header">
                    <div class="col">
                        <h1 class="title">{{__('messages.newArrival')}}</h1>
                    </div>
                    <div class="col-auto">
                        <a class="link-more" href="{{url('more?section=new-arrival')}}">{{__('messages.view_more')}}<ion-icon class="icon" name="chevron-forward-outline"></ion-icon></a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="hot-deals-swiper">
                            <div class="swiper-container swiper-container-initialized swiper-container-horizontal swiper-container-pointer-events">
                                <div class="swiper-wrapper" id="swiper-wrapper-a263f17a7105a4e4" aria-live="polite" style="transform: translate3d(0px, 0px, 0px);">
                                    @php
                                    $round = 0;
                                    shuffle($product['newArrival']);
                                    @endphp
                                    @foreach($product['newArrival'] as $p)
                                    <div class="swiper-slide swiper-slide-active" role="group" aria-label="1 / 9" style="width: 209.333px; margin-right: 8px;">
                                        <div class="product-grids-item">
                                            <div class="thumbnail">
                                                <div class="frame">
                                                    <img class="image" src="{{$p['images']}}" alt="">
                                                    <!-- <img class="shipping" src="{{asset('public/assets/images/new_template/Batch_24hoursship.png')}}" alt="shipping"> -->
                                                    <a href="{{ url('/') . '/product/' . $p['product_id'] . '/' . $p['url'] }}"><img class="favorite" src="{{asset('public/assets/images/new_template/favorite_active@2x.png')}}" alt="favorite"></a>
                                                </div>
                                            </div>
                                            <div class="detail">
                                                <div class="tags">
                                                    @if($p['product_type'] == 'B2C' || $p['product_type'] == 'BOTH')<span class="item color-00BFB1">{{__('messages.retail')}}</span>@endif<!--Retail -->
                                                    @if($p['product_type'] == 'B2B' || $p['product_type'] == 'BOTH')<span class="item color-EBB500">{{__('messages.wholesale_')}}</span>@endif
                                                </div>
                                                <a href="{{ url('/') . '/product/' . $p['product_id'] . '/' . $p['url'] }}" class="stretched-link">
                                                    <p class="name">{{$p['name']}}</p>
                                                </a>
                                                @if($p['currency']['display_percent'] > 0)
                                                <div class="discount">
                                                    <span class="tag">-{{$p['currency']['display_percent']}}%</span>
                                                    <span class="rebate">{{$currency}}{{$p['currency']['base']}} </span>
                                                </div>
                                                @else
                                                <div class="discount"></div>
                                                @endif
                                                <p class="price">{{$currency}}{{$p['currency']['price']}} </p>
                                            </div>
                                        </div>
                                    </div>
                                    @php
                                    $round++;
                                    if($round == 12) break;
                                    @endphp
                                    @endforeach
                                </div>
                            <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span></div>
                            <div class="swiper-button-next" tabindex="0" role="button" aria-label="Next slide" aria-controls="swiper-wrapper-a263f17a7105a4e4" aria-disabled="false">
                                <img class="icon" src="{{asset('public/assets/images/new_template/arrow@2x.png')}}" alt="next">
                            </div>
                            <div class="swiper-button-prev swiper-button-disabled" tabindex="-1" role="button" aria-label="Previous slide" aria-controls="swiper-wrapper-a263f17a7105a4e4" aria-disabled="true">
                                <img class="icon" src="{{asset('public/assets/images/new_template/arrow-prev.png')}}" alt="prev">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif

        @if($product['category'][0]['data']) @php shuffle($product['category'][0]['data']) @endphp
        <div class="section">
            <div class="container">
                <div class="row justify-content-between align-items-center section-header">
                    <div class="col">
                        <h1 class="title">{{$product['category'][0]['category']['name']}}</h1>
                    </div>
                    <div class="col-auto">
                        <a class="link-more" href="{{url('categories/'.$product['category'][0]['category']['id'].'/'.$product['category'][0]['category']['url'])}}">{{__('messages.view_more')}}<ion-icon class="icon" name="chevron-forward-outline"></ion-icon></a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="hot-deals-swiper">
                            <div class="swiper-container swiper-container-initialized swiper-container-horizontal swiper-container-pointer-events">
                                <div class="swiper-wrapper" id="swiper-wrapper-a263f17a7105a4e4" aria-live="polite" style="transform: translate3d(0px, 0px, 0px);">
                                    @php
                                    $round = 0;
                                    shuffle($product['category'][0]['data']);
                                    @endphp
                                    @foreach($product['category'][0]['data'] as $p)
                                    <div class="swiper-slide swiper-slide-active" role="group" aria-label="1 / 9" style="width: 209.333px; margin-right: 8px;">
                                        <div class="product-grids-item">
                                            <div class="thumbnail">
                                                <div class="frame">
                                                    <img class="image" src="{{$p['images']}}" alt="">
                                                    <!-- <img class="shipping" src="{{asset('public/assets/images/new_template/Batch_24hoursship.png')}}" alt="shipping"> -->
                                                    <a href="{{ url('/') . '/product/' . $p['product_id'] . '/' . $p['url'] }}"><img class="favorite" src="{{asset('public/assets/images/new_template/favorite_active@2x.png')}}" alt="favorite"></a>
                                                </div>
                                            </div>
                                            <div class="detail">
                                                <div class="tags">
                                                    @if($p['product_type'] == 'B2C' || $p['product_type'] == 'BOTH')<span class="item color-00BFB1">{{__('messages.retail')}}</span>@endif<!--Retail -->
                                                    @if($p['product_type'] == 'B2B' || $p['product_type'] == 'BOTH')<span class="item color-EBB500">{{__('messages.wholesale_')}}</span>@endif
                                                </div>
                                                <a href="{{ url('/') . '/product/' . $p['product_id'] . '/' . $p['url'] }}" class="stretched-link">
                                                    <p class="name">{{$p['name']}}</p>
                                                </a>
                                                @if($p['currency']['display_percent'] > 0)
                                                <div class="discount">
                                                    <span class="tag">-{{$p['currency']['display_percent']}}%</span>
                                                    <span class="rebate">{{$currency}}{{$p['currency']['base']}} </span>
                                                </div>
                                                @else
                                                <div class="discount"></div>
                                                @endif
                                                <p class="price">{{$currency}}{{$p['currency']['price']}} </p>
                                            </div>
                                        </div>
                                    </div>
                                    @php
                                    $round++;
                                    if($round == 12) break;
                                    @endphp
                                    @endforeach
                                </div>
                            <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span></div>
                            <div class="swiper-button-next" tabindex="0" role="button" aria-label="Next slide" aria-controls="swiper-wrapper-a263f17a7105a4e4" aria-disabled="false">
                                <img class="icon" src="{{asset('public/assets/images/new_template/arrow@2x.png')}}" alt="next">
                            </div>
                            <div class="swiper-button-prev swiper-button-disabled" tabindex="-1" role="button" aria-label="Previous slide" aria-controls="swiper-wrapper-a263f17a7105a4e4" aria-disabled="true">
                                <img class="icon" src="{{asset('public/assets/images/new_template/arrow-prev.png')}}" alt="prev">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif

        @if(isset($product['category'][10]['data'])) @php shuffle($product['category'][10]['data']) @endphp
        <div class="section">
            <div class="container">
                <div class="row justify-content-between align-items-center section-header">
                    <div class="col">
                        <h1 class="title">{{$product['category'][10]['category']['name']}}</h1>
                    </div>
                    <div class="col-auto">
                        <a class="link-more" href="{{url('categories/'.$product['category'][10]['category']['id'].'/'.$product['category'][10]['category']['url'])}}">{{__('messages.view_more')}}<ion-icon class="icon" name="chevron-forward-outline"></ion-icon></a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="hot-deals-swiper">
                            <div class="swiper-container swiper-container-initialized swiper-container-horizontal swiper-container-pointer-events">
                                <div class="swiper-wrapper" id="swiper-wrapper-a263f17a7105a4e4" aria-live="polite" style="transform: translate3d(0px, 0px, 0px);">
                                    @php
                                    $round = 0;
                                    shuffle($product['category'][10]['data']);
                                    @endphp
                                    @foreach($product['category'][10]['data'] as $p)
                                    <div class="swiper-slide swiper-slide-active" role="group" aria-label="1 / 9" style="width: 209.333px; margin-right: 8px;">
                                        <div class="product-grids-item">
                                            <div class="thumbnail">
                                                <div class="frame">
                                                    <img class="image" src="{{$p['images']}}" alt="">
                                                    <!-- <img class="shipping" src="{{asset('public/assets/images/new_template/Batch_24hoursship.png')}}" alt="shipping"> -->
                                                    <a href="{{ url('/') . '/product/' . $p['product_id'] . '/' . $p['url'] }}"><img class="favorite" src="{{asset('public/assets/images/new_template/favorite_active@2x.png')}}" alt="favorite"></a>
                                                </div>
                                            </div>
                                            <div class="detail">
                                                <div class="tags">
                                                    @if($p['product_type'] == 'B2C' || $p['product_type'] == 'BOTH')<span class="item color-00BFB1">{{__('messages.retail')}}</span>@endif<!--Retail -->
                                                    @if($p['product_type'] == 'B2B' || $p['product_type'] == 'BOTH')<span class="item color-EBB500">{{__('messages.wholesale_')}}</span>@endif
                                                </div>
                                                <a href="{{ url('/') . '/product/' . $p['product_id'] . '/' . $p['url'] }}" class="stretched-link">
                                                    <p class="name">{{$p['name']}}</p>
                                                </a>
                                                @if($p['currency']['display_percent'] > 0)
                                                <div class="discount">
                                                    <span class="tag">-{{$p['currency']['display_percent']}}%</span>
                                                    <span class="rebate">{{$currency}}{{$p['currency']['base']}} </span>
                                                </div>
                                                @else
                                                <div class="discount"></div>
                                                @endif
                                                <p class="price">{{$currency}}{{$p['currency']['price']}} </p>
                                            </div>
                                        </div>
                                    </div>
                                    @php
                                    $round++;
                                    if($round == 12) break;
                                    @endphp
                                    @endforeach
                                </div>
                            <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span></div>
                            <div class="swiper-button-next" tabindex="0" role="button" aria-label="Next slide" aria-controls="swiper-wrapper-a263f17a7105a4e4" aria-disabled="false">
                                <img class="icon" src="{{asset('public/assets/images/new_template/arrow@2x.png')}}" alt="next">
                            </div>
                            <div class="swiper-button-prev swiper-button-disabled" tabindex="-1" role="button" aria-label="Previous slide" aria-controls="swiper-wrapper-a263f17a7105a4e4" aria-disabled="true">
                                <img class="icon" src="{{asset('public/assets/images/new_template/arrow-prev.png')}}" alt="prev">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
        
        <!-- <div class="section">
            <div class="container">
                <div class="row justify-content-between align-items-center section-header">
                    <div class="col">
                        <h1 class="title">Coming Soon</h1>
                    </div>                    
                </div>
                <div class="row">
                    <div class="col-12 mb-2">
                        <div class="top-brand-discounts-highlight-swiper">
                            <div class="swiper-container">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide" >
                                        <div class="brand-highlight-item" style="background-image: url({{asset('public/assets/images/new_template/Picture1.png')}});">
                                            <div class="h-100 row justify-content-end align-items-center">
                                                <div class="col-6 col-sm-auto mb-4">
                                                    <a href="#">
                                                        <div class="item">
                                                            <div class="thumbnail">
                                                                <div class="frame">
                                                                    <img class="image" src="{{asset('public/assets/images/new_template/iphone_red.png')}}" alt="">
                                                                </div>
                                                            </div>
                                                            <p class="price">THB29,900 <span class="del">THB31,900 </span></p>
                                                        </div>
                                                    </a>
                                                </div>
                                                <div class="col-6 col-sm-auto mb-4">
                                                    <a href="#">
                                                        <div class="item">
                                                            <div class="thumbnail">
                                                                <div class="frame">
                                                                    <img class="image" src="{{asset('public/assets/images/new_template/iphone_pro.png')}}" alt="">
                                                                </div>
                                                            </div>
                                                            <p class="price">THB35,000  <span class="del">THB36,900 </span></p>
                                                        </div>
                                                    </a>
                                                </div>
                                                <div class="col-6 col-sm-auto mb-4">
                                                    <a href="#">
                                                        <div class="item">
                                                            <div class="thumbnail">
                                                                <div class="frame">
                                                                    <img class="image" src="{{asset('public/assets/images/new_template/iphone_pro_max.png')}}" alt="">
                                                                </div>
                                                            </div>
                                                            <p class="price">THB38,000  <span class="del">THB39,900 </span></p>
                                                        </div>
                                                    </a>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>   
                                
                            <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span></div>
                        </div>
                    </div>                    
                </div>
            </div>
        </div> -->
        <div class="section">
            <div class="container">
                <div class="deals-of-the-week dark">
                    <div class="row">
                        <div class="col-12 col-md-4 col-lg-3">
                            <div class="h-100 row align-items-center">
                                <div class="h-100 col">
                                    <div class="h-100 row timer justify-content-between align-items-center">
                                        <div class="col-12 align-self-center">
                                            <p class="title brand-you-love">BRAND <br class="d-none d-md-inline" />YOU <br class="d-none d-md-inline" />LOVE</p>
                                        </div>
                                        <!-- <div class="d-none d-md-block col-md-12 align-self-center">
                                            <p class="big-text">%</p>
                                        </div>
                                        <div class="w-100"></div>
                                        <div class="col-12 align-self-end">
                                            <div class="d-none d-md-block time">
                                                <div class="frame">
                                                    <span class="unit">01 &nbsp;:&nbsp; 00 &nbsp;:&nbsp; 00</span>
                                                </div>
                                            </div>
                                            <div class="d-md-none time square">
                                                <span class="text">01</span>
                                                <span class="text">00</span>
                                                <span class="text">00</span>
                                            </div>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-8 col-lg-9">
                            <div class="deals-of-the-week-swiper">
                                <div class="swiper-container swiper-container-initialized swiper-container-horizontal swiper-container-pointer-events">
                                    <div class="swiper-wrapper" id="swiper-wrapper-9b21113907ca2839" aria-live="polite" style="transform: translate3d(0px, 0px, 0px);">
                                    @if($product['category'][6]['data']) @php shuffle($product['category'][6]['data']) @endphp
                                        @foreach($product['category'][6]['data'] as $data) 
                                        <div class="swiper-slide swiper-slide-active" role="group" aria-label="1 / 9" style="width: 229.5px; margin-right: 8px;">
                                            <div class="product-grids-item">
                                                <div class="thumbnail">
                                                    <div class="frame">
                                                        <img class="image" src="{{$data['images']}}" alt="">
                                                        <!-- <img class="shipping" src="{{asset('public/assets/images/new_template/Batch_24hoursship.png')}}" alt="shipping"> -->
                                                        <a href="#"><img class="favorite" src="{{asset('public/assets/images/new_template/favorite_active@2x.png')}}" alt="favorite"></a>
                                                    </div>
                                                </div>
                                                <div class="detail">
                                                    <div class="tags">
                                                        @if($data['product_type'] == 'B2C' || $data['product_type'] == 'BOTH')<span class="item color-00BFB1">{{__('messages.retail')}}</span>@endif<!--Retail -->
                                                        @if($data['product_type'] == 'B2B' || $data['product_type'] == 'BOTH')<span class="item color-EBB500">{{__('messages.wholesale_')}}</span>@endif
                                                    </div>
                                                    <a href="{{url('product/'.$data['product_id'].'/'.$data['url'])}}" class="stretched-link">
                                                        <p class="name">{{$data['name']}}</p>
                                                    </a>
                                                    @if($data['currency']['display_percent'] > 0)
                                                    <div class="discount">
                                                        <span class="tag">-{{$data['currency']['display_percent']}}%</span>
                                                        <span class="rebate">{{$currency}}{{$data['currency']['base']}} </span>
                                                    </div>
                                                    @else
                                                    <div class="discount"></div>
                                                    @endif
                                                    <p class="price">{{$currency}}{{$data['currency']['price']}} </p>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    @endif
                                    </div>
                                <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span></div>
                                <div class="swiper-button-next" tabindex="0" role="button" aria-label="Next slide" aria-controls="swiper-wrapper-9b21113907ca2839" aria-disabled="false">
                                    <img class="icon" src="{{asset('public/assets/images/new_template/arrow@2x.png')}}" alt="next">
                                </div>
                                <div class="swiper-button-prev swiper-button-disabled" tabindex="-1" role="button" aria-label="Previous slide" aria-controls="swiper-wrapper-9b21113907ca2839" aria-disabled="true">
                                    <img class="icon" src="{{asset('public/assets/images/new_template/arrow-prev.png')}}" alt="prev">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section shift-up level-3">
            <div class="container">
                <div class="row gx-3 gy-3">
                    <div class="col-12 col-sm-6">
                        <a href="#"><img class="w-100" src="{{asset('public/assets/images/new_template/smart-gadget.png')}}" alt="ads"></a>
                    </div>
                    <div class="col-12 col-sm-6">
                        <a href="#"><img class="w-100" src="{{asset('public/assets/images/new_template/unlockYourStyle.png')}}" alt="ads"></a>
                    </div>
                </div>
            </div>
        </div>
        <!-- HOT DEALS -->
        @if($product['category'][2]['data']) @php shuffle($product['category'][2]['data']) @endphp
        <div class="section">
            <div class="container">
                <div class="row justify-content-between align-items-center section-header">
                    <div class="col">
                        <h1 class="title">{{$product['category'][2]['category']['name']}}</h1>
                    </div>
                    <div class="col-auto">
                        <a class="link-more" href="{{url('categories/'.$product['category'][2]['category']['id'].'/'.$product['category'][2]['category']['url'])}}">{{__('messages.view_more')}}<ion-icon class="icon" name="chevron-forward-outline"></ion-icon></a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="hot-deals-swiper">
                            <div class="swiper-container swiper-container-initialized swiper-container-horizontal swiper-container-pointer-events">
                                <div class="swiper-wrapper" id="swiper-wrapper-a263f17a7105a4e4" aria-live="polite" style="transform: translate3d(0px, 0px, 0px);">
                                    @php
                                    $round = 0;
                                    @endphp
                                    @foreach($product['category'][2]['data'] as $p)
                                    <div class="swiper-slide swiper-slide-active" role="group" aria-label="1 / 9" style="width: 209.333px; margin-right: 8px;">
                                        <div class="product-grids-item">
                                            <div class="thumbnail">
                                                <div class="frame">
                                                    <img class="image" src="{{$p['images']}}" alt="">
                                                    <!-- <img class="shipping" src="{{asset('public/assets/images/new_template/Batch_24hoursship.png')}}" alt="shipping"> -->
                                                    <a href="{{ url('/') . '/product/' . $p['product_id'] . '/' . $p['url'] }}"><img class="favorite" src="{{asset('public/assets/images/new_template/favorite_active@2x.png')}}" alt="favorite"></a>
                                                </div>
                                            </div>
                                            <div class="detail">
                                                <div class="tags">
                                                    @if($p['product_type'] == 'B2C' || $p['product_type'] == 'BOTH')<span class="item color-00BFB1">{{__('messages.retail')}}</span>@endif<!--Retail -->
                                                    @if($p['product_type'] == 'B2B' || $p['product_type'] == 'BOTH')<span class="item color-EBB500">{{__('messages.wholesale_')}}</span>@endif
                                                </div>
                                                <a href="{{ url('/') . '/product/' . $p['product_id'] . '/' . $p['url'] }}" class="stretched-link">
                                                    <p class="name">{{$p['name']}}</p>
                                                </a>
                                                @if($p['currency']['display_percent'] > 0)
                                                <div class="discount">
                                                    <span class="tag">-{{$p['currency']['display_percent']}}%</span>
                                                    <span class="rebate">{{$currency}}{{$p['currency']['base']}} </span>
                                                </div>
                                                @else
                                                <div class="discount"></div>
                                                @endif
                                                <p class="price">{{$currency}}{{$p['currency']['price']}} </p>
                                            </div>
                                        </div>
                                    </div>
                                    @php
                                    $round++;
                                    if($round == 12) break;
                                    @endphp
                                    @endforeach
                                </div>
                            <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span></div>
                            <div class="swiper-button-next" tabindex="0" role="button" aria-label="Next slide" aria-controls="swiper-wrapper-a263f17a7105a4e4" aria-disabled="false">
                                <img class="icon" src="{{asset('public/assets/images/new_template/arrow@2x.png')}}" alt="next">
                            </div>
                            <div class="swiper-button-prev swiper-button-disabled" tabindex="-1" role="button" aria-label="Previous slide" aria-controls="swiper-wrapper-a263f17a7105a4e4" aria-disabled="true">
                                <img class="icon" src="{{asset('public/assets/images/new_template/arrow-prev.png')}}" alt="prev">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif

        @if($product['category'][9]['data']) @php shuffle($product['category'][9]['data']) @endphp
        <div class="section">
            <div class="container">
                <div class="row justify-content-between align-items-center section-header">
                    <div class="col">
                        <h1 class="title">{{$product['category'][9]['category']['name']}}</h1>
                    </div>
                    <div class="col-auto">
                        <a class="link-more" href="{{url('categories/'.$product['category'][9]['category']['id'].'/'.$product['category'][9]['category']['url'])}}">{{__('messages.view_more')}}<ion-icon class="icon" name="chevron-forward-outline"></ion-icon></a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="hot-deals-swiper">
                            <div class="swiper-container swiper-container-initialized swiper-container-horizontal swiper-container-pointer-events">
                                <div class="swiper-wrapper" id="swiper-wrapper-a263f17a7105a4e4" aria-live="polite" style="transform: translate3d(0px, 0px, 0px);">
                                    @php
                                    $round = 0;
                                    @endphp
                                    @foreach($product['category'][9]['data'] as $p)
                                    <div class="swiper-slide swiper-slide-active" role="group" aria-label="1 / 9" style="width: 209.333px; margin-right: 8px;">
                                        <div class="product-grids-item">
                                            <div class="thumbnail">
                                                <div class="frame">
                                                    <img class="image" src="{{$p['images']}}" alt="">
                                                    <!-- <img class="shipping" src="{{asset('public/assets/images/new_template/Batch_24hoursship.png')}}" alt="shipping"> -->
                                                    <a href="{{ url('/') . '/product/' . $p['product_id'] . '/' . $p['url'] }}"><img class="favorite" src="{{asset('public/assets/images/new_template/favorite_active@2x.png')}}" alt="favorite"></a>
                                                </div>
                                            </div>
                                            <div class="detail">
                                                <div class="tags">
                                                    @if($p['product_type'] == 'B2C' || $p['product_type'] == 'BOTH')<span class="item color-00BFB1">{{__('messages.retail')}}</span>@endif<!--Retail -->
                                                    @if($p['product_type'] == 'B2B' || $p['product_type'] == 'BOTH')<span class="item color-EBB500">{{__('messages.wholesale_')}}</span>@endif
                                                </div>
                                                <a href="{{ url('/') . '/product/' . $p['product_id'] . '/' . $p['url'] }}" class="stretched-link">
                                                    <p class="name">{{$p['name']}}</p>
                                                </a>
                                                @if($p['currency']['display_percent'] > 0)
                                                <div class="discount">
                                                    <span class="tag">-{{$p['currency']['display_percent']}}%</span>
                                                    <span class="rebate">{{$currency}}{{$p['currency']['base']}} </span>
                                                </div>
                                                @else
                                                <div class="discount"></div>
                                                @endif
                                                <p class="price">{{$currency}}{{$p['currency']['price']}} </p>
                                            </div>
                                        </div>
                                    </div>
                                    @php
                                    $round++;
                                    if($round == 12) break;
                                    @endphp
                                    @endforeach
                                </div>
                            <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span></div>
                            <div class="swiper-button-next" tabindex="0" role="button" aria-label="Next slide" aria-controls="swiper-wrapper-a263f17a7105a4e4" aria-disabled="false">
                                <img class="icon" src="{{asset('public/assets/images/new_template/arrow@2x.png')}}" alt="next">
                            </div>
                            <div class="swiper-button-prev swiper-button-disabled" tabindex="-1" role="button" aria-label="Previous slide" aria-controls="swiper-wrapper-a263f17a7105a4e4" aria-disabled="true">
                                <img class="icon" src="{{asset('public/assets/images/new_template/arrow-prev.png')}}" alt="prev">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif

        <!--Health and beauty -->
        @if($product['category'][3]['data']) @php shuffle($product['category'][3]['data']) @endphp
        <div class="section">
            <div class="container">
                <div class="row justify-content-between align-items-center section-header">
                    <div class="col">
                        <h1 class="title">{{$product['category'][3]['category']['name']}}</h1>
                    </div>
                    <div class="col-auto">
                        <a class="link-more" href="{{url('categories/'.$product['category'][3]['category']['id'].'/'.$product['category'][3]['category']['url'])}}">{{__('messages.view_more')}}<ion-icon class="icon" name="chevron-forward-outline"></ion-icon></a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="hot-deals-swiper">
                            <div class="swiper-container swiper-container-initialized swiper-container-horizontal swiper-container-pointer-events">
                                <div class="swiper-wrapper" id="swiper-wrapper-a263f17a7105a4e4" aria-live="polite" style="transform: translate3d(0px, 0px, 0px);">
                                    @php
                                    $round = 0;
                                    @endphp
                                    @foreach($product['category'][3]['data'] as $p)
                                    <div class="swiper-slide swiper-slide-active" role="group" aria-label="1 / 9" style="width: 209.333px; margin-right: 8px;">
                                        <div class="product-grids-item">
                                            <div class="thumbnail">
                                                <div class="frame">
                                                    <img class="image" src="{{$p['images']}}" alt="">
                                                    <!-- <img class="shipping" src="{{asset('public/assets/images/new_template/Batch_24hoursship.png')}}" alt="shipping"> -->
                                                    <a href="{{ url('/') . '/product/' . $p['product_id'] . '/' . $p['url'] }}"><img class="favorite" src="{{asset('public/assets/images/new_template/favorite_active@2x.png')}}" alt="favorite"></a>
                                                </div>
                                            </div>
                                            <div class="detail">
                                                <div class="tags">
                                                    @if($p['product_type'] == 'B2C' || $p['product_type'] == 'BOTH')<span class="item color-00BFB1">{{__('messages.retail')}}</span>@endif<!--Retail -->
                                                    @if($p['product_type'] == 'B2B' || $p['product_type'] == 'BOTH')<span class="item color-EBB500">{{__('messages.wholesale_')}}</span>@endif
                                                </div>
                                                <a href="{{ url('/') . '/product/' . $p['product_id'] . '/' . $p['url'] }}" class="stretched-link">
                                                    <p class="name">{{$p['name']}}</p>
                                                </a>
                                                @if($p['currency']['display_percent'] > 0)
                                                <div class="discount">
                                                    <span class="tag">-{{$p['currency']['display_percent']}}%</span>
                                                    <span class="rebate">{{$currency}}{{$p['currency']['base']}} </span>
                                                </div>
                                                @else
                                                <div class="discount"></div>
                                                @endif
                                                <p class="price">{{$currency}}{{$p['currency']['price']}} </p>
                                            </div>
                                        </div>
                                    </div>
                                    @php
                                    $round++;
                                    if($round == 12) break;
                                    @endphp
                                    @endforeach
                                </div>
                            <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span></div>
                            <div class="swiper-button-next" tabindex="0" role="button" aria-label="Next slide" aria-controls="swiper-wrapper-a263f17a7105a4e4" aria-disabled="false">
                                <img class="icon" src="{{asset('public/assets/images/new_template/arrow@2x.png')}}" alt="next">
                            </div>
                            <div class="swiper-button-prev swiper-button-disabled" tabindex="-1" role="button" aria-label="Previous slide" aria-controls="swiper-wrapper-a263f17a7105a4e4" aria-disabled="true">
                                <img class="icon" src="{{asset('public/assets/images/new_template/arrow-prev.png')}}" alt="prev">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif

        <div class="section">
            <div class="container">
                <div class="row justify-content-between align-items-center section-header">
                    <div class="col">
                        <h1 class="title">{{__('messages.top_category')}}</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="top-categories-swiper">
                            <div class="swiper-container">
                                <div class="swiper-wrapper">
                                @if(isset($category_all) && !empty($category_all))
                                @php 
                                $round = 0;
                                $idx = 1;
                                @endphp
                                    @foreach($category_all as $key => $cat) 
                                    @if(in_array($cat['id'], $alive_cat))
                                    @if(($round == 2 || $round == 0) )
                                    
                                    <div class="swiper-slide swiper-slide-active" role="group" aria-label="{{$idx}} / 5" style="width: 426.667px; margin-right: 8px;">@endif
                                        <div class="category-highlight-item">
                                            <div class="row align-items-center">
                                                <div class="col-auto">
                                                    <img class="figure" src="{{$cat['image']}}" alt="">
                                                </div>
                                                <div class="col">
                                                    <div class="content">
                                                        <div class="title">{{$cat['name']}}</div>
                                                        <div class="links">
                                                            @if(isset($cat['parent']) && !empty($cat['parent'])) @php $sub_round = 0; shuffle($cat['parent']); @endphp
                                                                @foreach($cat['parent'] as $sub_cat_key => $sub_cat)
                                                                @if(in_array($sub_cat['id'], $alive_cat))
                                                                    <div class="item"><a href="{{url('categories/'.$sub_cat['id'].'/'.$sub_cat['url'])}}" class="link">{{$sub_cat['name']}}</a></div>
                                                                    @php $sub_round++; @endphp
                                                                    @php if($sub_round == 4) break; @endphp
                                                                @endif
                                                                @endforeach
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="text-end">
                                                        <a href="{{url('categories/'.$cat['id'].'/'.$cat['url'])}}" class="link-more">{{__('messages.view_more')}}<ion-icon class="icon" name="chevron-forward-outline"></ion-icon></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>                                   
                                        @php $round++; $idx++; @endphp
                                    @if($round == 2  || $round == 0)</div>@endif
                                    @php if($round > 2) $round = 1; @endphp
                                    @endif
                                    @endforeach
                                @endif                                  
                                    </div>
                                    
                                </div>
                            </div>
                            <!-- <div class="swiper-button-next">
                                <img class="icon" src="{{asset('public/assets/images/new_template/arrow@2x.png')}}" alt="next" />
                            </div>
                            <div class="swiper-button-prev">
                                <img class="icon" src="{{asset('public/assets/images/new_template/arrow-prev.png')}}" alt="prev" />
                            </div>                             -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section">
            <div class="container">
                <!-- <div class="row justify-content-between align-items-center section-header">
                    <div class="col">
                        <h1 class="title">Popular Deals</h1>
                    </div>
                    <div class="col-auto">
                        <a class="link-more" href="#">View more<ion-icon class="icon" name="chevron-forward-outline"></ion-icon></a>
                    </div>
                </div> -->
                <div class="row g-2">
                    <div class="col-12 col-sm-6 col-xl-4">
                        <a href="#"><img class="w-100" src="{{asset('public/assets/images/new_template/watchCollection.jpg')}}" alt=""></a>
                    </div>
                    <div class="col-12 col-sm-6 col-xl-4">
                        <a href="#"><img class="w-100" src="{{asset('public/assets/images/new_template/superPetproduct.jpg')}}" alt=""></a>
                    </div>
                    <div class="col-12 col-sm-6 col-xl-4">
                        <a href="#"><img class="w-100" src="{{asset('public/assets/images/new_template/registerNow.jpg')}}" alt=""></a>
                    </div>                    
                </div>
            </div>
        </div>
        <div class="section shift-up">
            <div class="container">
                <div class="single-banner-swiper">
                    <div class="swiper-container">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <a href="#">
                                    <img class="w-100" src="{{asset('public/assets/images/new_template/MicrosoftTeams-image.png')}}" alt="" />
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="#">
                                    <img class="w-100" src="{{asset('public/assets/images/new_template/3ver1695x532Happydogandcat3.jpg')}}" alt="" />
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="#">
                                    <img class="w-100" src="{{asset('public/assets/images/new_template/Artboard 12.jpg')}}" alt="" />
                                </a>
                            </div>
                        </div>
                        <div class="swiper-pagination"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section">
            <div class="container">
                <div class="row justify-content-between align-items-center section-header">
                    <div class="col">
                        <h1 class="title">Top Brands/Partners</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="top-brands">
                        @php 
                            $brand_link = array(
                                21 => "coach",
                                22 => "kipling",
                                23 => "casio",
                                24 => "citizen",
                                25 => "",
                                26 => "mille",
                                27 => "betrago",
                                28 => "xiaomi",
                                29 => "error",
                                30 => "Samsung",
                                31 => "LG",
                                32 => "playink"
                                
                            ); 
                            //shuffle($brand_link);
                        @endphp
                            <div class="row gx-2 gy-2 align-items-center grids" id="top-brands">
                                @foreach($brand_link as $key => $val)
                                <div class="col-4 col-md-4 col-lg-2 brand">
                                    <a href="{{url('search?cat_id=0&s='.$val)}}"><img class="logo" src="{{asset('public/assets/images/new_template/'.$key.'.png')}}" alt=""></a>
                                </div>
                                @endforeach
                                <!-- <div class="w-100"></div>
                                <div class="col-12">
                                    <div class="row justify-content-center">
                                        <div class="col-12 col-md-6 col-lg-3">
                                            <button class="link-more">View more</button>
                                        </div>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @if(isset($product['category'][4]['data'])) @php shuffle($product['category'][4]['data']) @endphp
        <div class="section">
            <div class="container">
                <div class="row justify-content-between align-items-center section-header">
                    <div class="col">
                        <h1 class="title">{{$product['category'][4]['category']['name']}}</h1>
                    </div>
                    <div class="col-auto">
                        <a class="link-more" href="{{url('categories/'.$product['category'][4]['category']['id'].'/'.$product['category'][4]['category']['url'])}}">{{__('messages.view_more')}}<ion-icon class="icon" name="chevron-forward-outline"></ion-icon></a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="hot-deals-swiper">
                            <div class="swiper-container swiper-container-initialized swiper-container-horizontal swiper-container-pointer-events">
                                <div class="swiper-wrapper" id="swiper-wrapper-a263f17a7105a4e4" aria-live="polite" style="transform: translate3d(0px, 0px, 0px);">
                                    @php
                                    $round = 0;
                                    @endphp
                                    @foreach($product['category'][4]['data'] as $p)
                                    <div class="swiper-slide swiper-slide-active" role="group" aria-label="1 / 9" style="width: 209.333px; margin-right: 8px;">
                                        <div class="product-grids-item">
                                            <div class="thumbnail">
                                                <div class="frame">
                                                    <img class="image" src="{{$p['images']}}" alt="">
                                                    <!-- <img class="shipping" src="{{asset('public/assets/images/new_template/Batch_24hoursship.png')}}" alt="shipping"> -->
                                                    <a href="{{ url('/') . '/product/' . $p['product_id'] . '/' . $p['url'] }}"><img class="favorite" src="{{asset('public/assets/images/new_template/favorite_active@2x.png')}}" alt="favorite"></a>
                                                </div>
                                            </div>
                                            <div class="detail">
                                                <div class="tags">
                                                    @if($p['product_type'] == 'B2C' || $p['product_type'] == 'BOTH')<span class="item color-00BFB1">{{__('messages.retail')}}</span>@endif<!--Retail -->
                                                    @if($p['product_type'] == 'B2B' || $p['product_type'] == 'BOTH')<span class="item color-EBB500">{{__('messages.wholesale_')}}</span>@endif
                                                </div>
                                                <a href="{{ url('/') . '/product/' . $p['product_id'] . '/' . $p['url'] }}" class="stretched-link">
                                                    <p class="name">{{$p['name']}}</p>
                                                </a>
                                                @if($p['currency']['display_percent'] > 0)
                                                <div class="discount">
                                                    <span class="tag">-{{$p['currency']['display_percent']}}%</span>
                                                    <span class="rebate">{{$currency}}{{$p['currency']['base']}} </span>
                                                </div>
                                                @else
                                                <div class="discount"></div>
                                                @endif
                                                <p class="price">{{$currency}}{{$p['currency']['price']}} </p>
                                            </div>
                                        </div>
                                    </div>
                                    @php
                                    $round++;
                                    if($round == 12) break;
                                    @endphp
                                    @endforeach
                                </div>
                            <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span></div>
                            <div class="swiper-button-next" tabindex="0" role="button" aria-label="Next slide" aria-controls="swiper-wrapper-a263f17a7105a4e4" aria-disabled="false">
                                <img class="icon" src="{{asset('public/assets/images/new_template/arrow@2x.png')}}" alt="next">
                            </div>
                            <div class="swiper-button-prev swiper-button-disabled" tabindex="-1" role="button" aria-label="Previous slide" aria-controls="swiper-wrapper-a263f17a7105a4e4" aria-disabled="true">
                                <img class="icon" src="{{asset('public/assets/images/new_template/arrow-prev.png')}}" alt="prev">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif

        @if(isset($product['category'][5]['data'])) @php shuffle($product['category'][5]['data']) @endphp
        <div class="section">
            <div class="container">
                <div class="row justify-content-between align-items-center section-header">
                    <div class="col">
                        <h1 class="title">{{$product['category'][5]['category']['name']}}</h1>
                    </div>
                    <div class="col-auto">
                        <a class="link-more" href="{{url('categories/'.$product['category'][5]['category']['id'].'/'.$product['category'][5]['category']['url'])}}">{{__('messages.view_more')}}<ion-icon class="icon" name="chevron-forward-outline"></ion-icon></a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="hot-deals-swiper">
                            <div class="swiper-container swiper-container-initialized swiper-container-horizontal swiper-container-pointer-events">
                                <div class="swiper-wrapper" id="swiper-wrapper-a263f17a7105a4e4" aria-live="polite" style="transform: translate3d(0px, 0px, 0px);">
                                    @php
                                    $round = 0;
                                    @endphp
                                    @foreach($product['category'][5]['data'] as $p)
                                    <div class="swiper-slide swiper-slide-active" role="group" aria-label="1 / 9" style="width: 209.333px; margin-right: 8px;">
                                        <div class="product-grids-item">
                                            <div class="thumbnail">
                                                <div class="frame">
                                                    <img class="image" src="{{$p['images']}}" alt="">
                                                    <!-- <img class="shipping" src="{{asset('public/assets/images/new_template/Batch_24hoursship.png')}}" alt="shipping"> -->
                                                    <a href="{{ url('/') . '/product/' . $p['product_id'] . '/' . $p['url'] }}"><img class="favorite" src="{{asset('public/assets/images/new_template/favorite_active@2x.png')}}" alt="favorite"></a>
                                                </div>
                                            </div>
                                            <div class="detail">
                                                <div class="tags">
                                                    @if($p['product_type'] == 'B2C' || $p['product_type'] == 'BOTH')<span class="item color-00BFB1">{{__('messages.retail')}}</span>@endif<!--Retail -->
                                                    @if($p['product_type'] == 'B2B' || $p['product_type'] == 'BOTH')<span class="item color-EBB500">{{__('messages.wholesale_')}}</span>@endif
                                                </div>
                                                <a href="{{ url('/') . '/product/' . $p['product_id'] . '/' . $p['url'] }}" class="stretched-link">
                                                    <p class="name">{{$p['name']}}</p>
                                                </a>
                                                @if($p['currency']['display_percent'] > 0)
                                                <div class="discount">
                                                    <span class="tag">-{{$p['currency']['display_percent']}}%</span>
                                                    <span class="rebate">{{$currency}}{{$p['currency']['base']}} </span>
                                                </div>
                                                @else
                                                <div class="discount"></div>
                                                @endif
                                                <p class="price">{{$currency}}{{$p['currency']['price']}} </p>
                                            </div>
                                        </div>
                                    </div>
                                    @php
                                    $round++;
                                    if($round == 12) break;
                                    @endphp
                                    @endforeach
                                </div>
                            <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span></div>
                            <div class="swiper-button-next" tabindex="0" role="button" aria-label="Next slide" aria-controls="swiper-wrapper-a263f17a7105a4e4" aria-disabled="false">
                                <img class="icon" src="{{asset('public/assets/images/new_template/arrow@2x.png')}}" alt="next">
                            </div>
                            <div class="swiper-button-prev swiper-button-disabled" tabindex="-1" role="button" aria-label="Previous slide" aria-controls="swiper-wrapper-a263f17a7105a4e4" aria-disabled="true">
                                <img class="icon" src="{{asset('public/assets/images/new_template/arrow-prev.png')}}" alt="prev">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif

        @if(isset($product['category'][8]['data'])) @php shuffle($product['category'][8]['data']) @endphp
        <div class="section">
            <div class="container">
                <div class="row justify-content-between align-items-center section-header">
                    <div class="col">
                        <h1 class="title">{{$product['category'][8]['category']['name']}}</h1>
                    </div>
                    <div class="col-auto">
                        <a class="link-more" href="{{url('categories/'.$product['category'][8]['category']['id'].'/'.$product['category'][8]['category']['url'])}}">{{__('messages.view_more')}}<ion-icon class="icon" name="chevron-forward-outline"></ion-icon></a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="hot-deals-swiper">
                            <div class="swiper-container swiper-container-initialized swiper-container-horizontal swiper-container-pointer-events">
                                <div class="swiper-wrapper" id="swiper-wrapper-a263f17a7105a4e4" aria-live="polite" style="transform: translate3d(0px, 0px, 0px);">
                                    @php
                                    $round = 0;
                                    @endphp
                                    @foreach($product['category'][8]['data'] as $p)
                                    <div class="swiper-slide swiper-slide-active" role="group" aria-label="1 / 9" style="width: 209.333px; margin-right: 8px;">
                                        <div class="product-grids-item">
                                            <div class="thumbnail">
                                                <div class="frame">
                                                    <img class="image" src="{{$p['images']}}" alt="">
                                                    <!-- <img class="shipping" src="{{asset('public/assets/images/new_template/Batch_24hoursship.png')}}" alt="shipping"> -->
                                                    <a href="{{ url('/') . '/product/' . $p['product_id'] . '/' . $p['url'] }}"><img class="favorite" src="{{asset('public/assets/images/new_template/favorite_active@2x.png')}}" alt="favorite"></a>
                                                </div>
                                            </div>
                                            <div class="detail">
                                                <div class="tags">
                                                    @if($p['product_type'] == 'B2C' || $p['product_type'] == 'BOTH')<span class="item color-00BFB1">{{__('messages.retail')}}</span>@endif<!--Retail -->
                                                    @if($p['product_type'] == 'B2B' || $p['product_type'] == 'BOTH')<span class="item color-EBB500">{{__('messages.wholesale_')}}</span>@endif
                                                </div>
                                                <a href="{{ url('/') . '/product/' . $p['product_id'] . '/' . $p['url'] }}" class="stretched-link">
                                                    <p class="name">{{$p['name']}}</p>
                                                </a>
                                                @if($p['currency']['display_percent'] > 0)
                                                <div class="discount">
                                                    <span class="tag">-{{$p['currency']['display_percent']}}%</span>
                                                    <span class="rebate">{{$currency}}{{$p['currency']['base']}} </span>
                                                </div>
                                                @else
                                                <div class="discount"></div>
                                                @endif
                                                <p class="price">{{$currency}}{{$p['currency']['price']}} </p>
                                            </div>
                                        </div>
                                    </div>
                                    @php
                                    $round++;
                                    if($round == 12) break;
                                    @endphp
                                    @endforeach
                                </div>
                            <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span></div>
                            <div class="swiper-button-next" tabindex="0" role="button" aria-label="Next slide" aria-controls="swiper-wrapper-a263f17a7105a4e4" aria-disabled="false">
                                <img class="icon" src="{{asset('public/assets/images/new_template/arrow@2x.png')}}" alt="next">
                            </div>
                            <div class="swiper-button-prev swiper-button-disabled" tabindex="-1" role="button" aria-label="Previous slide" aria-controls="swiper-wrapper-a263f17a7105a4e4" aria-disabled="true">
                                <img class="icon" src="{{asset('public/assets/images/new_template/arrow-prev.png')}}" alt="prev">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
        
        

        @if(isset($product['category'][7]['data'])) @php shuffle($product['category'][7]['data']) @endphp
        <!-- <div class="section">
            <div class="container">
                <div class="row justify-content-between align-items-center section-header">
                    <div class="col">
                        <h1 class="title">{{$product['category'][7]['category']['name']}}</h1>
                    </div>
                    <div class="col-auto">
                        <a class="link-more" href="{{url('categories/'.$product['category'][7]['category']['id'].'/'.$product['category'][7]['category']['url'])}}">{{__('messages.view_more')}}<ion-icon class="icon" name="chevron-forward-outline"></ion-icon></a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="hot-deals-swiper">
                            <div class="swiper-container swiper-container-initialized swiper-container-horizontal swiper-container-pointer-events">
                                <div class="swiper-wrapper" id="swiper-wrapper-a263f17a7105a4e4" aria-live="polite" style="transform: translate3d(0px, 0px, 0px);">
                                    @php
                                    $round = 0;
                                    @endphp
                                    @foreach($product['category'][7]['data'] as $p)
                                    <div class="swiper-slide swiper-slide-active" role="group" aria-label="1 / 9" style="width: 209.333px; margin-right: 8px;">
                                        <div class="product-grids-item">
                                            <div class="thumbnail">
                                                <div class="frame">
                                                    <img class="image" src="{{$p['images']}}" alt="">
                                                    <img class="shipping" src="{{asset('public/assets/images/new_template/Batch_24hoursship.png')}}" alt="shipping">
                                                    <a href="{{ url('/') . '/product/' . $p['product_id'] . '/' . $p['url'] }}"><img class="favorite" src="{{asset('public/assets/images/new_template/favorite_active@2x.png')}}" alt="favorite"></a>
                                                </div>
                                            </div>
                                            <div class="detail">
                                                <div class="tags">
                                                    @if($p['product_type'] == 'B2C' || $p['product_type'] == 'BOTH')<span class="item color-00BFB1">{{__('messages.retail')}}</span>@endif
                                                    @if($p['product_type'] == 'B2B' || $p['product_type'] == 'BOTH')<span class="item color-EBB500">{{__('messages.wholesale_')}}</span>@endif
                                                </div>
                                                <a href="{{ url('/') . '/product/' . $p['product_id'] . '/' . $p['url'] }}" class="stretched-link">
                                                    <p class="name">{{$p['name']}}</p>
                                                </a>
                                                @if($p['currency']['display_percent'] > 0)
                                                <div class="discount">
                                                    <span class="tag">-{{$p['currency']['display_percent']}}%</span>
                                                    <span class="rebate">{{$currency}}{{$p['currency']['base']}} </span>
                                                </div>
                                                @else
                                                <div class="discount"></div>
                                                @endif
                                                <p class="price">{{$currency}}{{$p['currency']['price']}} </p>
                                            </div>
                                        </div>
                                    </div>
                                    @php
                                    $round++;
                                    if($round == 12) break;
                                    @endphp
                                    @endforeach
                                </div>
                            <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span></div>
                            <div class="swiper-button-next" tabindex="0" role="button" aria-label="Next slide" aria-controls="swiper-wrapper-a263f17a7105a4e4" aria-disabled="false">
                                <img class="icon" src="{{asset('public/assets/images/new_template/arrow@2x.png')}}" alt="next">
                            </div>
                            <div class="swiper-button-prev swiper-button-disabled" tabindex="-1" role="button" aria-label="Previous slide" aria-controls="swiper-wrapper-a263f17a7105a4e4" aria-disabled="true">
                                <img class="icon" src="{{asset('public/assets/images/new_template/arrow-prev.png')}}" alt="prev">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
        @endif
        
        <div class="howto-big-banner">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <a id='howto_banner' href="https://lin.ee/ON8SJz5">
                            <img class="banner" src="{{asset('public/assets/images/new_template/howto.png')}}" alt="banner">
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="why-should-shop-with-us">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h1 class="title">{{__('messages.why_shop_with_us')}}</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card-items">
                            <div class="row gx-4 gy-4">
                                <div class="col">
                                    <div class="item title-only">
                                        <img class="figure" src="{{asset('public/assets/images/new_template/icon01.png')}}" alt="">
                                        <p class="text" >{{__('messages.world_wide_sutomer_base')}}</p>
                                        <p class="detail">Duty Free shopping provides international travellers an opportunity to buy more but pay less on a wide variety of products.</p>
                                        <a href="#" class="link stretched-link">{{__('messages.view_more')}}</a>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="item title-only">
                                        <img class="figure" src="{{asset('public/assets/images/new_template/icon02.png')}}" alt="">
                                        <p class="text" >{{__('messages.secure_payment')}}</p>
                                        <p class="detail">Duty Free shopping provides international travellers an opportunity to buy more but pay less on a wide variety of products.</p>
                                        <a href="#" class="link stretched-link">{{__('messages.view_more')}}</a>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="item title-only">
                                        <img class="figure" src="{{asset('public/assets/images/new_template/icon03.png')}}" alt="">
                                        <p class="text" >{{__('messages.fast_deli')}}</p>
                                        <p class="detail">Duty Free shopping provides international travellers an opportunity to buy more but pay less on a wide variety of products.</p>
                                        <a href="#" class="link stretched-link">{{__('messages.view_more')}}</a>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="item title-only">
                                        <img class="figure" src="{{asset('public/assets/images/new_template/icon04.png')}}" alt="">
                                        <p class="text" >{{__('messages.trusted_brands')}}</p>
                                        <p class="detail">Duty Free shopping provides international travellers an opportunity to buy more but pay less on a wide variety of products.</p>
                                        <a href="#" class="link stretched-link">{{__('messages.view_more')}}</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="newsletter" style="background-image: url({{asset('public/assets/images/new_template/newsletterBG.png')}});">
            <div class="container">
                <div class="row justify-content-center align-items-center content">
                    <div class="d-none d-sm-block col-auto">
                        <img class="figure" src="{{asset('public/assets/images/new_template/newsletterBungkie.png')}}" alt="figure" />
                    </div>
                    <div class="col">
                        <div class="row justify-content-center align-items-center form-content">
                            <div class="col-12 col-lg-auto">
                                <div class="intro">
                                    <p class="title">{{__('messages.sign_up_to_newsletter')}}</p>
                                    <p class="text">{{__('messages.news_register_privilege')}}</p>
                                </div>
                            </div>
                            <div class="col">
                                <form class="newsletter-form letter-signup" method='post'>
                                    @csrf                                    
                                    <div class="input-group">                                        
                                        <input type="text" autocomplete="on" class="input-text form-control" id="signupEmail" name="signupEmail" placeholder="{{__('messages.email_address')}}" >
                                        <button class="btn" type="submit">{{__('messages.register')}}</button>                                        
                                    </div>
                                </form>
                                <!-- <label id="signupEmail-error" class="has-error" for="signupEmail">Please enter a valid email address.</label> -->
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <footer class="site-footer">
    <div class="site-map">
        <div class="container">
            <div class="row">
                <div class="col-6 col-lg-2">
                    <div class="map-item">
                        <p class="title">{{ __('messages.business') }}</p>
                        <div class="links">
                            <div class="item"><a href="#howto_banner" class="link">{{__('messages.sell_on_bungkie')}}</a></div>
                            <div class="item"><a href="#" class="link">{{ __('messages.why_wholesale') }}</a></div>
                        </div>
                    </div>
                    <div class="map-item mb-inherit mb-sm-0">
                        <p class="title">{{ __('messages.aboutus') }}</p>
                        <div class="links">
                            <!-- <div class="item"><a href="#" class="link">About Bungkie</a></div> -->
                            <!-- <div class="item"><a href="#" class="link">{{ __('messages.career') }}</a></div> -->
                            <div class="item"><a href="#" class="link">{{ __('messages.privacy_policy') }}</a></div>
                            <div class="item"><a target='_blank' href="{{asset('public/assets/doc/Terms_and_Conditions_(Bungkie)_'.\Session::get('lang').'.pdf')}}" class="link">{{ __('messages.term_of_use') }}</a></div>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-lg-2">
                    <div class="map-item mb-inherit mb-lg-0">
                        <p class="title">{{__('messages.help')}}</p>
                        <div class="links">
                            <div class="item"><a href="#" class="link">{{ __('messages.customer_care') }}</a></div>
                            <div class="item"><a href="#" class="link">{{ __('messages.help_center') }}</a></div>
                            <div class="item"><a href="#" class="link">{{ __('messages.how_to_saign_up') }}</a></div>
                            <div class="item"><a href="#" class="link">{{ __('messages.how_to_buy') }}</a></div>
                            <div class="item"><a href="#" class="link">{{ __('messages.how_to_sell') }}</a></div>
                            <div class="item"><a href="#" class="link">{{ __('messages.b2b_exporting_policy') }}</a></div>
                            <div class="item"><a href="#" class="link">{{ __('messages.refund_return') }}</a></div>
                            <div class="item"><a href="#" class="link">{{ __('messages.midnight_service') }}</a></div>
                            <div class="item"><a href="#" class="link">FAQ</a></div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-5">
                    <div class="row">
                        <div class="col">
                            <div class="map-item mb-inherit">
                                <p class="title">{{__('messages.pay_way')}}</p>
                                <div class="links">
                                    <div class="item inline"><a class="partner_icon link-icon"><img class="icon extra-small" src="https://bungkie.com/public/assets/images/brands/1.png" alt="" /></a></div>
                                    <div class="item inline"><a class="partner_icon link-icon"><img class="icon extra-small" src="https://bungkie.com/public/assets/images/brands/2.png" alt="" /></a></div>
                                    <div class="item inline"><a class="partner_icon link-icon"><img class="icon extra-small" src="https://bungkie.com/public/assets/images/brands/3.png" alt="" /></a></div>
                                    <div class="item inline"><a class="partner_icon link-icon"><img class="icon extra-small" src="https://bungkie.com/public/assets/images/brands/6.png" alt="" /></a></div>
                                    <div class="item inline"><a class="partner_icon link-icon"><img class="icon extra-small" src="https://bungkie.com/public/assets/images/brands/4.png" alt="" /></a></div>
                                    <div class="item inline"><a class="partner_icon link-icon"><img class="icon" src="{{asset('public/assets/images/new_template/BAY.png')}}" alt="" /></a></div>
                                    <div class="item inline"><a class="partner_icon link-icon"><img class="icon" src="{{asset('public/assets/images/new_template/BBL.png')}}" alt="" /></a></div>
                                    <div class="item inline"><a class="partner_icon link-icon"><img class="icon" src="{{asset('public/assets/images/new_template/CIMB.png')}}" alt="" /></a></div>
                                    <div class="item inline"><a class="partner_icon link-icon"><img class="icon" src="{{asset('public/assets/images/new_template/KBank.png')}}" alt="" /></a></div>
                                    <div class="item inline"><a class="partner_icon link-icon"><img class="icon" src="{{asset('public/assets/images/new_template/KTB.png')}}" alt="" /></a></div>
                                    <div class="item inline"><a class="partner_icon link-icon"><img class="icon" src="{{asset('public/assets/images/new_template/SCB.png')}}" alt="" /></a></div>
                                    <div class="item inline"><a class="partner_icon link-icon"><img class="icon" src="{{asset('public/assets/images/new_template/TBank.png')}}" alt="" /></a></div>
                                    <div class="item inline"><a class="partner_icon link-icon"><img class="icon" src="{{asset('public/assets/images/new_template/TMB.png')}}" alt="" /></a></div>
                                    <div class="item inline"><a class="partner_icon link-icon"><img class="icon extra-small" src="{{asset('public/assets/images/new_template/45560770_314804025772541_8704287121298423808_n.png')}}" alt="" /></a></div>
                                    <div class="item inline"><a class="partner_icon link-icon"><img class="icon extra-small" src="{{asset('public/assets/images/new_template/45612467_473021229772429_2004824007160889344_n.png')}}" alt="" /></a></div>
                                    <div class="item inline"><a class="partner_icon link-icon"><img class="icon extra-small" src="{{asset('public/assets/images/new_template/45628940_312935032632914_4081315264551976960_n.png')}}" alt="" /></a></div>
                                    <div class="item inline"><a class="partner_icon link-icon"><img class="icon extra-small" src="{{asset('public/assets/images/new_template/45660974_244942486173875_4583618269709074432_n.png')}}" alt="" /></a></div>
                                    <div class="item inline"><a class="partner_icon link-icon"><img class="icon extra-small" src="{{asset('public/assets/images/new_template/sspay.png')}}" alt="" /></a></div>
                                    <div class="item inline"><a class="partner_icon link-icon"><img class="icon extra-small" src="{{asset('public/assets/images/new_template/mPay.png')}}" alt="" /></a></div>
                                    <div class="item inline"><a class="partner_icon link-icon"><img class="icon extra-small" src="{{asset('public/assets/images/new_template/TrueMoney.png')}}" alt="" /></a></div>
                                    <div class="item inline"><a class="partner_icon link-icon"><img class="icon extra-small" src="{{asset('public/assets/images/new_template/grab-pay.png')}}" alt="" /></a></div>
                                    <div class="item inline"><a class="partner_icon link-icon"><img class="icon extra-small" src="{{asset('public/assets/images/new_template/Promptpay.png')}}" alt="" /></a></div>
                                    <div class="item inline"><a class="partner_icon link-icon"><img class="icon extra-small" src="{{asset('public/assets/images/new_template/AP_logo.png')}}" alt="" /></a></div>
                                    <div class="item inline"><a class="partner_icon link-icon"><img class="icon extra-small" src="{{asset('public/assets/images/new_template/Cenpay.png')}}" alt="" /></a></div>
                                    <div class="item inline"><a class="partner_icon link-icon"><img class="icon" src="{{asset('public/assets/images/new_template/Pay@post.png')}}" alt="" /></a></div>
                                    <div class="item inline"><a class="partner_icon link-icon"><img class="icon extra-small" src="{{asset('public/assets/images/new_template/BigC.png')}}" alt="" /></a></div> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="map-item mb-inherit mb-lg-0">
                                <p class="title">{{__('messages.delivery_services')}}</p>
                                <div class="links">
                                <div class="item inline"><a  class="partner_icon link-icon"><img class="icon" src="{{asset('public/assets/images/new_template/flash-express.png')}}" alt="" /></a></div>
                                    <div class="item inline"><a class="partner_icon link-icon"><img class="icon" src="{{asset('public/assets/images/new_template/Lalamove-Logo.png')}}" alt="" /></a></div>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="map-item mb-inherit mb-lg-0">
                                <p class="title">{{__('messages.verified_by')}}</p>
                                <div class="links">
                                <div class="item inline"><a class="partner_icon link-icon"><img class="icon" src="{{asset('public/assets/images/new_template/DBD-verified.png')}}" alt="" /></a></div>
                                    <div class="item inline"><a  class="partner_icon link-icon"><img class="icon" src="{{asset('public/assets/images/new_template/DBD-Registered.png')}}" alt="" /></a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-3">
                    <div class="row">
                        <div class="col-12 col-md-6 col-lg-12">
                            <div class="map-item mb-inherit mb-sm-0">
                                <p class="title">{{ __('messages.contact') }}</p>
                                <div class="links">
                                    <div class="item"><a href="#" class="link">{{ __('messages.phone') }}: {{ __('messages.contact_us_call') }}</a></div>
                                    <div class="item"><a href="mailto:sales@bungkie.com" class="link">{{ __('messages.email_address') }}: sales@bungkie.com</a></div>
                                    <!-- <div class="item"><a href="#" class="link">QR code line@</a></div> -->
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="map-item">
                                <p class="title">{{__('messages.follow_us')}}</p>
                                <div class="links">
                                    <div class="item inline"><a href="https://www.facebook.com/bungkieofficial" class="link-icon"><img class="icon round" src="{{asset('public/assets/images/new_template/iconFB.png')}}" alt="Facebook"></a></div>
                                    <div class="item inline"><a href="https://lin.ee/ON8SJz5" class="link-icon"><img class="icon round" src="{{asset('public/assets/images/new_template/iconL.png')}}" alt="Line"></a></div>
                                    <div class="item inline"><a href="https://www.instagram.com/bungkieofficial/" class="link-icon"><img class="icon round" src="{{asset('public/assets/images/new_template/iconIG.png')}}" alt="Instagram"></a></div>
                                    <div class="item inline"><a href="https://www.youtube.com/channel/UCCv8pvQDWx-rkBUYh9FzJqg?view_as=subscriber" class="link-icon"><img class="icon round" src="{{asset('public/assets/images/new_template/iconY.png')}}" alt="Youtube"></a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright">
        <div class="container">
            <p class="text">Copyright 2020 Bungkie.com All rights reserved.</p>
        </div>
    </div>
</footer>

<script>
    document.querySelectorAll('[data-menu-toggle="menu"]').forEach(item => {
        item.addEventListener('click', event => {
            document.querySelector('.nav-menu-collapse').classList.toggle('open');
        });
    });

    function openPane(){
        //alert('sssssss');
        document.querySelector('#nav-search-link').addEventListener('click', event => {
            console.log('dddd');
            document.querySelector('#nav-search-link >.search-icon').classList.toggle('expand');
            document.querySelector('.nav-search-collapse').classList.toggle('open');
        });
    }

    

</script>
    <script src="{{asset('public/assets/js/new_template/swiper-bundle.min.js')}}"></script>
    <script>
        new Swiper('.top-categories-swiper>.swiper-container', {
            slidesPerView: 1,
            slidesPerGroup: 1,
            spaceBetween: 8,
            autoplay: {
                delay: 2509,
            },
            navigation: {
                nextEl: '.top-categories-swiper>.swiper-button-next',
                prevEl: '.top-categories-swiper>.swiper-button-prev',
            },
            breakpoints: {
                576: {
                    slidesPerView: 1,
                    slidesPerGroup: 1,
                },
                768: {
                    slidesPerView: 2,
                    slidesPerGroup: 1,
                },
                992: {
                    slidesPerView: 2,
                    slidesPerGroup: 1,
                },
                1200: {
                    slidesPerView: 3,
                    slidesPerGroup: 1,
                },
            }
        });
        new Swiper('.hot-deals-swiper>.swiper-container', {
            slidesPerView: 2,
            slidesPerGroup: 1,
            spaceBetween: 8,
            autoplay: {
                delay: 2600,
            },
            navigation: {
                nextEl: '.hot-deals-swiper>.swiper-button-next',
                prevEl: '.hot-deals-swiper>.swiper-button-prev',
            },
            breakpoints: {
                576: {
                    slidesPerView: 2,
                    slidesPerGroup: 1,
                },
                768: {
                    slidesPerView: 3,
                    slidesPerGroup: 1,
                },
                992: {
                    slidesPerView: 4,
                    slidesPerGroup: 2,
                },
                1200: {
                    slidesPerView: 6,
                    slidesPerGroup: 2,
                },
            }
        });
        new Swiper('.deals-of-the-week-swiper>.swiper-container', {
            slidesPerView: 2,
            slidesPerGroup: 1,
            spaceBetween: 8,
            autoplay: {
                delay: 2350,
            },
            navigation: {
                nextEl: '.deals-of-the-week-swiper>.swiper-button-next',
                prevEl: '.deals-of-the-week-swiper>.swiper-button-prev',
            },
            breakpoints: {
                576: {
                    slidesPerView: 2,
                    slidesPerGroup: 1,
                },
                768: {
                    slidesPerView: 2,
                    slidesPerGroup: 1,
                },
                992: {
                    slidesPerView: 3,
                    slidesPerGroup: 1,
                },
                1200: {
                    slidesPerView: 4,
                    slidesPerGroup: 2,
                },
            }
        });

        

        new Swiper('#top-brand>.swiper-container', {
            slidesPerView: 2,
            slidesPerGroup: 1,
            spaceBetween: 8,
            autoplay: {
                delay: 2600,
            },
            navigation: {
                nextEl: '.top-brand-discounts-banners-swiper>.swiper-button-next',
                prevEl: '.top-brand-discounts-banners-swiper>.swiper-button-prev',
            },
            breakpoints: {
                576: {
                    slidesPerView: 2,
                    slidesPerGroup: 1,
                },
                768: {
                    slidesPerView: 3,
                    slidesPerGroup: 1,
                },
                992: {
                    slidesPerView: 4,
                    slidesPerGroup: 2,
                },
                1200: {
                    slidesPerView: 5,
                    slidesPerGroup: 2,
                },
            }
        });


        new Swiper('.top-brand-discounts-banners-swiper>.swiper-container', {
            slidesPerView: 2,
            slidesPerGroup: 1,
            spaceBetween: 8,
            autoplay: {
                delay: 2550,
            },
            navigation: {
                nextEl: '.top-brand-discounts-banners-swiper>.swiper-button-next',
                prevEl: '.top-brand-discounts-banners-swiper>.swiper-button-prev',
            },
            breakpoints: {
                576: {
                    slidesPerView: 2,
                    slidesPerGroup: 1,
                },
                768: {
                    slidesPerView: 3,
                    slidesPerGroup: 1,
                },
                992: {
                    slidesPerView: 4,
                    slidesPerGroup: 2,
                },
                1200: {
                    slidesPerView: 5,
                    slidesPerGroup: 2,
                },
            }
        });
        new Swiper('.top-brand-discounts-highlight-swiper>.swiper-container', {
            slidesPerView: 1,
            slidesPerGroup: 1,
        });
        new Swiper('.product-flash-sale-swiper>.swiper-container', {
            slidesPerView: 2,
            slidesPerGroup: 1,
            spaceBetween: 8,
            autoplay: {
                delay: 2400,
            },
            navigation: {
                nextEl: '.product-flash-sale-swiper>.swiper-button-next',
                prevEl: '.product-flash-sale-swiper>.swiper-button-prev',
            },
            breakpoints: {
                576: {
                    slidesPerView: 2,
                    slidesPerGroup: 1,
                },
                768: {
                    slidesPerView: 3,
                    slidesPerGroup: 1,
                },
                992: {
                    slidesPerView: 4,
                    slidesPerGroup: 2,
                },
                1200: {
                    slidesPerView: 6,
                    slidesPerGroup: 2,
                },
            }
        });
        new Swiper('.head-banners-swiper>.swiper-container', {
            slidesPerView: 1,
            slidesPerGroup: 1,
            spaceBetween: 0,
            loop: true,
            //effect: 'fade',
            autoplay: {
                delay: 2380,
            },
            pagination: {
                el: '.head-banners-swiper .swiper-pagination',
                dynamicBullets: true,
                clickable: true,
            },
        });

        new Swiper('.single-banner-swiper>.swiper-container', {
            slidesPerView: 1,
            slidesPerGroup: 1,
            loop: true,
            autoplay: {
                delay: 2505,
            },
            pagination: {
                el: '.single-banner-swiper .swiper-pagination',
                dynamicBullets: true,
                clickable: true,
            },
        });

        new Swiper('.categories-icon-swiper>.swiper-container', {
            slidesPerView: 5,
            slidesPerGroup: 2,
            spaceBetween: 8,
            watchOverflow: true,
            autoplay: {
                delay: 2420,
            },
            pagination: {
                el: '.categories-icon-swiper .swiper-pagination',
                dynamicBullets: false,
                clickable: true,
            },
            breakpoints: {
                576: {
                    slidesPerView: 5,
                    slidesPerGroup: 2,
                    pagination: false,
                },
                768: {
                    slidesPerView: 5,
                    slidesPerGroup: 2,
                    pagination: false,
                },
                992: {
                    slidesPerView: 8,
                    slidesPerGroup: 2,
                    pagination: false,
                },
                1200: {
                    slidesPerView: 8,
                    slidesPerGroup: 2,
                    pagination: false,
                },
            }
        });

        var childCategoriesDelay;

        function showChildCategories(id) {
            @if(isset($category_all) && !empty($category_all))
                @foreach($category_all as $key => $cat)
                document.getElementById('category{{$cat['id']}}').style.display = 'none';
                @endforeach
            @endif
            document.getElementById(id).style.display = 'block';
        }

        function showChildCategoriesDelay(id) {
            childCategoriesDelay = setTimeout(() => {
                document.getElementById(id).style.display = 'none';
            }, 500);
        }

        function clearShowChildCategoriesDelay(id) {
            clearTimeout(childCategoriesDelay);        
        }

        function hideChildCategories(id) {
            childCategoriesDelay = setTimeout(() => {
                document.getElementById(id).style.display = 'none';
            }, 10);            
        }
    </script>


</body></html>
    <link rel="stylesheet" href="{{ pages_path('homepage/css/homepage.css') }}">
    <script src="{{asset('public/assets/js/new_template/popper.min.js')}}"></script>
    <script src="{{asset('public/assets/js/new_template/bootstrap.js')}}"></script>
    <script type="text/javascript" src="{{ asset('public/template/js/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/template/js/tether.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/template/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/template/js/jquery-migrate.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/template/js/hidemaxlistitem.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/template/js/jquery-ui.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/template/js/hidemaxlistitem.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/template/js/jquery.easing.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/template/js/scrollup.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/template/js/jquery.waypoints.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/template/js/waypoints-sticky.min.js') }}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.1/axios.min.js"></script>
    <script type="text/javascript" src="{{ asset('public/assets/vendor/jquery.loading.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/template/js/pace.min.js') }}"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.20/lodash.min.js"></script>
    <script type="text/javascript" src="{{ asset('public/template/js/slick.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/template/js/scripts.js?version=100') }}"></script>
    <script type="text/javascript" src="{{ asset('public/assets/js/bungkie.js?version=100') }}"></script>
    <script type="text/javascript" async src="{{ asset('public/assets/vendor/lazyload.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue@2"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css">
    <link href="{{ asset('public/assets/vendor/chosen/chosen.css') }}" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/additional-methods.min.js"></script>
    <script src="{{ asset('public/assets/vendor/chosen/chosen.jquery.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendor/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js') }}"></script>
    <script src="{{ pages_path('homepage/js/new_template.js') }}"></script>
    <script src="{{ pages_path('homepage/js/plugin/slider.js') }}"></script>
    
    @php
        $lang = (\Session::get('lang') == 'en') ? 'en' : 'th';
    @endphp
    <script src="{{ asset('public/assets/location/location_'. $lang .'.js') }}"></script>
    <style>
        

        .full-screen {         
            max-width: 100% !important;
            
        }
        @media (min-width: 576px) {
        .full-screen {         
            max-width: 100% !important;
                
            }
        }          

        @media (min-width: 768px) {
        .full-screen {         
            max-width: 100% !important;    
            }
        }
        
        .partner_icon{
            cursor: default !important;
        }
        
    </style>