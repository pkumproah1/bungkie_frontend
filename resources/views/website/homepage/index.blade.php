@extends('layouts.bungkie.homepage')
@section('modalimage')
    @includeChild('function.modal', ['modal' => array(
        'name' => 'promotion',
        'url' => url('product') . '/1/productname', 
        'image' => "https://cf.shopee.co.th/file/6b2f62b9ebc7aab8de0e8dfef64052a0_xxhdpi",
        'status' => false
    )])
@endsection
@section('content')
    <div class="homepage">
        @includeChild('categories')
            @includeChild('product',[])
    </div>
@endsection

@section('css')
    <link rel="stylesheet" href="{{ pages_path('homepage/css/homepage.css') }}">
@endsection
@section('js')
    <script src="https://cdn.jsdelivr.net/npm/vue@2"></script>
    <script src="{{ pages_path('homepage/js/homepage.js') }}"></script>
    <script src="{{ pages_path('homepage/js/plugin/slider.js') }}"></script>
@endsection