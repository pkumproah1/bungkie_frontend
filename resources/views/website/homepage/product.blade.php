<section id="productList" class="mt-5">

    <div class="bungkie">
    @if (isset($product) && !empty($product))

    @if (isset($product['newArrival']) && !empty($product['newArrival']))
        <div class="bungkie-header">
            <span class="text">{{__('messages.newArrival')}}</span>
        </div>
        <div class="bungkie-content">
            <div id="prarrow" class="arrow-product"></div>
            <div class="product-carousel">
                    @foreach($product['newArrival'] as $p)
                        <div class="item">
                            <div class="util-grid-product-item">
                                <div class="frame">
                                    <a href="{{ url('/') . '/product/' . $p['product_id'] . '/' . $p['url'] }}">
                                        <div class="content top">
                                            @if ($p['currency']['display_percent'] !== "0")
                                            <span class="label success">-{{ $p['currency']['display_percent'] }}%</span>
                                            @endif
                                            <div class="thumbnail">
                                                <img class="image" src="{{ $p['images'] }}" alt="{{ $p['name'] }}">  
                                            </div>
                                            <div class="meta">
                                                <p class="text">{{ $p['name'] }}</p>
                                            </div>
                                        </div>
                                    </a>
                                    <div class="content bottom">
                                        <p class="price sale">{{ $currency }}{{ $p['currency']['price'] }}</p>
                                        @if ($p['currency']['display_percent'] !== "0")
                                            <p class="price full">{{ $currency }}{{ $p['currency']['base'] }}</p>
                                        @else
                                            <p class="price full" style="text-decoration: none;">&nbsp;</p>
                                        @endif
                                        <div class="meta">
                                            <div class="left">
                                                <div class="rating">
                                                    @if (isset($p['rating']) && !empty($p['rating']))
                                                        {{ get_rating_stars($p['rating']['star']) }}
                                                    @else
                                                        <img class="icon" src="{{ asset('public/assets/images/icon/star-empty.png') }}"/>
                                                        <img class="icon" src="{{ asset('public/assets/images/icon/star-empty.png') }}"/>
                                                        <img class="icon" src="{{ asset('public/assets/images/icon/star-empty.png') }}"/>
                                                        <img class="icon" src="{{ asset('public/assets/images/icon/star-empty.png') }}"/>
                                                        <img class="icon" src="{{ asset('public/assets/images/icon/star-empty.png') }}"/>
                                                    @endif
                                                </div>
                                            </div>
                                            <!-- <div class="right">
                                                <div class="action">
                                                    <a class="link" href="#"><i class="icon tm tm-compare"></i></a>
                                                    <a class="link" href="#"><i class="icon tm tm-favorites"></i></a>
                                                </div>
                                            </div> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
            </div>
        </div>
    @endif

    @if (isset($product['hotDeals']) && !empty($product['hotDeals']))
        <div class="bungkie-header">
            <span class="text">Hot Deals</span>
        </div>
        <div class="bungkie-content">
            <div id="prarrow" class="arrow-product"></div>
            <div class="product-carousel">
                    @foreach($product['hotDeals'] as $p)
                        <div class="item">
                            <div class="util-grid-product-item">
                                <div class="frame">
                                    <a href="{{ url('/') . '/product/' . $p['product_id'] . '/' . $p['url'] }}">
                                        <div class="content top">
                                            @if ($p['currency']['display_percent'] !== "0")
                                            <span class="label success">-{{ $p['currency']['display_percent'] }}%</span>
                                            @endif
                                            <div class="thumbnail">
                                                <img class="image" src="{{ $p['images'] }}" alt="{{ $p['name'] }}">  
                                            </div>
                                            <div class="meta">
                                                <p class="text">{{ $p['name'] }}</p>
                                            </div>
                                        </div>
                                    </a>
                                    <div class="content bottom">
                                        <p class="price sale">{{ $currency }}{{ $p['currency']['price'] }}</p>
                                        @if ($p['currency']['display_percent'] !== "0")
                                            <p class="price full">{{ $currency }}{{ $p['currency']['base'] }}</p>
                                        @else
                                            <p class="price full" style="text-decoration: none;">&nbsp;</p>
                                        @endif
                                        <div class="meta">
                                            <div class="left">
                                                <div class="rating">
                                                    @if (isset($p['rating']) && !empty($p['rating']))
                                                        {{ get_rating_stars($p['rating']['star']) }}
                                                    @else
                                                        <img class="icon" src="{{ asset('public/assets/images/icon/star-empty.png') }}"/>
                                                        <img class="icon" src="{{ asset('public/assets/images/icon/star-empty.png') }}"/>
                                                        <img class="icon" src="{{ asset('public/assets/images/icon/star-empty.png') }}"/>
                                                        <img class="icon" src="{{ asset('public/assets/images/icon/star-empty.png') }}"/>
                                                        <img class="icon" src="{{ asset('public/assets/images/icon/star-empty.png') }}"/>
                                                    @endif
                                                </div>
                                            </div>
                                            <!-- <div class="right">
                                                <div class="action">
                                                    <a class="link" href="#"><i class="icon tm tm-compare"></i></a>
                                                    <a class="link" href="#"><i class="icon tm tm-favorites"></i></a>
                                                </div>
                                            </div> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
            </div>
        </div>
    @endif

    
    @foreach($product['category'] as $key => $prod)
        <div class="bungkie-header">
            <span class="text">{{$prod['category']['name']}}</span>
        </div>
        <div class="bungkie-content">
            <div id="prarrow" class="arrow-product"></div>
            <div class="product-carousel">
                    @foreach($prod['data'] as $p)
                        <div class="item">
                            <div class="util-grid-product-item">
                                <div class="frame">
                                    <a href="{{ url('/') . '/product/' . $p['product_id'] . '/' . $p['url'] }}">
                                        <div class="content top">
                                            @if ($p['currency']['display_percent'] !== "0")
                                            <span class="label success">-{{ $p['currency']['display_percent'] }}%</span>
                                            @endif
                                            <div class="thumbnail">
                                                <img class="image" src="{{ $p['images'] }}" alt="{{ $p['name'] }}">  
                                            </div>
                                            <div class="meta">
                                                <p class="text">{{ $p['name'] }}</p>
                                            </div>
                                        </div>
                                    </a>
                                    <div class="content bottom">
                                        <p class="price sale">{{ $currency }}{{ $p['currency']['price'] }}</p>
                                        @if ($p['currency']['display_percent'] !== "0")
                                            <p class="price full">{{ $currency }}{{ $p['currency']['base'] }}</p>
                                        @else
                                            <p class="price full" style="text-decoration: none;">&nbsp;</p>
                                        @endif
                                        <div class="meta">
                                            <div class="left">
                                                <div class="rating">
                                                    @if (isset($p['rating']) && !empty($p['rating']))
                                                        {{ get_rating_stars($p['rating']['star']) }}
                                                    @else
                                                        <img class="icon" src="{{ asset('public/assets/images/icon/star-empty.png') }}"/>
                                                        <img class="icon" src="{{ asset('public/assets/images/icon/star-empty.png') }}"/>
                                                        <img class="icon" src="{{ asset('public/assets/images/icon/star-empty.png') }}"/>
                                                        <img class="icon" src="{{ asset('public/assets/images/icon/star-empty.png') }}"/>
                                                        <img class="icon" src="{{ asset('public/assets/images/icon/star-empty.png') }}"/>
                                                    @endif
                                                </div>
                                            </div>
                                            <!-- <div class="right">
                                                <div class="action">
                                                    <a class="link" href="#"><i class="icon tm tm-compare"></i></a>
                                                    <a class="link" href="#"><i class="icon tm tm-favorites"></i></a>
                                                </div>
                                            </div> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
            </div>
        </div>
    @endforeach

    @if (isset($product['recommend']) && !empty($product['recommend']))
        <div class="bungkie-header">
            <span class="text">{{__('messages.recommend')}}</span>
        </div>
        <div class="bungkie-content">
            <div id="prarrow" class="arrow-product"></div>
            <div class="product-carousel">
                    @foreach($product['recommend'] as $p)
                        <div class="item">
                            <div class="util-grid-product-item">
                                <div class="frame">
                                    <a href="{{ url('/') . '/product/' . $p['product_id'] . '/' . $p['url'] }}">
                                        <div class="content top">
                                            @if ($p['currency']['display_percent'] !== "0")
                                            <span class="label success">-{{ $p['currency']['display_percent'] }}%</span>
                                            @endif
                                            <div class="thumbnail">
                                                <img class="image" src="{{ $p['images'] }}" alt="{{ $p['name'] }}">  
                                            </div>
                                            <div class="meta">
                                                <p class="text">{{ $p['name'] }}</p>
                                            </div>
                                        </div>
                                    </a>
                                    <div class="content bottom">
                                        <p class="price sale">{{ $currency }}{{ $p['currency']['price'] }}</p>
                                        @if ($p['currency']['display_percent'] !== "0")
                                            <p class="price full">{{ $currency }}{{ $p['currency']['base'] }}</p>
                                        @else
                                            <p class="price full" style="text-decoration: none;">&nbsp;</p>
                                        @endif
                                        <div class="meta">
                                            <div class="left">
                                                <div class="rating">
                                                    @if (isset($p['rating']) && !empty($p['rating']))
                                                        {{ get_rating_stars($p['rating']['star']) }}
                                                    @else
                                                        <img class="icon" src="{{ asset('public/assets/images/icon/star-empty.png') }}"/>
                                                        <img class="icon" src="{{ asset('public/assets/images/icon/star-empty.png') }}"/>
                                                        <img class="icon" src="{{ asset('public/assets/images/icon/star-empty.png') }}"/>
                                                        <img class="icon" src="{{ asset('public/assets/images/icon/star-empty.png') }}"/>
                                                        <img class="icon" src="{{ asset('public/assets/images/icon/star-empty.png') }}"/>
                                                    @endif
                                                </div>
                                            </div>
                                            <!-- <div class="right">
                                                <div class="action">
                                                    <a class="link" href="#"><i class="icon tm tm-compare"></i></a>
                                                    <a class="link" href="#"><i class="icon tm tm-favorites"></i></a>
                                                </div>
                                            </div> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
            </div>
        </div>
    @endif
@endif
    </div>
</section>