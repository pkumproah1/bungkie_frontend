@extends('layouts.bungkie.homepage')
@section('breadcrumb')
    
    @include('layouts.bungkie.2021.breadcrumb', [
        'name' => __('messages.lost_password'),
        
    ])
@endsection

@section('content')
<section>    
    <div class="type-page hentry">
        <header class="entry-header">
            <div class="page-header-caption" style="text-align: center;">
                <h1 class="entry-title">{{__('messages.lost_password')}}</h1>
            </div>
        </header><!-- .entry-header -->

        <div class="entry-content">
            <div class="woocommerce">
                <form  class=" lost-password" method="post">
                @csrf
                
                <div class="row">
                    <div class="offset-md-3 col-md-6 text-center">
                        <p class="d-block my-5">{{__('messages.lost_password_detail')}}</p>
                        <div class="d-flex justify-content-center" style="height: 150px">
                           
                            <div style="width: 450px;">
                                <input placeholder="{{__('messages.enter_your_email')}}" class="woocommerce-Input woocommerce-Input--text input-text form-control" type="email" name="email" id="email" autocomplete="off" required />
                            </div>
                            
                            <div style="width: 200px;">
                                <button type="submit" id="reset_password" class="woocommerce-Button btn-sm button" value="{{__('messages.reset_password')}}">{{__('messages.reset_password')}}</button>
                            </div>
                            
                        </div>
                    </div>
                </div>
                
                
                </form><!-- .track_order -->
            </div><!-- .woocommerce -->
        </div><!-- .entry-content -->
    </div><!-- .hentry -->
</section>
    
@endsection


@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css">
    <link href="{{ asset('public/assets/vendor/chosen/chosen.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ pages_path('cart/css/checkout.css') }}">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('js')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue@2"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/additional-methods.min.js"></script>
    <script src="{{ asset('public/assets/vendor/chosen/chosen.jquery.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendor/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js') }}"></script>
    <script src="{{ pages_path('homepage/js/lost.js') }}"></script>
@endsection

@php
    $lang = (\Session::get('lang') == 'en') ? 'en' : 'th';
@endphp
<script src="{{ asset('public/assets/location/location_'. $lang .'.js') }}"></script>