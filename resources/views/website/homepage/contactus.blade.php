@extends('layouts.bungkie.homepage')
@section('breadcrumb')
    
    @include('layouts.bungkie.2021.breadcrumb', [
        'name' => __('messages.contact_us'),
        
    ])
@endsection

@section('content')
    <div class="type-page hentry">
        <div class="entry-content">
            <div class="stretch-full-width-map">
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d968.8834068375472!2d100.573735!3d13.746662!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e29f97cc6251b5%3A0xd290486d7a49b331!2sCentral%20Siam%20Public%20(Thailand)%20Co.%2C%20Ltd.!5e0!3m2!1sth!2sth!4v1616562510059!5m2!1sth!2sth" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
            </div>
            <!-- .stretch-full-width-map -->
            <div class="row contact-info">
                <div class="col-md-9 left-col">
                    <div class="text-block">
                        <h2 class="contact-page-title">{{__('messages.leave_us_a_message')}}</h2>
                        <!-- <p>Maecenas dolor elit, semper a sem sed, pulvinar molestie lacus. Aliquam dignissim, elit non mattis ultrices,
                            <br> neque odio ultricies tellus, eu porttitor nisl ipsum eu massa.</p> -->
                    </div>
                    <div class="contact-form">
                        <div role="form" class="wpcf7" id="wpcf7-f425-o1" lang="en-US" dir="ltr">
                            <div class="screen-reader-response"></div>
                            <form class="wpcf7-form" method='post'>
                                <div class="form-group row">
                                    <div class="col-xs-12 col-md-6">
                                        <label>{{__('messages.fname')}} - {{__('messages.lname')}}
                                            <abbr title="required" class="required">*</abbr>
                                        </label>
                                        <br>
                                        <span class="wpcf7-form-control-wrap first-name">
                                            <input type="text" aria-invalid="false" aria-required="true" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required input-text form-control" required size="40" value="" name="first-name">
                                        </span>
                                    </div>
                                    <!-- .col -->
                                    <div class="col-xs-12 col-md-6">
                                        <label>{{__('messages.email')}}
                                            <abbr title="required" class="required">*</abbr>
                                        </label>
                                        <br>
                                        <span class="wpcf7-form-control-wrap last-name">
                                            <input type="email" required aria-invalid="false" aria-required="true" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required input-text form-control" size="40" value=""  name="email">
                                        </span>
                                    </div>
                                    <!-- .col -->
                                </div>
                                <!-- .form-group -->
                                <div class="form-group">
                                    <label>{{__('messages.subject')}}</label>
                                        <abbr title="required" class="required">*</abbr>
                                    <br>
                                    <span class="wpcf7-form-control-wrap subject">
                                        <input type="text" required aria-invalid="false" form-control class="wpcf7-form-control wpcf7-text input-text form-control" size="40" value="" name="subject">
                                    </span>
                                </div>
                                <!-- .form-group -->
                                <div class="form-group">
                                    <label>{{__('messages.your_message')}}</label>
                                        <abbr title="required" class="required">*</abbr>
                                    <br>
                                    <span class="wpcf7-form-control-wrap your-message">
                                        <textarea aria-invalid="false" required class="wpcf7-form-control wpcf7-textarea form-control" rows="10" cols="40" name="your-message"></textarea>
                                    </span>
                                </div>
                                <!-- .form-group-->
                                <div class="form-group clearfix">
                                    <p>
                                        <input type="submit" value="{{__('messages.send_message')}}" class="wpcf7-form-control wpcf7-submit " />
                                    </p>
                                </div>
                                <!-- .form-group-->
                                <div class="wpcf7-response-output wpcf7-display-none"></div>
                            </form>
                            <!-- .wpcf7-form -->
                        </div>
                        <!-- .wpcf7 -->
                    </div>
                    <!-- .contact-form7 -->
                </div>
                <!-- .col -->
                <div class="col-md-3 store-info">
                    <div class="text-block">
                        <h2 class="contact-page-title">{{__('messages.our_office')}}</h2>
                        <address>
                            {{__('messages.address1')}}
                            <br> {{__('messages.address2')}}
                            <br> {{__('messages.address3')}}
                        </address>
                        <!-- <h3>Hours of Operation</h3>
                        <ul class="list-unstyled operation-hours inner-right-md">
                            <li class="clearfix">
                                <span class="day">Monday:</span>
                                <span class="pull-right flip hours">12-6 PM</span>
                            </li>
                            <li class="clearfix">
                                <span class="day">Tuesday:</span>
                                <span class="pull-right flip hours">12-6 PM</span>
                            </li>
                            <li class="clearfix">
                                <span class="day">Wednesday:</span>
                                <span class="pull-right flip hours">12-6 PM</span>
                            </li>
                            <li class="clearfix">
                                <span class="day">Thursday:</span>
                                <span class="pull-right flip hours">12-6 PM</span>
                            </li>
                            <li class="clearfix">
                                <span class="day">Friday:</span>
                                <span class="pull-right flip hours">12-6 PM</span>
                            </li>
                            <li class="clearfix">
                                <span class="day">Saturday:</span>
                                <span class="pull-right flip hours">12-6 PM</span>
                            </li>
                            <li class="clearfix">
                                <span class="day">Sunday</span>
                                <span class="pull-right flip hours">Closed</span>
                            </li>
                        </ul>
                        <h3>Careers</h3>
                        <p class="inner-right-md">If you’re interested in employment opportunities at Techmarket, please email us: <a href="mailto:contact@yourstore.com">contact@yourstore.com</a></p> -->
                    </div>
                    <!-- .text-block -->
                </div>
                <!-- .col -->
            </div>
            <!-- .contact-info -->
        </div>
        <!-- .entry-content -->
    </div>
@endsection


@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css">
    <link href="{{ asset('public/assets/vendor/chosen/chosen.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ pages_path('homepage/css/homepage.css') }}">
@endsection


@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/additional-methods.min.js"></script>
    <script src="{{ asset('public/assets/vendor/chosen/chosen.jquery.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendor/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue@2"></script>
    <script src="{{ pages_path('homepage/js/contactus.js') }}"></script>
    <script src="{{ pages_path('homepage/js/plugin/slider.js') }}"></script>
@endsection