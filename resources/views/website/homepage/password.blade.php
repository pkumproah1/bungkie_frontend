@extends('layouts.bungkie.homepage')
@section('breadcrumb')
    
    @include('layouts.bungkie.2021.breadcrumb', [
        'name' => __('messages.password_change'),
        
    ])
@endsection

@section('content')
    
    <div class="type-page hentry">
        <header class="entry-header">
            <div class="page-header-caption" style="text-align: center;">
                <h1 class="entry-title">{{__('messages.password_change')}}</h1>
            </div>
        </header><!-- .entry-header -->

        <div class="entry-content">
            <div class="woocommerce">
                <div class="woocommerce-MyAccount-content">
                    <div class="woocommerce-notices-wrapper"></div>
                    <form class="woocommerce-EditAccountForm reset-password" method="post"  >
                        @csrf
                        <div class="row">
                            <div class="offset-md-2 text-center">
                                <div class="d-flex align-items-start flex-column bd-highlight mb-1 my-3" style="height: 150px">
                                
                                    <div style="width: 450px;">
                                        <label for="password_1">{{__('messages.new_password')}}</label>
                                        <input type="password" class="woocommerce-Input woocommerce-Input--password input-text form-control" name="password" id="password" autocomplete="off" minlength="6" required/>
                                    </div>

                                    <div style="width: 450px;">
                                        <label for="password_2">{{__('messages.confirm_new_password')}}</label>
                                        <input type="password" class="woocommerce-Input woocommerce-Input--password input-text form-control" name="conf-password" id="conf-password" minlength="6" autocomplete="off" required />
                                    </div>
                                    <BR>
                                    <label><input class="woocommerce-Input" type="checkbox" onclick="myFunction()"> {{__('messages.show_password')}}</label>
                                    <BR>
                                    <div style="width: 450px;">
                                        <button type="submit"  class="woocommerce-Button btn-sm button" name="save_account_details" value="Save changes">{{__('messages.save')}}</button>
                                    </div>                                    
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div><!-- .entry-content -->
    </div><!-- .hentry -->
    
@endsection

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css">
    <link href="{{ asset('public/assets/vendor/chosen/chosen.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ pages_path('cart/css/checkout.css') }}">
@endsection

@section('js')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue@2"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/additional-methods.min.js"></script>
    <script src="{{ asset('public/assets/vendor/chosen/chosen.jquery.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendor/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js') }}"></script>
    <script src="{{ pages_path('homepage/js/password.js') }}"></script>
    @php
        $lang = (\Session::get('lang') == 'en') ? 'en' : 'th';
    @endphp
    <script src="{{ asset('public/assets/location/location_'. $lang .'.js') }}"></script>
    <script>
        function myFunction() {
            var x = document.getElementById("password");
            var y = document.getElementById("conf-password");
            if (x.type === "password") {
                x.type = "text";
                y.type = "text";
            } else {
                x.type = "password";
                y.type = "password";
            }
        }
    </script>
@endsection