@extends('layouts.bungkie.pages')
{{-- breadcrumb --}}
@section('breadcrumb')
    @include('layouts.bungkie.2021.breadcrumb', [
        'name' => "Cart"
    ])
@endsection
@section('content')
<section id="cart">
    <div class="type-page hentry">
        <div class="entry-content">
            <div class="woocommerce">
                <div class="cart-wrapper-new">
                    <div class="row">
                        <div class="col-lg-9 col-md-12">
                            <div class="cart-list">
                                <div class="cart-header">
                                    <div class="check-all">
                                        <input type="checkbox" class="all" @change="selectProduct('all',$event)">
                                    </div>
                                    <div class="product">
                                        <span>สินค้า</span>
                                    </div>
                                    <div class="price-unit">
                                        <span>ราคาต่อหน่วย</span>
                                    </div>
                                    <div class="qty">
                                        <span>{{ __('messages.quantity') }}</span>
                                    </div>
                                    <div class="price-total">
                                        <span>ราคารวม</span>
                                    </div>
                                    <div class="action">
                                        <span>แอคชั่น</span>
                                    </div>
                                </div>
                                <div class="cart-body" v-for="(item, index) in carts.full.InStock" :key="index" v-if="carts.full.InStock">
                                    <div class="shop-info">
                                        <div class="check-all">
                                            <input type="checkbox" class="shop" @change="selectProduct('shop',$event)">
                                        </div>
                                        <div class="shop-name">
                                            <span class="name">@{{ item.shopName }}</span>
                                        </div>
                                    </div>
                                    
                                    <div class="product-detail" v-for="(product, indexx) in item.product" :key="indexx">
                                        <div class="check-all">
                                            <input type="checkbox" :data-id="product.productId" @change="selectProduct('product',$event)">
                                        </div>
                                        <div class="product">
                                            <div class="thumbnail">
                                                <img :src="product.image">
                                            </div>
                                            <span class="name">@{{ product.name }}</span>
                                        </div>
                                        
                                        <div class="price-unit">
                                            <span v-if="product.price.discount_percent != '0.00'" class="p-after">
                                                @{{ carts.currency }}@{{ product.price.after }}
                                            </span>
                                            <span class="text-danger">@{{ carts.currency }}@{{ product.price.price }}</span>
                                        </div>
                                        <div class="qty">
                                            <div class="quantity">
                                                <div class="left">
                                                    <div class="add-qty">
                                                        <button type="button" @click="decrement(product.productId)" class="decrement">
                                                            <span>-</span>
                                                        </button>
                                                        <span class="qty-input" :data-qty="product.price.quantity">@{{ product.price.quantity }}</span>
                                                        <button type="button" @click="increment(product.price.quantity,product.inStock,product.productId)" class="increment">
                                                            <span>+</span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="price-total">
                                            <span v-if="product.price.discount_percent != '0.00'" class="p-after">
                                                @{{ carts.currency }}@{{ product.price.before }}
                                            </span>
                                            <span class="text-danger">@{{ carts.currency }}@{{ product.price.total }}</span>
                                        </div>
                                        <div class="action">
                                            <a href="#" @click.prevent="remove(product.productId,product.price.quantity)" class="text-danger">ลบ</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-lg-3 col-md-12">
                            <div class="cart-collaterals">
                                <div class="cart_totals">
                                    <h2>Cart totals</h2>
                                    <table class="shop_table shop_table_responsive">
                                        <tbody>
                                            <tr class="cart-subtotal">
                                                <th>Subtotal</th>
                                                <td data-title="Subtotal">
                                                    <span class="woocommerce-Price-amount amount">
                                                        @{{ carts.currency }}@{{ subtotal }}
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr class="cart-subtotal">
                                                <th>Discount</th>
                                                <td data-title="Subtotal">
                                                    <span class="woocommerce-Price-amount amount">
                                                        @{{ carts.currency }}@{{ discount }}
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr class="order-total">
                                                <th>Total</th>
                                                <td data-title="Total">
                                                    <strong>
                                                        <span class="woocommerce-Price-amount amount">
                                                            @{{ carts.currency }}@{{ total }}
                                                        </span>
                                                    </strong>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <div class="wc-proceed-to-checkout">
                                        <button type="button" @click="proceedToCheckout" class="checkout-button btn-block button alt wc-forward" :disabled="!submit">
                                            Proceed to checkout
                                        </button>
                                        <a class="back-to-shopping" href="{{ url('/') }}">
                                            Back to Shopping
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('public/assets/vendor/froala_editor_3.2.5/css/froala_editor.pkgd.css') }}">
    <link rel="stylesheet" href="{{ pages_path('cart/css/cart.css') }}">
@endsection

@section('js')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue@2"></script>
    <script src="{{ pages_path('cart/js/cart.js') }}"></script>
@endsection