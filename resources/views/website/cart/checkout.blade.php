@extends('layouts.bungkie.pages')
{{-- breadcrumb --}}
@section('breadcrumb')
    @include('layouts.bungkie.2021.breadcrumb', [
        'name' => "Checkout"
    ])
@endsection
@section('content')
<section id="checkout">
    <div class="type-page hentry">
        <div class="entry-content">
            <div class="woocommerce">
                <div class="cart-wrapper-new">
                    <div class="row">
                        <div class="col-lg-9 col-md-12">
                            <div class="address">
                                <div class="content animate__fadeIn" v-if="address.page == 'select'">
                                    <div class="header">
                                        <h6><i class="icofont-location-pin"></i> {{__('messages.shipping_address')}}</h6>
                                        <a href="#" @click.prevent="address.page = 'list'" class="text-info" v-if="address.default.info">{{__('messages.change')}}</a>
                                    </div>
                                    <div class="body">
                                        <span class="selected" v-if="address.default.info">
                                            <i class="icofont-checked"></i>
                                            <strong>@{{ address.default.info.display_name }}, @{{ address.default.info.phone }}</strong>
                                            @{{ address.default.info.address }}, @{{ address.default.info.province }}, @{{ address.default.info.city }}, @{{ address.default.info.district }}, @{{ address.default.info.zipcode }}
                                        </span>
                                        <small>
                                            <a href="#" @click.prevent="address.page = 'add'" class="text-danger" v-if="!address.default.info">{{__('messages.click_to_change')}}</a>
                                        </small>
                                    </div>
                                </div>

                                <div class="content animate__fadeIn" v-if="address.page == 'list'">
                                    <div class="header">
                                        <h6><i class="icofont-address-book"></i> {{__('messages.selected')}}</h6>
                                        <a href="#" @click.prevent="address.page = 'select'" class="text-primary">{{__('messages.back')}}</a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="#" @click.prevent="address.page = 'add'" class="text-dagner">{{__('messages.add')}}</a>
                                    </div>
                                    <div class="body">
                                        <div class="lists">
                                            <ul class="nav flex-column">
                                                <li class="nav-item" v-for="(item, index) in address.list" :key="index" v-if="address.list">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="selected" v-model="address.default.id" :id="'address_' + item.address_id" :value="item.address_id">
                                                        <label class="form-check-label" :for="'address_' + item.address_id">
                                                            <strong>@{{ item.display_name }}, @{{ item.phone }}</strong>
                                                            @{{ item.address }}, @{{ item.province }}, @{{ item.city }}, @{{ item.district }}, @{{ item.zipcode }}
                                                        </label>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <div class="content animate__fadeIn" v-if="address.page == 'add'">
                                    <div class="header">
                                        <h6><i class="icofont-ui-add"></i> {{__('messages.add_item')}}</h6>
                                        <a href="#" @click.prevent="address.page = 'list'" class="text-primary">{{__('messages.back')}}</a>
                                    </div>
                                    <div class="body">
                                        <form class="add-address" method="post">
                                            <div class="add">
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <input type="text" class="form-control" id="name" placeholder="{{__('messages.fname')}}" name="name" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <input type="text" maxlength="10" class="form-control numeric" name="phone" placeholder="{{__('messages.phone')}}" id="phone" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="form-group">
                                                            <textarea class="form-control" name="address" rows="3" placeholder="{{__('messages.addresses')}}" required></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <select name="province" id="province" class="form-control chosen" required>
                                                                @if (isset($province) && !empty($province))
                                                                    <option value="">{{__('messages.town_city')}}</option>
                                                                    @foreach ($province as $key => $item)
                                                                        <option value="{{ $item['province_code'] }}">
                                                                            {{ $item['name'] }}
                                                                        </option>
                                                                    @endforeach
                                                                @endif
                                                                
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <select name="city" id="city" class="form-control chosen" required>
                                                                <option value="">{{__('messages.amphur')}}</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <select name="district" id="district" class="form-control chosen" required>
                                                                <option value="">{{__('messages.tumbon')}}</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <input class="form-control" type="text" name="zipcode" id="zipcode" readonly required>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                            <div class="footer border-top d-flex pt-2">
                                                <button type="submit" @click="addAddress" class="btn btn-primary ml-auto">{{__('messages.save')}}</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>


                            <div class="cart-list">
                                <div class="cart-header">
                                    <div class="product">
                                        <span>{{__('messages.tproduct')}}</span>
                                    </div>
                                    <div class="price-unit">
                                        <span>{{ __('messages.price_per_unit') }}</span>
                                    </div>
                                    <div class="qty">
                                        <span>{{ __('messages.quantity') }}</span>
                                    </div>
                                    <div class="price-total">
                                        <span>{{ __('messages.total_price') }}</span>
                                    </div>
                                </div>
                                <div class="cart-body">  
                                    <div class="product-detail justify-content-center" v-if="!items">
                                        <h3 class="text-danger p-3">
                                            <i class="icofont-warning"></i> {{ __('messages.warning_selection') }}
                                        </h3>
                                    </div>    
                                    
                                    <div class="product-detail" v-for="(item, index) in items" :key="index" v-if="items">
                                        <div class="product">
                                            <div class="thumbnail">
                                                <img :src="item.image">
                                            </div>
                                            <div class="name">
                                                <span>@{{ item.name }}</span>
                                                <span class="by-shop">{{ __('messages.product_from') }} @{{ item.shopName }}</span>
                                            </div>
                                            
                                        </div>
                                        
                                        <div class="price-unit">
                                            <span v-if="item.price.discount_percent != '0.00'" class="p-after">
                                                @{{ currency }}@{{ item.price.after }}
                                            </span>
                                            <span class="text-danger">@{{ currency }}@{{ item.price.price }}</span>
                                        </div>
                                        <div class="qty">
                                            <span class="x">×</span>
                                            <span>@{{ item.price.quantity }}</span>
                                        </div>
                                        <div class="price-total">
                                            <span v-if="item.price.discount_percent != '0.00'" class="p-after">
                                                @{{ currency }}@{{ item.price.before }}
                                            </span>
                                            <span class="text-danger">@{{ currency }}@{{ item.price.total }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- right --}}
                        <div class="col-lg-3 col-md-12">
                            <div class="cart-collaterals">
                                <div class="cart_totals">
                                    <h2>{{__('messages.cart_total')}}</h2>
                                    <table class="shop_table shop_table_responsive">
                                        <tbody>
                                            <tr class="cart-subtotal">
                                                <th>{{ __('messages.subtotal') }}</th>
                                                <td data-title="Subtotal">
                                                    <span class="woocommerce-Price-amount amount">
                                                       @{{ currency }}@{{ subtotal }}
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr class="cart-subtotal">
                                                <th>{{ __('messages.discount') }}</th>
                                                <td data-title="Discount">
                                                    <span class="woocommerce-Price-amount amount">
                                                       @{{ currency }}@{{ discount }}
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr class="cart-subtotal">
                                                <th>{{ __('messages.shipping_rate') }}</th>
                                                <td data-title="Shipping">
                                                    <span class="woocommerce-Price-amount amount">
                                                       @{{ currency }}@{{ shipping }}
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr class="order-total">
                                                <th>{{ __('messages.total') }}</th>
                                                <td data-title="Total">
                                                    <strong>
                                                        <span class="woocommerce-Price-amount amount">
                                                           @{{ currency }}@{{ total }} 
                                                        </span>
                                                    </strong>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <script>
                                        fbq('track', 'Purchase', {
                                            currency: "{{ $fbq_currency }}", 
                                            value: @{{ total }} 
                                        });
                                    </script>

                                    <div class="wc-proceed-to-checkout">
                                        <p class="form-row terms wc-terms-and-conditions woocommerce-validated">
                                            <label class="woocommerce-form__label woocommerce-form__label-for-checkbox checkbox">
                                        <input type="checkbox" id="terms" v-model="terms" name="terms" value="1" class="woocommerce-form__input woocommerce-form__input-checkbox input-checkbox" required> <span>{{ __('messages.term_condition') }} <a targer="_blank" class="woocommerce-terms-and-conditions-link" href="{{asset("public/assets/doc/Terms_and_Conditions_(Bungkie)_".\Session::get('lang').".pdf")}}">terms &amp; conditions</a></span> <span class="required">*</span>
                                            </label>                                        
                                        </p>
                                        <button type="button" @click="proceedPurchase" class="checkout-button btn-block button alt wc-forward" :disabled="!submit">
                                        {{ __('messages.process_to_purchase') }}
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css">
    <link href="{{ asset('public/assets/vendor/chosen/chosen.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ pages_path('cart/css/checkout.css') }}">
@endsection

@section('js')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue@2"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/additional-methods.min.js"></script>
    <script src="{{ asset('public/assets/vendor/chosen/chosen.jquery.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendor/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js') }}"></script>
    <script src="{{ pages_path('cart/js/checkout.js') }}"></script>
    @php
        $lang = (\Session::get('lang') == 'en') ? 'en' : 'th';
    @endphp
    <script src="{{ asset('public/assets/location/location_'. $lang .'.js') }}"></script>
@endsection