@extends('layouts.bungkie.pages')
{{-- breadcrumb --}}
@section('breadcrumb')
    @include('layouts.bungkie.2021.breadcrumb', [
        'name' => "Order received"
    ])
@endsection
@section('content')
<section id="cart" class="woocommerce-order-received">
<div class="page hentry">
    <div class="entry-content">
       sds
        <!-- .woocommerce -->
    </div>
    <!-- .entry-content -->
</div>
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('public/assets/vendor/froala_editor_3.2.5/css/froala_editor.pkgd.css') }}">
    <link rel="stylesheet" href="{{ pages_path('cart/css/cart.css') }}">
@endsection

@section('js')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue@2"></script>
    {{-- <script src="{{ pages_path('cart/js/cart.js') }}"></script> --}}
@endsection