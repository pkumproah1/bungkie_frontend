@extends('layouts.bungkie.pages')
{{-- breadcrumb --}}
@section('breadcrumb')
    @include('layouts.bungkie.2021.breadcrumb', [
        'name' => "Order received"
    ])
@endsection
@section('content')
<section id="cart" class="woocommerce-order-received">
    <div class="page hentry">
        <div class="entry-content">
            <div class="woocommerce">
                @if (!empty($purchase))
                    <div class="woocommerce-order">
                        @if (isset($purchase['payment_status']) && !empty($purchase['payment_status']))
                            @if($purchase['payment_status'] === "การชำระเงินสำเร็จ")
                                <p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received">
                                    Thank you. Your order has been received.
                                </p>
                            @else
                                <p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received" style="background-color: #daa226;">
                                    {{ $purchase['payment_status'] ?? ''}}
                                </p>
                            @endif
                        @endif

                        <ul class="woocommerce-order-overview woocommerce-thankyou-order-details order_details">

                            <li class="woocommerce-order-overview__order order">
                                Order number:<strong>{{ $purchase['payment_order'] ?? '' }}</strong>
                            </li>

                            <li class="woocommerce-order-overview__date date">
                                Date:<strong>{{ $purchase['transaction_datetime'] ?? '' }}</strong>
                            </li>

                            
                            <li class="woocommerce-order-overview__total total">
                                Total: 
                                <strong>
                                    <span class="woocommerce-Price-amount amount">
                                        <span class="woocommerce-Price-currencySymbol">{{ $purchase['payment_currency'] ?? '' }}</span>
                                        {{ $purchase['payment_amount'] ?? '' }}
                                    </span>
                                </strong>
                            </li>

                            <li class="woocommerce-order-overview__payment-method method">
                                    Payment method: <strong>{{ $purchase['payment_channel'] ?? '' }}</strong>
                            </li>
                        </ul>

                        <section class="woocommerce-order-details">
                            <h2 class="woocommerce-order-details__title">Order details</h2>
                            <table class="woocommerce-table woocommerce-table--order-details shop_table order_details">
                                <thead>
                                    <tr>
                                        <th class="woocommerce-table__product-name product-name">Product</th>
                                        <th class="woocommerce-table__product-table product-total">Total</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    @if(isset($purchase['products']) && !empty($purchase['products']))
                                        @foreach($purchase['products'] as $product)
                                        <tr class="woocommerce-table__line-item order_item">
                                            <td class="woocommerce-table__product-name product-name">
                                                <a href="{{ url('/') . '/product/' . $product['product_id'] . '/' .  $product['url'] }}">{{ $product['name'] }}</a>
                                                <strong class="product-quantity">× 1</strong>   
                                            </td>
                                            <td class="woocommerce-table__product-total product-total">
                                                <span class="woocommerce-Price-amount amount">
                                                    <span class="woocommerce-Price-currencySymbol">{{ $purchase['payment_currency'] ?? '' }}</span>
                                                    {{ $product['price']['price'] }}
                                                </span>
                                            </td>
                                        </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </section>
                    </div>
                @else
                    <div class="text-center">
                        <h1 class="text-danger">ไม่พบข้อมูลของท่าน</h1>
                    </div>
                @endif
            </div>
        </div>
    </div>
</section>
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('public/assets/vendor/froala_editor_3.2.5/css/froala_editor.pkgd.css') }}">
    <link rel="stylesheet" href="{{ pages_path('cart/css/cart.css') }}">
@endsection

@section('js')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue@2"></script>
    <script src="{{ pages_path('purchase/js/order-received.js') }}"></script>
@endsection