@extends('layouts.bungkie.homepage')
@section('breadcrumb')
    
    @include('layouts.bungkie.2021.breadcrumb', [
        'name' => __('messages.wishlist'),
        
    ])
@endsection

@section('content')
    
    <div class="type-page hentry">
        <header class="entry-header">
            <div class="page-header-caption" style="text-align: center;">
                <h1 class="entry-title">{{__('messages.wishlist')}}</h1>
            </div>
        </header><!-- .entry-header -->

        <div class="entry-content">
            <form class="woocommerce" method="post" action="#">
            
            <table class="shop_table cart wishlist_table">
            
                <thead>
                    <tr>
                        <th class="product-remove"></th>

                        <th class="product-thumbnail"></th>

                        <th class="product-name">
                            <span class="nobr">{{__('messages.tproduct')}}</span>
                        </th>


                        <th class="product-price">
                            <span class="nobr">
                            {{__('messages.price_per_unit')}}                   
                            </span>
                        </th>


                        <th class="product-stock-status">
                            <span class="nobr">
                            {{__('messages.stock_status')}}         
                            </span>
                        </th>

                        <th class="product-add-to-cart"></th>
                    </tr>
                </thead>            
                <tbody>
                    <tr>

                        <td class="product-remove">
                            <div>
                                <a title="Remove this product" class="remove remove_from_wishlist" href="#">×</a>
                            </div>
                        </td>

                        <td class="product-thumbnail">
                            <a href="#">
                                <img loading='lazy'  async='on'width="180" height="180" alt="" class="wp-post-image" src="#">                            
                            </a>
                        </td>

                        <td class="product-name">
                            <a href="#">bbbb</a>
                        </td>

                        <td class="product-price">
                            <ins>
                                <span class="woocommerce-Price-amount amount">
                                <span class="woocommerce-Price-currencySymbol">{{ session()->get('currency') }} </span>10.10</span>
                            </ins> 

                            <del>
                                <span class="woocommerce-Price-amount amount">
                                <span class="woocommerce-Price-currencySymbol">{{ session()->get('currency') }} </span>9.9</span>
                            </del>             
                            <ins>
                                <span class="woocommerce-Price-amount amount">
                                <span class="woocommerce-Price-currencySymbol">{{ session()->get('currency') }} </span>8.8</span>
                            </ins>             
                        </td>

                        <td class="product-stock-status">
                            <span class="wishlist-in-stock">In Stock</span>
                            <span class="wishlist-not-in-stock">N/A</span>
                        </td>

                        <td class="product-add-to-cart">
                            <a class="button add_to_cart_button button alt"  href="#"> {{__('messages.add_to_cart')}} </a>
                            
                        </td>
                    </tr>

                    <tr>
                        <td colspan="6" class="product-name">
                        {{__('messages.no_item')}}, <a href="#">{{__('messages.back_to_shopping')}}</a>
                        </td>
                    </tr>
                    
                </tbody>
            </table><!-- .wishlist_table -->
        </form><!-- .woocommerce -->
        </div><!-- .entry-content -->
    </div><!-- .hentry -->
    
@endsection


@section('css')
    <link rel="stylesheet" href="{{ pages_path('homepage/css/homepage.css') }}">
@endsection


@section('js')
    <script src="https://cdn.jsdelivr.net/npm/vue@2"></script>
    <script src="{{ pages_path('homepage/js/homepage.js') }}"></script>
    <script src="{{ pages_path('homepage/js/plugin/slider.js') }}"></script>
@endsection