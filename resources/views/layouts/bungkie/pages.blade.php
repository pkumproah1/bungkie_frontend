    @include('layouts.bungkie.2021.header')
    <body class="woocommerce-active single-product full-width normal">
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NFRKHZJ" height="0" width="0"
                style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
        <script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '2835383683395893');
  fbq('track', 'PageView');
</script>
        <script>
        fbq('track', 'AddToCart');
        </script>
        <div v-cloak id="app" class="hfeed site">
            @include('layouts.bungkie.2021.header.topbar')
            @include('layouts.bungkie.2021.header.header')
            <div id="content" class="site-content" tabindex="-1">
                <div class="col-full">
                    <div class="row">
                        @yield('breadcrumb')
                        <!-- .woocommerce-breadcrumb -->
                        <div id="primary" class="content-area">
                            <main id="main" class="site-main">
                                @yield('content')
                            </main>
                            <!-- #main -->
                        </div>
                        <!-- #primary -->
                    </div>
                    <!-- .row -->
                </div>
                <!-- .col-full -->
            </div>
            <!-- #content -->
            @include('layouts.bungkie.2021.footer')
            <!-- .site-footer -->
        