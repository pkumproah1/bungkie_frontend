<!DOCTYPE html>
<html lang="en-US" itemscope="itemscope" itemtype="http://schema.org/WebPage">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
        <title>Bungkie | Jump into Happy Jungle | ซื้อขายออนไลน์ B2B2C</title>
        <meta name="baseUrl" content="{{ url('/') }}">
        <meta name="baseApi" content="{{ config('constants.web_api') }}">
        @if(\Session::has('logged'))
            <meta name="token" content="{{ \Session::get('token') }}">
        @endif
        <meta name="csrf-token" content="{!! csrf_token() !!}" />
        <meta name="robots" content="noindex, nofollow">
        <meta name="keywords" content="bungkie, e-commerce, ขายของออนไลน์, ซื้อของออนไลน์, ซื้อขายออนไลน์ B2B2C">
        <meta name="description" content="Bungkie แพลตฟอร์ม E-commerce ที่นำเสนอรูปแบบการขายสินค้าและบริการแบบออนไลน์ ผสานความต้องการของผู้ซื้อและผู้ขายไว้ด้วยกันทั้งในรูปแบบของ B2B2C">
        <meta property="og:title" content="Bungkie | Jump into Happy Jungle | ซื้อขายออนไลน์ B2B2C">
        <meta property="og:description" content="Bungkie แพลตฟอร์ม E-commerce ที่นำเสนอรูปแบบการขายสินค้าและบริการแบบออนไลน์ ผสานความต้องการของผู้ซื้อและผู้ขายไว้ด้วยกันทั้งในรูปแบบของ B2B2C">
        <meta property="og:image" content="{{ asset('public/assets/images/bungkie.png') }}">
        <meta property="og:url" content="{{ url()->current() }}"">
        <meta property="og:site_name" content="Bungkie.com">
        <meta name="twitter:card" content="{{ asset('public/assets/images/bungkie.png') }}">
        <meta name="twitter:image:alt" content="Bungkie แพลตฟอร์ม E-commerce ที่นำเสนอรูปแบบการขายสินค้าและบริการแบบออนไลน์ ผสานความต้องการของผู้ซื้อและผู้ขายไว้ด้วยกันทั้งในรูปแบบของ B2B2C">
        <meta name="twitter:site" content="Bungkie | Jump into Happy Jungle | ซื้อขายออนไลน์ B2B2C">
        <meta name="facebook-domain-verification" content="hguthmtbb830hkxznwaceezddyt7md" />
        <link rel="canonical" href="{{ url()->current() }}"/>
        <link rel="stylesheet" type="text/css" href="{{ asset('public/template/css/bootstrap.min.css') }}" media="all" />
        <link rel="stylesheet" type="text/css" href="{{ asset('public/template/css/font-awesome.min.css') }}" media="all" />
        <link rel="stylesheet" type="text/css" href="{{ asset('public/template/css/bootstrap-grid.min.css') }}" media="all" />
        <link rel="stylesheet" type="text/css" href="{{ asset('public/template/css/bootstrap-reboot.min.css') }}" media="all" />
        <link rel="stylesheet" type="text/css" href="{{ asset('public/template/css/font-techmarket.css') }}" media="all" />
        <link rel="stylesheet" type="text/css" href="{{ asset('public/template/css/slick.css') }}" media="all" />
        <link rel="stylesheet" type="text/css" href="{{ asset('public/template/css/techmarket-font-awesome.css') }}" media="all" />
        <link rel="stylesheet" type="text/css" href="{{ asset('public/template/css/slick-style.css') }}" media="all" />
        <link rel="stylesheet" type="text/css" href="{{ asset('public/template/css/animate.min.css') }}" media="all" />
        <link rel="stylesheet" type="text/css" href="{{ asset('public/css/app.css') }}" media="all" />
        <link rel="stylesheet" type="text/css" href="{{ asset('public/template/css/style.css') }}" media="all" />
        <link rel="stylesheet" type="text/css" href="{{ asset('public/template/css/colors/orange.css') }}" media="all" />
        <link rel="stylesheet" type="text/css" href="{{ asset('public/assets/fonts/icofont/icofont.min.css') }}" media="all" />
        @yield('css')
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,900" rel="stylesheet">
        <link rel="shortcut icon" href="{{ asset('public/assets/favicon.ico') }}" type="image/x-icon">
        <link rel="icon" href="{{ asset('public/assets/favicon.ico') }}" type="image/x-icon">
        <!-- Google Tag Manager -->
        <script>(function (w, d, s, l, i) {
                w[l] = w[l] || []; w[l].push({
                    'gtm.start':
                        new Date().getTime(), event: 'gtm.js'
                }); var f = d.getElementsByTagName(s)[0],
                    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
                        'https://www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', 'GTM-NFRKHZJ');</script>
        <!-- End Google Tag Manager -->
    </head>