 <footer class="util-footer">
    <div class="buyer-guide">
        <div class="col-full">
            <div class="row">
                <div class="col-6 col-md item">
                    <div class="frame">
                        <div class="row text-center justify-content-center align-items-center">
                            <div class="col-12">
                                <i class="icon icofont-world"></i>
                            </div>
                            <div class="col-12">
                                <p class="title">{{ __('messages.wwcb') }}</p>
                                <p class="subtitle">{{ __('messages.we_ship') }}</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md item">
                    <div class="frame">
                        <div class="row text-center justify-content-center align-items-center">
                            <div class="col-12">
                                <i class="icon icofont-safety"></i>
                            </div>
                            <div class="col-12">
                                <p class="title">{{ __('messages.secure_payment') }}</p>
                                <p class="subtitle">{{ __('messages.pay_with') }}</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md item">
                    <div class="frame">
                        <div class="row text-center justify-content-center align-items-center">
                            <div class="col-12">
                                <i class="icon icofont-vehicle-delivery-van"></i>
                            </div>
                            <div class="col-12">
                                <p class="title">{{ __('messages.fast_deli') }}</p>
                                <p class="subtitle">{{ __('messages.what_you_want') }}</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md item">
                    <div class="frame">
                        <div class="row text-center justify-content-center align-items-center">
                            <div class="col-12">
                                <i class="icon icofont-tag"></i>
                            </div>
                            <div class="col-12">
                                <p class="title">{{ __('messages.obb') }}</p>
                                <p class="subtitle">{{ __('messages.on_sale') }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="payment-logo">
        <div class="col-full">
            <div class="frame">
                <div class="title"><span class="text">{{ __('messages.secure_payment') }}</span></div>
                <div class="logo-group">
                    <div class="carousel" data-ride="tm-slick-carousel" data-wrap=".brands" data-slick="{&quot;slidesToShow&quot;:6,&quot;slidesToScroll&quot;:1,&quot;dots&quot;:false,&quot;arrows&quot;:true,&quot;responsive&quot;:[{&quot;breakpoint&quot;:400,&quot;settings&quot;:{&quot;slidesToShow&quot;:1,&quot;slidesToScroll&quot;:1}},{&quot;breakpoint&quot;:800,&quot;settings&quot;:{&quot;slidesToShow&quot;:3,&quot;slidesToScroll&quot;:3}},{&quot;breakpoint&quot;:992,&quot;settings&quot;:{&quot;slidesToShow&quot;:3,&quot;slidesToScroll&quot;:3}},{&quot;breakpoint&quot;:1200,&quot;settings&quot;:{&quot;slidesToShow&quot;:4,&quot;slidesToScroll&quot;:4}},{&quot;breakpoint&quot;:1400,&quot;settings&quot;:{&quot;slidesToShow&quot;:5,&quot;slidesToScroll&quot;:5}}]}">
                        <div class="brands">
                            <?php for ($i = 1; $i < 7; $i++) : ?>
                                <div class="item">
                                    <img src="{{ asset('public/assets/images/brands') . '/' . $i .'.png' }}">
                                </div>
                            <?php endfor; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="site-map">
        <div class="col-full">
            <div class="row justify-content-between">
                <div class="col-6 col-md-auto">
                    <div class="foot-menu-group">
                        <p class="header">
                            <span class="text text-uppercase">
                                {{ __('messages.footer_quick_search') }}
                            </span>
                        </p>
                        <ul class="link-list list-unstyled">
                            @if(isset($category_all) && !empty($category_all))
                                @foreach($category_all as $key => $cat)
                                    @if($key >= 4)
                                        <li class="item">
                                            <a class="link" href="{{ url('categories') . '/'.$cat['id'].'/' . $cat['url'] }}">
                                                {{ $cat['name'] }}
                                            </a>
                                        </li>
                                    @endif
                                @endforeach
                            @endif
                        </ul>
                    </div>
                </div>
                <div class="col-6 col-md-auto">
                    <div class="foot-menu-group">
                        <p class="header">
                            <span class="text text-uppercase">
                                {{ __('messages.customer_care') }}
                            </span>
                        </p>
                        <ul class="link-list list-unstyled">
                            <li class="item">
                                <a class="link" href="{{ url('member') }}">
                                    <span class="text">{{ __('messages.my_account') }}</span>
                                </a>
                            </li>
                            <li class="item">
                                <a class="link" href="{{ url('product/track-your-order') }}">
                                    <span class="text">{{ __('messages.track_order') }}</span>
                                </a>
                            </li>
                            <li class="item">
                                <a class="link" href="{{ url('product') }}">
                                    <span class="text">{{ __('messages.shop') }}</span>
                                </a>
                            </li>
                            <li class="item">
                                <a class="link" href="{{ url('/wishlist') }}">
                                    <span class="text">{{ __('messages.wishlist') }}</span>
                                </a>
                            </li>
                            <li class="item">
                                <a class="link" href="{{ url('about-us') }}">
                                    <span class="text">{{ __('messages.aboutus') }}</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-6 col-md-auto">
                    <div class="foot-menu-group">
                        <p class="header">
                            <span class="text text-uppercase">
                                {{ __('messages.follow_us') }}
                            </span>
                        </p>
                        <ul class="link-list list-unstyled">
                            <li class="item">
                                <a class="link" href="https://www.facebook.com/bungkieofficial" target="_facebook">
                                    <i class="icon icofont-facebook"></i> 
                                    <span class="text">Facebook</span>
                                </a>
                            </li>
                            <li class="item">
                                <a class="link" href="https://lin.ee/ON8SJz5" target="_line">
                                    <i class="icon icofont-line"></i> 
                                    <span class="text">Line</span>
                                </a>
                            </li>
                            <li class="item">
                                <a class="link" href="https://www.instagram.com/bungkieofficial/" target="_instagram">
                                    <i class="icon icofont-instagram"></i> 
                                    <span class="text">Instagram</span>
                                </a>
                            </li>
                            <li class="item">
                                <a class="link" href="https://www.youtube.com/channel/UCCv8pvQDWx-rkBUYh9FzJqg?view_as=subscriber" target="_youtube">
                                    <i class="icon icofont-youtube-play"></i> 
                                    <span class="text">Youtube</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-6 col-md-auto">
                    <div class="foot-menu-group">
                        <p class="header"><span class="text text-uppercase">{{ __('messages.contact') }}</span></p>
                        <ul class="link-list list-unstyled">
                            <li class="item">
                                <span class="text">
                                    <strong>{{ __('messages.phone') }}:</strong><br />
                                    {{ __('messages.contact_us_call') }}
                                </span>
                            </li>
                            <li class="item">
                                <span class="text">
                                    <strong>{{ __('messages.email_address') }}:</strong><br />
                                    <a href="mailto:info@bungkie.com">info@bungkie.com</a>
                                </span>
                            </li>
                            <li class="item">
                                <span class="text">
                                    <strong>{{ __('messages.contact1') }}:</strong><br />
                                    {{ __('messages.contact2') }}
                                </span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="coppy-right">
        <div class="col-full">
            <p class="text">Copyright © 2020 <a href="{{ url('/') }}">Bungkie.com</a>. All rights reserved.</p>
        </div>
    </div>
</footer>

</div>
        
    <script type="text/javascript" src="{{ asset('public/template/js/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/template/js/tether.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/template/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/template/js/jquery-migrate.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/template/js/hidemaxlistitem.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/template/js/jquery-ui.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/template/js/hidemaxlistitem.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/template/js/jquery.easing.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/template/js/scrollup.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/template/js/jquery.waypoints.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/template/js/waypoints-sticky.min.js') }}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.1/axios.min.js"></script>
    <script type="text/javascript" src="{{ asset('public/assets/vendor/jquery.loading.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/template/js/pace.min.js') }}"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.20/lodash.min.js"></script>
    <script type="text/javascript" src="{{ asset('public/template/js/slick.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/template/js/scripts.js?version=100') }}"></script>
    <script type="text/javascript" src="{{ asset('public/assets/js/bungkie.js?version=100') }}"></script>
    <script type="text/javascript" async src="{{ asset('public/assets/vendor/lazyload.min.js') }}"></script>
    @yield('js')
</body>
</html>