@if(isset($name))
<nav class="woocommerce-breadcrumb">
    <a href="{{ url('/') }}">Home</a>
    <span class="delimiter">
        <i class="tm tm-breadcrumbs-arrow-right"></i>
    </span>
    @if(isset($cat))
        <a href="{{ url('/') . '/' . $cat['url'] }}">{{ $cat['name'] }}</a>
        <span class="delimiter">
            <i class="tm tm-breadcrumbs-arrow-right"></i>
        </span>
    @endif
    {{ $name ?? '' }}
</nav>
@endif