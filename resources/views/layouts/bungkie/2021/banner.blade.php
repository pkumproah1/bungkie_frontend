@if(!Request::segment(1))
<section id="banners">
    @php
        $imgs = array(
            'yPSxr6dzwU2RxT2AD6P7QBMhTF2DxPevVuDXFVG7.jpg',
            'qO0E2ZiiKcE0rQ1wLb00ckj0KYGPzjTo4CijTvvF.jpg',
            'GmFtLUPfmsM2ooRWbi4rGje3V2Z07Uzmcfg0xOQH.jpg'
        );
    @endphp
    <div class="home-v1-slider home-slider">
        <div class="slider-1" style="background-image: url({{ url('/') . '/public/assets/images/banner/banner0001.jpg'}});">
        </div> 
        <div class="slider-1" style="background-image: url({{ url('/') . '/public/assets/images/banner/kingpower.png'}});">
        </div> 
        <div class="slider-1" style="background-image: url({{ url('/') . '/public/assets/images/banner/banner5.jpg'}});">
        </div>
        <div class="slider-1" style="background-image: url({{ url('/') . '/public/assets/images/banner/banner6.jpg'}});">
        </div>
        @foreach($imgs as $img)
        <div class="slider-1" style="background-image: url(http://backoffice-uat.bungkie.com/public/statics/images/products/{{ $img }});">
            {{-- <div class="caption top-right">
                <div class="button">Get Yours now
                    <i class="tm tm-long-arrow-right"></i>
                </div>
            </div> --}}
        </div>
        @endforeach
    </div>
</section>
@endif