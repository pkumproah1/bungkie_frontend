
 
 <ul id="site-header-cart" class="site-header-cart menu" v-if="carts != null">
    <li class="animate-dropdown dropdown">
        <a class="cart-contents" href="#" data-toggle="dropdown" title="View your shopping cart">
            <i class="tm tm-shopping-bag"></i>
            <span class="count" v-if="carts">@{{ carts.mini.InStock.length }}</span>
            <span class="amount text-center">
                <span class="price-label">{{ __('messages.your_cart') }}</span>
                <strong v-if="carts">@{{ carts.currency }}@{{ carts['totalPrice'] }}</strong>
            </span>
        </a>
        <ul class="dropdown-menu dropdown-menu-mini-cart">
            <li>
                <div class="widget woocommerce widget_shopping_cart">
                    <div class="widget_shopping_cart_content" v-if="carts">
                        <ul class="woocommerce-mini-cart cart_list product_list_widget ">
                            {{-- In Stock --}}
                            <li class="woocommerce-mini-cart-item mini_cart_item" v-for="(item, index) in carts.mini.InStock" :key="index" v-if="carts.mini.InStock">
                                <a :href="'{{url('/')}}/product/' + item.productId + '/' + item.url">
                                    <img :src="item.image" class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" :alt="item.name">
                                    @{{ item.name }}
                                </a>
                                <span class="quantity">
                                    @{{ item.price.quantity }} ×
                                    <span class="woocommerce-Price-amount amount">
                                        @{{ carts.currency }}@{{ item.price.total }}
                                    </span>
                                </span>
                            </li>
                        </ul>

                        <p class="woocommerce-mini-cart__total total">
                            <strong>{{  __('messages.subtotal') }}:</strong>
                            <span class="woocommerce-Price-amount amount">
                                @{{ currency }}@{{ carts['totalPrice'] }}
                            </span>
                        </p>
                        <p class="woocommerce-mini-cart__buttons buttons">
                            <a href="{{ url('cart') }}" class="button wc-forward">{{ __('messages.product_view_cart') }}</a>
                        </p>
                    </div>
                </div>
            </li>
        </ul>
    </li>
</ul>