<div class="col-full desktop-only">
    <div class="techmarket-sticky-wrap">
        <div class="row">
            <div class="site-branding">
                <a href="{{ url('/') }}" class="custom-logo-link" rel="home">
                    <img src="{{ asset('public/assets/images/bungkie-logo.png') }}" alt="Bungkie.com">
                </a>
            </div>
            {{-- departments --}}
            <div id="departments-menu" class="dropdown departments-menu">
                <button class="btn dropdown-toggle btn-block" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="tm tm-departments-thin"></i>
                    <span>All Departments</span>
                </button>
                <ul id="menu-departments-menu" class="dropdown-menu yamm departments-menu-dropdown">
                    <li class="highlight menu-item animate-dropdown">
                        <a title="{{ __('messages.all_cat') }}" href="{{ url('/') . '/categories/0/all-categories' }}">
                            {{ __('messages.all_cat') }}
                        </a>
                    </li>
                    @if(isset($category_all))
                        @foreach($category_all as $key => $cat)
                            @if($key <= 8)
                                <li class="highlight menu-item animate-dropdown">
                                    <a title="{{ $cat['name'] }}" href="{{ url('/') . '/categories/'.$cat['id'].'/' . $cat['url']  }}">{{ $cat['name'] }}</a>
                                </li>
                            @endif
                        @endforeach
                    @endif
                    
                </ul>
            </div>

            {{-- search --}}
            <form class="navbar-search" method="GET" action="{{ url('/search') }}">
                <label class="sr-only screen-reader-text" for="search">Search for:</label>
                <div class="input-group animate-dropdown">
                    <input type="hidden" name="cat_id" value="0">
                    <input type="text" id="search" value="{{$search ?? ""}}" onkeyup="document.getElementById('search_old').value=this.value" onfocus="document.getElementById('search_old').value=this.value; this.value=''" onblur="if(this.value==document.getElementById('search_old').value)this.value=document.getElementById('search_old').value" class="form-control search-field product-search-field" autocomplete="off" name="s" placeholder="{{ __('messages.search_for_product') }}" />
                    <!-- <div class="input-group-addon search-categories " style="width: 10%;" onclick="document.getElementById('search').value = ''" id="clearButton" >
                        X
                    </div> -->
                    <input type="hidden" id="search_old" value="0">
                    <div class="input-group-addon search-categories dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {{ __('messages.all_cat') }}
                    </div>

                    <ul id="menu-departments-menu" class="dropdown-menu yamm departments-menu-dropdown">
                        <li class="highlight menu-item animate-dropdown">
                            <a title="{{ __('messages.all_cat') }}" href="{{ url('/') . '/categories/all-categories' }}" data-search="0">
                                {{ __('messages.all_cat') }}
                            </a>
                        </li>
                        @if(isset($category_all))
                            @foreach($category_all as $key => $cat)
                                <li class="highlight menu-item animate-dropdown">
                                    <a title="{{ $cat['name'] }}" href="#" data-search="{{ $cat['id'] }}">{{ $cat['name'] }}</a>
                                </li>
                            @endforeach
                        @endif
                    </ul>
                    <div class="input-group-btn input-group-append">
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-search"></i>
                            <span class="search-btn">{{ __('messages.search') }}</span>
                        </button>
                    </div>
                </div>
            </form>
            
            <!-- <ul class="header-compare nav navbar-nav">
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="tm tm-compare"></i>
                        <span id="top-cart-compare-count" class="value">3</span>
                    </a>
                </li>
            </ul> -->
            
            <!-- <ul class="header-wishlist nav navbar-nav">
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="tm tm-favorites"></i>
                        <span id="top-cart-wishlist-count" class="value">3</span>
                    </a>
                </li>
            </ul> -->
            @includeChild('cart')
        </div>
    </div>
    @includeChild('stretched')
</div>