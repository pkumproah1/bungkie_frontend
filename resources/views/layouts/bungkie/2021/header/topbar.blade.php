<div class="top-bar top-bar-v10">
    <div class="col-full">
        <ul id="menu-top-bar-right" class="nav menu-top-bar-right">
            {{-- tracking --}}
            @if(!\Session::has('logged'))
            <li class="hidden-sm-down menu-item animate-dropdown">
                <a title="Track Your Order" href="{{URL::to('/tracking')}}">
                    <i class="tm tm-order-tracking"></i>{{ __('messages.track_your_order') }}
                </a>
            </li>
            @endif
            {{-- currency --}}
            <li class="menu-item menu-item-has-children animate-dropdown dropdown">
                @php
                    $currency = (session()->get('currency') === 'usd') ? "Dollar (US)" : "Baht (THB)";
                @endphp
                <a title="{{ $currency }}" data-toggle="dropdown" class="dropdown-toggle" href="#">
                    <span class="currency-text"> {{ $currency }}</span>
                    <span class="caret"></span>
                </a>
                <ul role="menu" class="dropdown-menu">
                    <li class="menu-item animate-dropdown">
                        <a title="Baht (THB)" data-currency="thb" data-name="Baht (THB)" data-current="{{ session()->get('currency') }}" href="#">
                            Baht (THB)
                        </a>
                    </li>
                    <li class="menu-item animate-dropdown">
                        <a title="Dollar (US)" data-currency="usd" data-name="Dollar (US)" data-current="{{ session()->get('currency') }}" href="#">
                            Dollar (US)
                        </a>
                    </li>
                </ul>
            </li>
            {{-- language --}}
            <li class="menu-item menu-item-has-children animate-dropdown dropdown">
                @php
                    $language = (App::getLocale() == 'th') ? array(
                        'name' => 'ภาษาไทย',
                        'icon' => 'flag-18-th'
                    ) : array(
                        'name' => 'English',
                        'icon' => 'flag-18-us'
                    );
                @endphp
                <a title="{{ $language['name'] }}" data-toggle="dropdown" class="dropdown-toggle" href="#">
                    <i class="flag flag-18 {{ $language['icon'] }}"></i> 
                    <span class="language-text">{{ $language['name'] }}</span>
                    <span class="caret"></span>
                </a>
                <ul role="menu" class="dropdown-menu">
                    <li class="menu-item animate-dropdown">
                        <a title="thailand" href="#" data-language="th" data-name="ภาษาไทย" data-current="{{ App::getLocale() }}">
                            <i class="flag flag-18 flag-18-th"></i> ภาษาไทย
                        </a>
                    </li>
                    <li class="menu-item animate-dropdown">
                        <a title="englist" href="#" data-language="en" data-name="English" data-current="{{ App::getLocale() }}">
                            <i class="flag flag-18 flag-18-us"></i> English
                        </a>
                    </li>
                </ul>
            </li>
            {{-- register --}}
            <li class="menu-item menu-item-has-children animate-dropdown dropdown">
                
                    <!-- <i class="tm tm-login-register"></i> -->
                    @if(\Session::has('logged'))
                    
                    
                        <a title="{{ $language['name'] }}" data-toggle="dropdown" class="dropdown-toggle" href="{{URL::to('/member')}}">
                            <i class="tm tm-login-register"></i>
                            <span class="language-text">{{ __('messages.hello') }} {{ \Session::get('profile')['display_name'] }}</span>
                            <span class="caret"></span>
                        </a>
                        <ul role="menu" class="dropdown-menu">
                            <li class="menu-item animate-dropdown">
                                <a href="{{URL::to('/member')}}">{{__('messages.dashboard')}}</a>
                            </li>
                            <li class="menu-item animate-dropdown">
                                <a href="{{URL::to('/member/order')}}">{{__('messages.torders')}}</a>
                            </li>
                            <li class="menu-item animate-dropdown">
                                <a href="{{URL::to('/member/address')}}">{{__('messages.addresses')}}</a>
                            </li>
                            <li class="menu-item animate-dropdown">
                                <a href="{{URL::to('/member/detail')}}">{{__('messages.account_details')}}</a>
                            </li>
                            @if(\Session::get('account_type') == 'local')
                            <li class="menu-item animate-dropdown">
                                <a href="{{URL::to('/member/password')}}">{{__('messages.password_change')}}</a>
                            </li>
                            @endif
                            <li class="menu-item animate-dropdown">
                                <a href="{{URL::to('/member/logout')}}">{{__('messages.logout')}}</a>
                            </li>
                        </ul>
                    
                    @else
                    <a title="{{ __('messages.lb_register_or_login') }}" href="{{ url('member') }}">{{ __('messages.lb_register_or_login') }}</a>
                    @endif
            </li>
        </ul>
    </div>
</div>