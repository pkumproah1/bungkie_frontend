<header id="masthead" class="site-header header-v10" style="background-image: none; ">
    @includeChild('desktop')
    <div class="col-full handheld-only">
        <div class="handheld-header">
            <div class="row">
                <div class="site-branding">
                    <a href="{{ url('/') }}" class="custom-logo-link" rel="home">
                        <img class="img-fluid" src="{{ asset('public/assets/images/bungkie-logo.png') }}" alt="Bungkie.com">
                    </a>
                </div>
                <div class="handheld-header-links">
                    <ul class="columns-3">
                        <li class="my-account">
                            <a href="{{ url('member') }}" class="has-icon">
                                <i class="tm tm-login-register"></i>
                            </a>
                        </li>
                        <!-- <li class="wishlist">
                            <a href="{{ url('wishlist') }}" class="has-icon">
                                <i class="tm tm-favorites"></i>
                                <span class="count">0</span>
                            </a>
                        </li> -->
                        <!-- <li class="compare">
                            <a href="{{ url('compare') }}" class="has-icon">
                                <i class="tm tm-compare"></i>
                                <span class="count">0</span>
                            </a>
                        </li> -->
                    </ul>
                    <!-- .columns-3 -->
                </div>
            </div>
            <!-- /.row -->
            <div class="techmarket-sticky-wrap">
                <div class="row">
                    <nav id="handheld-navigation" class="handheld-navigation" aria-label="Handheld Navigation">
                        <button class="btn navbar-toggler" type="button">
                            <i class="tm tm-departments-thin"></i>
                            <span>Menu</span>
                        </button>
                        <div class="handheld-navigation-menu">
                            <span class="tmhm-close">Close</span>
                            <ul id="menu-departments-menu-1" class="nav">
                                <!-- <li class="highlight menu-item animate-dropdown">
                                    <a title="{{ __('messages.all_cat')}}" href="{{ url('categories/0/all-categories') }}">
                                        {{ __('messages.all_cat') }}
                                    </a>
                                </li> -->
                                @if (isset($category_all))
                                    @foreach ($category_all as $cat)
                                    <li class="highlight menu-item animate-dropdown">
                                        <a title="{{ $cat['name'] }}" href="{{ url('/') . '/categories/'.$cat['id'].'/'. $cat['url']  }}">
                                            {{ $cat['name'] }}
                                        </a>
                                    </li>
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                    </nav>
                    <div class="site-search">
                        <div class="widget woocommerce widget_product_search">
                            <form role="search" method="get" class="woocommerce-product-search" action="{{ url('search') }}">
                                <label class="screen-reader-text" for="woocommerce-product-search-field-0">{{ __('messages.search_for_product') }}</label>
                                <input type="text" name="s" id="woocommerce-product-search-field-0" class="search-field" placeholder="{{ __('messages.search_for_product') }}" value="{{$search ?? ""}}"/>
                                <input type="submit" value="{{ __('messages.search') }}" />
                                <input type="hidden" name="cat_id" value="0">
                            </form>
                        </div>
                    </div>
                    <a class="handheld-header-cart-link has-icon" href="{{ url('cart') }}" title="View your shopping cart">
                        <i class="tm tm-shopping-bag"></i>
                        <span class="count" v-if="carts">@{{ carts.count }}</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</header>