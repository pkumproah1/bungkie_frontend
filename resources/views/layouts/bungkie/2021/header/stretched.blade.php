@if(!Request::segment(1))
<div class="stretched-row">
    <div class="col-full">
        <div class="row">
            <nav id="navbar-primary" class="navbar-primary" aria-label="Navbar Primary" data-nav="flex-menu">
                <ul id="menu-navbar-primary" class="nav yamm">
                    <!-- <li class="menu-item animate-dropdown">
                        <a title="{{ __('messages.all_cat') }}" href="{{ url('/') . '/categories/0/all-categories' }}">{{ __('messages.all_cat') }}</a>
                    </li> -->
                    @if(isset($category_all))
                        @foreach($category_all as $key => $cat)
                            @if($key <= 8)
                                <li class="menu-item animate-dropdown">
                                    <a title="{{ $cat['name'] }}" href="{{ url('/') . '/categories/'.$cat['id'].'/'. $cat['url']  }}">{{ $cat['name'] }}</a>
                                </li>
                            @endif
                            {{-- <li class="techmarket-flex-more-menu-item dropdown">
                                <a title="..." href="#" data-toggle="dropdown" class="dropdown-toggle">...</a>
                                <ul class="overflow-items dropdown-menu"></ul>
                            </li> --}}
                        @endforeach
                    @endif
                </ul>
            </nav>
        </div>
    </div>
</div>
@endif