<?php
return [
	'footer_quick_search' => 'ค้นหาอย่างรวดเร็ว',
    'home' => 'หน้าแรก',
    'search' => 'ค้นหา',
	'save' => 'บันทึก',
	'lb_or' => 'หรือ',
	'lb_register_or_login' => "สมัครสมาชิกหรือเข้าสู่ระบบ",
	'btn_register' => 'สมัครสมาชิก',
	'btn_login' => 'เข้าสู่ระบบ',
    'track_your_order' => 'ติดตามสถานะจัดส่งสินค้า',
    'product_checkout' => 'เช็คเอ้าท์',
    'product_view_cart' => 'ดูตะกร้าสินค้า',
    'mm1' => "มกราคม",
    'mm2' => "กุมภาพันธ์",
    'mm3' => "มีนาคม",
    'mm4' => "เมษายน",
    'mm5' => "พฤษภาคม",
    'mm6' => "มิถุนายน",
    'mm7' => "กรกฏาคม",
    'mm8' => "สิงหาคม",
    'mm9' => "กันยายน",
    'mm10' => "ตุลาคม",
    'mm11' => "พฤศจิกายน",
    'mm12' => "ธันวาคม",
    'm1' => "ม.ค.",
    'm2' => "ก.พ.",
    'm3' => "มี.ค.",
    'm4' => "ม.ย.",
    'm5' => "พ.ค.",
    'm6' => "มิ.ย.",
    'm7' => "ก.ค.",
    'm8' => "ส.ค.",
    'm9' => "ก.ย.",
    'm10' => "ต.ค.",
    'm11' => "พ.ย.",
    'm12' => "ธ.ค.",
    'dd1' => "วันจันทร์",
    'dd2' => "วันอังคาร",
    'dd3' => "วันพุธ",
    'dd4' => "วันพฤหัสบดี",
    'dd5' => "วันศุกร์",
    'dd6' => "วันเสาร์",
    'dd7' => "วันอาทิตย์",
    'd1' => "จ.",
    'd2' => "อ.",
    'd3' => "พ.",
    'd4' => "พฤ.",
    'd5' => "ศ.",
    'd6' => "ส.",
    'd7' => "อา.",
    'nocomment' => "ยังไม่มีความคิดเห็น",
	'related_product' => "สินค้าที่เกี่ยวข้อง",
	'description' => "รายละะอียด",
	'review' => "รีวิว",
	'out_of' => "จาก",
	'rated' => "เรท",
	'your_review' => "การรีวิวของคุณ",
	'your_rating' => "เรทติ้งของคุณ",
	'add_a_review' => "เพิ่มรีวิว",
	'name' => "ชื่อ",
	'email' => "อีเมล์",
	'recently_product' => "สินค้าที่เพิ่งรับชม",
	'sign_up_to_newsletter' => "ลงทะเบียนเพื่อรับอีเมล์",
	'sign_up' => "ลงทะเบียน",
	'enter_your_email' => "กรุณาใส่อีเมล์",
	'add_to_wishlist' => "เพิ่มในรายการโปรด",
	'add_to_cart' => "เพิ่มใส่ตะกร้า",
	'add_to_compare' => "เพิ่มเพื่อเปรียบเทียบ",
	'shop' => "ร้านค้า",
	'home' => "หน้าหลัก",
	'read_more' => "อ่านเพิ่มเติม",
	'latest_product' => "สินค้าล่าสุด",
	'browse_category' => "เข้าชมหมวดหมู่ต่างๆ",
	'filter' => "ตัวกรอง",
	'filter_by_price' => "คัดกรองจากราคา",
	'show_all_category' => "แสดงหมวดหมู่ทั้งหมด",
	'search_for_product' => "ค้นหาสินค้า",
	'search_result_for' => "ผลลัพธ์การค้นหาสำหรับ",
	'signup_alert' => "ลงทะเบียนแล้ว",
	'signup_complete' => "ขอบคุณสำหรับการลงทะเบียน",
	'subtotal' => "รวมทั้งหมด",
	'view_cart' => "ดูตะกร้าสินค้า",
	'checkout' => "เช็คเอ้าท์",
	'your_cart' => "ตะกร้าสินค้าของคุณ",
	'proceed_to_checkout' => "ไปที่เช็คเอาท์",
	'shipping' => "กำลังจัดส่ง",
	'flat_rate' => "อัตราคงที่",
	'total' => "ทั้งหมด",
	'cart_total' => "จำนวนสินค้าในตะกร้าทั้งหมด",
	'back_to_shopping' => "กลับไปช้อปปิ้งต่อ",
	'tproduct' => "สินค้า",
	'tprice' => "ราคา",
	'quantity' => "จำนวน",
	'unit'=> "ชิ้น",
	'all_product' => "มีสินค้าทั้งหมด",
	'total' => "ทั้งหมด",
	'update_cart' => "อัพเดทตะกร้าสินค้า",
	'product_remove_cart' => "สินค้าได้ถูกนำออกจากตะกร้าสินค้าเรียบร้อยแล้ว",
	'confirm_remove_cart' => "นำสินค้าชิ้นนี้ออกจากตะกร้าสินค้า?",
	'no_register_email' => "ไม่พบอีเมล์",
	'found_register_email' => "ส่งอีเมล์รีเซ็ตแล้ว",
	'register_or_sign_in' => "ลงทะเบียนใหม่ หรือ เข้าสู่ระบบ",
	'my_account' => "บัญชีของฉัน",
	'text_in_login' => "ข้อความแสดงในส่วนหน้าต่างการเข้าสู่ระบบ",
	'text_in_register' => "ลงทะเบียนสมัครบัญชีใหม่วันนี้ รับสิทธิประโยชน์มากมายในการช้อปปิ้ง พิเศษเฉพาะคุณเท่านั้น",
	'login_error' => "ไม่พบอีเมล์หรือพาสเวิร์ดนี้",
	'login_fail_exceed' => "เข้าสู่ระบบล้มเหลวเกินจำนวนครั้งที่กำหนด",
	'register_error' => "อีเมล์นี้ได้ถูกลงทะเบียนใช้งานแล้ว.",
	'register_fail_exceed' => "ลงทะเบียนล้มเหลวเกินจำนวนครั้งที่กำหนด",
	'product_added' => "เพิ่มสินค้าแล้ว",
	'contact_us_call' => " (+66) 02-114-7936 (5 คู่สาย)",
	'na' => "N/A",
	'password_is_too_short' => "รหัสผ่านต้องประกอบด้วย ตัวอักษรพิมพ์เล็กและใหญ่ ตัวเลข และอักขระพิเศษ อย่างละ 1 ตัว ความยาว 8 อักษร",
	'status' => "สถานะ",
	'tdate' => "วันที่ทำรายการ",
	'lb_password' => "รหัสผ่าน",
	'lb_conf_password' => "ยืนยันรหัสผ่าน",
	'out_of_stock' => "สินค้าหมด",
	'order_detail' => "รายละเอียดคำสั่งซื้อ",
	'track_your_order' => " ติดตามสถานะจัดส่งสินค้า",
	'country' => " ประเทศ",
	'language' => " ภาษา",
	'tcurrency' => " ค่าเงิน",
	'save' => " บันทึก",
	'all_cat' => " ทุกหมวดหมู่",
	'contact1' => " เวลาทำการ Bungkie Call Center",
	'contact2' => " วันจันทร์ - วันศุกร์ 09.00 – 18.00 น.",
	'contel' => "(+66) 02-114-7936 (5 คู่สาย)",
	'safe_payment' => " วิธีการชำระเงิน",
	'find_it_fast' => " ค้นหาอย่างรวดเร็ว",
	'customer_care' => " ศูนย์ดูแลลูกค้า",
	'track_order' => " ติดตามสถานการณ์จัดส่งสินค้า",
	'shop' => " ซื้อสินค้า",
	'wishlist' => " รายการโปรด",
	'aboutus' => " เกี่ยวกับเรา",
	'wholesale' => "สั่งสินค้าราคาส่ง",
	'contact_us' => "ติดต่อเรา",
	'secure_payment' => "ชำระเงินปลอดภัย",
	'wwcb' => "ลูกค้าทั่วทุกมุมโลก",
	'we_ship' => "จัดส่งทั่วโลกกว่า 200 ประเทศ และภูมิภาค",
	'pay_with' => "ชำระเงินด้วยระบบการจ่ายเงินชั้นนำระดับโลก",
	'fast_deli' => "จัดส่งรวดเร็ว",
	'what_you_want' => "ทุกอย่างที่คุณต้องการ เราจะสรรหามาให้คุณถึงที่",
	'obb' => "คัดสินค้าที่ดีที่สุด",
	'on_sale' => "สินค้าราคาโปรโมชั่นมีพร้อมเสมอบนบังกี้",
	'follow_us' => "ติดตามเรา",
	'dashboard' => "หน้าหลัก",
	'torders' => "คำสั่งซื้อ",
	'addresses' => "ที่อยู่",
	'account_details' => "รายละเอียดบัญชี",
	'order_details' => "รายละเอียดคำสั่งซื้อ",
	'tview' => "ดู",
	'logout' => "ออกจากระบบ",
	'account_edit' => "แก้ไขบัญชี",
	'billing_address' => "ที่อยู่ในการวางบิลล์",
	'shipping_address' => "ที่อยู่ในการจัดส่ง",
	'hello' => "สวัสดี",
	'wholesale' => "ติดต่อขายส่ง",
	'wholesale_submit' => "ขอบคุณสำหรับข้อมูลการค้าส่ง",
	'fname' => "ชื่อ",
	'lname' => "นามสกุล",
	'company_name' => "ชื่อบริษัท",
	'optional' => 'เพิ่มเติม',
	'street_address' => "ถนน",
	'town_city' => "จังหวัด/เมือง",
	'state' => "รัฐ",
	'postcode' => "รหัสไปรษณีย์",
	'phone' => "หมายเลขโทรศัพท์",
	'email_address' => "อีเมล์",
	'contact' => "ข้อมูลติดต่อ",
	'description' => "รายละเอียด",
	'note' => 'หมายเหตุ',
	'unit' => 'หน่วย',
	'birthday' => "วันเกิด",
	'current_password' => "รหัสผ่านปัจจุบัน ",
	'new_password' => "รหัสผ่านใหม่ ",
	'confirm_new_password' => "ยืนยันรหัสผ่านใหม่",
	'password_change' => "เปลี่ยนรหัสผ่าน",
	'display_name' => "ชื่อที่แสดง",
	'display_notice' => "เป็นชื่อที่ใช้แสดงในบัญชีนี้",
	'default_address' => "ที่อยู่ด้านล่างจะถูกใช้ตอน checkout โดยอัตโนมัติ",
	'order_description' => 'อีเมล์ที่ใช้ในการสั่งซื้อ',
	'orderid' => 'Order ID',
	'tracking_detail' => 'กรุณาใส่ Order ID ลงบนช่องด้านล่าง จากนั้นกดปุ่ม "ติดตาม" ซึ่ง Order ID จะอยู่ในอีเมล์ที่ใช้ในการสั่งซื้อและคุณจะได้รับหลังจากการสั่งซื้อสินค้าแล้ว',
	'track_btn' => 'ติดตาม',
	'change' => 'เปลี่ยน',
	'click_to_change' => 'คลิ๊กที่นี้เพื่อเพิ่มที่อยู่',
	'add' => 'เพิ่ม',
	'back' => 'กลับ',
	'selected' => 'เลือกรายการ',
	'add_item' => 'เพิ่มที่อยู่',
	'amphur' => 'อำเภอ',
	'tumbon' => 'ตำบล',
	'price_per_unit' => 'ราคาต่อหน่วย',
	'total_price' => 'ราคารวม',
	'warning_selection' => 'คุณต้องเลือกสินค้าก่อนจึงจะชำระเงินได้',
	'product_from' => 'สินค้าจากร้าน',
	'discount' => 'ส่วนลด',
	'shipping_rate' => 'ค่าจัดส่ง',
	'process_to_purchase' => 'ดำเนินการชำระเงิน',
	'stock_status' => 'สต๊อก',
	'no_item' => 'ยังไม่มีรายการ',
	'not_to' => 'ไม่ใช่',
	'intro' => 'นี่คือกระดานข่าวที่คุณสามารถที่จะดูรายการสั่งซื้อ จัดการที่อยู่ในการจัดส่งและออกบิลล์ และแก้ไขรหัสผ่านรวมถึงข้อมูลส่วนตัว',
	'lost_password' => 'ลืมรหัสผ่าน?',
	'lost_password_detail' => 'ลืมรหัสผ่าน? กรุณาใส่อีเมล์ เราจะทำการส่งรหัส่ผ่านใหม่ให้ทางอีเมล์' ,
	'reset_password' => 'Reset password',
	'term_condition' => 'ข้าพเจ้าได้อ่านและยอมรับ ',
	'pay_way' => 'ช่องทางการจ่ายเงิน',
	'pay_status' => 'สถานะการจ่ายเงิน',
	'remark' => 'รายละเอียดเพิ่มเติม',
	'newArrival' => 'สินค้ามาใหม่',
	'recommend' => 'สินค้าแนะนำ',
	'cart' => 'รถเข็น',
	'view_more' => 'ดูเพิ่มเติม',
	'rec_shop' => 'ร้านค้าแนะนำ',
	'wholesale_' => 'ขายส่ง',
	'retail' => 'ขายปลีก',
	'register' => 'ลงทะเบียน',
	'news_register_privilege' => 'รับข้อมูลสิทธิพิเศษและกิจกรรมต่างๆ ก่อนใคร',
	'sold_number' => 'ขายแล้ว',
	'top_category' => 'สุดยอด Category ประจำเดือน',
	'why_shop_with_us' => 'ทำไมต้องช้อปที่บังกี้?',
	'help_center' => 'ศูนย์ช่วยเหลือ',
	'reference_id' => 'หมายเลขคำสั่งซื้อ',
	'total_product' => 'ราคารวมสินค้า',
	'why_wholesale' => 'ขายส่ง',
	'career' => 'สมัครงานกับเรา',
	'privacy_policy' => 'นโยบายความเป็นส่วนตัว',
	'term_of_use' => 'ขอบเขตการใช้งาน',
	'business' => 'ด้านธุรกิจ',
	'how_to_saign_up' => 'ลงทะเบียนอย่างไร',
	'how_to_buy' => 'ซื้อของอย่างไร',
	'how_to_sell' => 'ขายสินค้าอย่างไร',
	'b2b_exporting_policy' => 'นโยบายการส่งออกแบบ B2B ',
	'refund_return' => 'การคืนสินค้าและการคืนเงิน',
	'midnight_service' => 'Midnight Service',
	'help' => 'ช่วยเหลือ',
	'verified_by' => 'ตรวจสอบโดย',
	'delivery_services' => 'จัดส่งโดย',
	'what_are_you_looking_for' => 'กำลังหาอะไรเอ่ย...',
	'not_found' => 'ไม่พบสินค้า',
	'world_wide_sutomer_base' => 'ฐานลูกค้าทั่วทุกมุมโลก',
	'trusted_brands' => 'แบรนด์ชั้นนำ',
	'not_avai' => 'ยังไม่เปิดให้ลงทะเบียน ขออภัยในความไม่สะดวก',
	'data_confirm' => 'ยืนยันข้อมูล',
	'confirm' => 'ยืนยัน',
	'sell_on_bungkie' => 'ร่วมธุรกิจกับ Bungkie',
	'leave_us_a_message' => 'ฝากข้อความถึงเรา',
	'subject' => 'หัวข้อ',
	'your_message' => 'ข้อความ',
	'send_message' => 'ส่งข้อความ',
	'our_office' => 'ออฟฟิศของเรา',
	'address1' => '2034/132 ตึกอิตัลไทย ชั้น 31',
	'address2' => 'ถนนเพชรบุรีตัดใหม่ แขวงบางกะปิ เขตห้วยขวาง',
	'address3' => 'กรุงเทพ ประเทศไทย',
	'show_password' => 'แสดงรหัสผ่าน',
	'delete' => 'ลบ',
	'edit' => 'แก้ไข',
	'activity' => 'กิจกรรม'
	
]; 